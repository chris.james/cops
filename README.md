# Cops ( Chris' Open Password System )

## Summary
Cops is four things

  1. A secure password generator
  2. A secure storage and retrieval system for tiny data
  3. A simple interface to many common hashing algorithms ( sha/md5/lanman ) for creation or verification of one-way-hashes
  4. A method for secure tiny data exchange (think pgp for very small text messages)

## Getting started
The first thing you will probably want to do (especially if you have pulled from gitlab) is to run ./environment_check in the cops directory. This will inform you of any issues with your installation or environment.

## User permissions
Do not ever allow cops to be run as a user other than yourself (for example via sudo) as cops has arbitrary permissions to write files locally with only file-system constraints.
Don't run as root at all unless you REALLY know what you are doing.

## Further information
More detailed information on cops is available in ABOUT.md and the /docs folder

## Raising bugs
Before raising a bug, please re-run the failed command with the --debug/-D option. This may give you insight as to the problem and help you resolve it. If not, please submit a bug report including the output from --debug but TAKING CARE to remove any sensitive information!

