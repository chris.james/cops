## Easy TODO list
- Check if new (dev) group functionality can be bypassed by touching an entry in the store with a non-group username (do we only check for a filename?)
- Add --noendorse flag
- Check examples document and bring it up to date
- Fix passphrase input being echoed when --stdin passed to cops decrypt

# Hard TODO list
- Encrypt entry names (Breaking change)
- All the other TODO listed in comments as a 'Breaking change'

