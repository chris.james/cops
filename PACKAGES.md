# Packages

Included is the file 'packages.csv' which attempts to describe which packages are required (or optional) for cops to function.

The first line of the CSV is the header and describes which column is needed for your operating system

This format is considered to be the simplest in order to allow users to customise their requirements via the CLI


e.g.
```
Package Name,Type,Dependency,Ubuntu 22.04,Fedora 37
```

Therefore, to generate a list of all packages required for Ubuntu 22.04 (optional and required) you can use the following command;

```
$ egrep '(REQUIRED|OPTIONAL)' packages.csv | cut -f 4 -d , | sort | uniq | perl -p -e 's/\n/ /g'
coreutils git grep libclass-xsaccessor-perl libcrypt-cbc-perl libcrypt-rijndael-perl libcrypt-smbhash-perl libcryptx-perl libdigest-md4-perl libcryptx-perl libperl libdate-simple-perl libdigest-bcrypt-perl libdigest-sha-perl libperl libhttp-tiny-perl perl-modules libio-socket-ssl-perl liblist-allutils-perl liblist-someutils-xs-perl libmoo-perl libperl libterm-readkey-perl openssh-client openssl perl-base perl-modules perl-modules procps qdbus x11-utils xdotool xsel
```

Please note that for the RHEL clones, EPEL is almost certainly a requirement for some of the optional packages

The package list will be maintained, and contributions are welcome.
