# Cops ( Chris' Open Password System )

## Overview
At the heart of cops is the usage of ssh keypairs for encyption and decryption. This means that each entry stored in cops is encrypted in one of the most secure methods currently known to cryptography.
The normal asymmetric cipher shortcomings are inconsequential in this instance due to the small amounts of data stored, and the fact that encryption/decryption time is not a limiting factor ( within reason ).
This also means that you can store your password database in public environments, such as a git repository.

Cops is written in perl, but relies on interaction with several standard linux binaries, notably a recent version of openssl.

The ethos behind cops is simple, that knowing the generation and encryption method of a password does not weaken it to a level considered insecure.

As every part of cops data storage is a discrete file, changes and revocations are easily made. It should be noted that if using a versioning system such as git that no data is ever truly destroyed. This is intentional.


## Installation
Cops should function under any modern version of perl, on any relatively recent linux distribution. This includes Cygwin or WSL for those users on a windows platform.
Upstream vendors should be able to simply provide the required additional binaries and any missing perl libraries in the rare case that a default installation does not cover all requirements.
A full list of required packages is at the end of this document.

The Cops install directory can be placed in its entirety in any part of the linux system. It is recommended that this location is either part of the users PATH, or it is added to the users PATH. The rpm version of this package stores it under /opt/cops

Cops requires that all users of shared password store use a unique user login ( determined by 'whoami' ).
Please note that a non-unique username does not compromise security, it simply causes read issues for additional users of the same name.

If you are a single user then no further configuration is likely required. If you are using a shared password store then the administrator of that store will provide you details of how to access, download and update that store.
Please note that if you are using git, additional functionality is built into cops.


## Usage
The usual linux help system will provide most required details at this point.
Assuming cops is in your PATH, simply typing 'cops' will give you a standard help page.
'cops <command> help' will provide further help details on a command including any additional available options.

Additional documentation is available in the installation directory/Docs


## Password Generator
Cops uses a custom built RNG interface coupled with a large wordlist as its default password generation mechanism.
The oft incorrectly referenced xkcd strip ( https://xkcd.com/936/ ) is the basis of this method.
The objective of a secure password is to ensure it is utterly random and with enough entropy to prevent a brute-force attack.

A wordlist based password ( assuming that the password hashing algorithm can accommodate the length of a typically generated password ) is easier to remember and type than a randomised character password exhibiting the same entropy.
Making a password easier to remember and type reduces password re-use ( I had to change my password, so now there is a '4' at the end instead of a '3' ) and encourages passwords with much more randomised data.

The generator also allows for users to generate passwords using a simplified or larger wordlist or randomised characters ( alphanumeric only or including special characters ) . It is worth detailing that cops does not subscribe to any arbitrary password complexity requirements by intention.

You should always use the strongest password that you find usable.
Remember, **Security at the expense of usability comes at the expense of security!**


## Password Storage
Passwords ( and any other secure data ) are stored using public key encryption. Random padding is used around an entry which slighly reduces the amount of data that can be stored, at the benefit of greatly increased security.

There is a limit on the size of data allowed by cops and a warning message will be displayed if those limits are breached.

The limit is determined per stored key. Users with larger ( more secure ) keys will be able to store larger amounts of data.

A user with a 4 kilobit rsa key could typically store up to 500 bytes of data with the default settings within cops


## Password endorsement
You have the option when creating password entries to sign them using the --endorse/-E flag.
Users who have your public key stored will be able to verify the person who created the entry.


## Password hashing
Cops has the ability (on both generation and retrieval) to hash a password using several algorithms (eg md5, sha512, mysql).
This allows for the user to generate a password, store it, implement the hash on a system and never actually know the password.

If you wish to access the hashing functions directly then cops allows this via 'cops hash'

Note: The hash of supplied data is not stored in cops by default.


## Random Number Generator
Cops uses a custom interface to the built-in RNG of /dev/random which is by default a blocking pRNG.
By specifying a flag (--weaker/-w) at generation time /dev/urandom can be used instead which could be considered adequate for most users needs should entropy generation be an issue on your system. This would typically be the case on a virtual machine where the entropy daemon cannot collect adequate data from hardware sources.

/dev/random is considered cryptographically secure for all but the most secret of applications ( Top-Secret government systems for example )
Modern Linux distributions support a quality pRNG within /dev/random. Further information is available at http://en.wikipedia.org/wiki//dev/random

As /dev/random supports only 8 bit integers, a custom wrapper has been built around it to provide large number generation up to the limits of either 32 bit or 64 bit architecture.
The process can be time consuming for large numbers but is considered trivial in this application.


## Weaknesses
### Storage mechanism
Due to the current belief in the strength of public key encryption, and assuming that all keys are of a minimum accepted strength then the only known weaknesses in this system are as follows

1. A users private key and passphrase are compromised, along with the password database
2. An attacker has access to and injects their key into a password store and it is not noticed when group passwords are generated.
3. A side-channel attack occurs at time of generation

Assuming that (for example) git is used for a central password storage facility, (2) is assumed to be negated by (1) assuming that PKA is required for git pull/push requests.

Although it would not be ideal for the contents of the password store to be publicly known it would contain data no less secure than any other asymmetrically encrypted data. Collision attacks are not currently viable on the typically used RSA algorithm and no publicly known weaknesses exist in the rsa algorithm itself.

### Memory
Perl scripts cannot be feasibly run in secure memory and it is not possible to prevent perl script memory from dumping to swap.
Given the short in memory life-cycle of cops and assuming that swap is encrypted then this exposes no real additional danger given that anyone able to access that data is also able to log your keystrokes ( must be in your user space or higher privilege )

### Keyrings
If you choose to use a system keyring via the --keyring option then a large alphanumeric encryption key is stored in your keyring. This encryption key is used to AES-256 encrypt your passphrase which is then stored in ~/.cops/cops.keyring/. AES-256 is considered secure by all current standards.
If an attacker gains access to

  Your keyring 'and'
  Your encrypted passphrase 'and'
  Your private ssh key

Then you have a problem.
However, once again, gaining access to your keyring should (by default) be a very difficult thing to do. This method could be considered MORE secure than typing your passphrase manually each time as it is less susceptable to keylogging and shoulder surfing.

### Password stores
Although the secret information is encrypted correctly in a password store, the names of the secrets is not.
This does present an issue in that information leakage, however small, is present in a public data store (i.e. incorrectly secured git). An attacker can gain insight into the types of entries stored.
This is a task to resolve when/if version 3 of cops is developed by creating an encrypted index of entries.

### Password indexes
Password indexes (i.e. the number assigned to an entry) is variable. This can lead to information leakage by potentially presenting the wrong secret after an update to the password store IF a number assignmnet is used rather than the entry name and --exactmatch
This is a task to resolve when/if version 3 of cops is developed by creating an encrypted index of entries.

## FAQ
### Why use ssh keys rather than industry standard GPG keys?

For a few main reasons
- For simplicity on the end users part. GPG keys, and their management can be very tricky to navigate for inexperienced users.
- Accessability. Linux users will typically already have an ssh key created. This makes it very simple for an administrator to construct a keystore for current users.
- Regular issues/vulnerabilities with gpg keys. It is becomming more frequent that issues with pgp/gpg are being revealed (as of 2020). Due to the age and complexity of these systems a simpler form of asymetric encryption is preferred.
- Portability - ssh keys are far more portable for the purposes of cops and the integrated systems therein.

There are some downsides to using ssh keys over GPG keys but there are keystores out there that *do* use GPG keys. You should certainly compare features and usability before committing to a choice of system.

### Will there ever be a native Windows version of cops?

Highly unlikely. The reliance on Linux binaries and the accessability of functions under Linux mean that a Windows version would be beyond the current maintainers ability to produce.
Cops is of course open source. Feel free to fork and try!

----------------------------------------------------------------------------------------------

## System requirements

### Operating system
Any modern Linux (or derivitive) system should be able to run cops without modification
It is untested on Windows primarily due to the openssl pre-requisite.

The supplied script environment_check can be used to test your local environment automatically
Details of those packages are supplied here for clarity

### Required packages that COPS will not work without (typically part of a default installation)
**Perl modules**
-  Getopt::Long
-  Term::ANSIColor
-  POSIX
-  Term::ReadKey
-  Time::Out

**Binaries**
- openssl
- base64
- md5sum

### Optional packages (required for additional functionality)
**Perl modules**
- Digest::SHA              - For the mysql hashing function       
- Digest::MD5              - For unsalted md5 hashes
- Digest::MD4              - Dependency if using Crypt::SmbHash
- Digest::Bcrypt           - For bcrypt hashing
- Crypt::CBC               - If using a keyring to store the ssh key passphrase
- Crypt::SmbHash           - To access the lanman and ntmd4 hashing functions
- Crypt::Rijndael          - If using a keyring to store the ssh key passphrase
- MIME::Base64             - If using a keyring to store the ssh key passphrase
- HTTP::Tiny               - For the haveIbeenpwnd API


**Binaries**
- ssh-keygen	 	           - Required for simple public key conversion 
- xsel       		           - For piping to copy buffers
- git        		           - For interacting with git repositories
- xdotool    		           - For auto-typing into dialogue boxes
- xwininfo                 - For auto-typing into dialogue boxes
- termux-clipboard-get     - For use under a termux environment
- termux-clipboard-set     - For use under a termux environment


----------------------------------------------------------------------------------------------

