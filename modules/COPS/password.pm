# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::password;
use strict;
use Time::Out qw(timeout);

sub generate
{
  # Define paramaters
  my $passin;
  
  if($COPS::args::param->{'easygen'})
  {
    $passin->{'type'}='alphanumeric';
    $passin->{'length'} = 21;
  }
  else
  {
    $COPS::args::param->{'randomchar'} and $passin->{'type'}='character';
    $COPS::args::param->{'alphanumeric'} and $passin->{'type'}='alphanumeric';
    $COPS::args::param->{'pin'} and $passin->{'type'}='numeric';
    $passin->{'type'} or $passin->{'type'}='word';

    $passin->{'no-hyphens'}=$COPS::args::param->{'no-hyphens'};
    $passin->{'use-spaces'}=$COPS::args::param->{'use-spaces'};


    $COPS::args::param->{'insensitive'} and $passin->{'case'}='insensitive';

    $passin->{'wordlist'} = $COPS::args::wordlist;
    $passin->{'maxlength'} = $COPS::args::param->{'maxlength'};

    $passin->{'length'} = $COPS::args::param->{'length'};
  }

  $passin->{'source'} = $COPS::config::rngsource;

  COPS::print::debug("Passing data to gen function\n");
  
  my $result = timeout $COPS::config::internal_timeout => sub { COPS::password::data($passin) };
  ($@) and COPS::print::fatal("Password generation timed out. Perhaps your entropy pool is empty? Try using --weaker");

  $COPS::args::param->{'easygen'} and $result->{'password'} ="\$8.$result->{'password'}";

  return $result;
}

sub data
{
  my $arg=shift;
  
  COPS::print::debug("Received call to generate password with the following arguments");
  COPS::print::debug("Data dump follows\n\n  ".COPS::FUNC::debug::dump($arg));
  
  # Defaults
  $arg->{'type'} or COPS::print::fatal("No type passed to generate::password");
  
  my $separator='-'; # Default
  
  $arg->{'no-hyphens'} and $separator='';
  $arg->{'use-spaces'} and $separator=' ';

  $arg->{'case'} or $arg->{'case'}='sensitive';
  $arg->{'length'} or $arg->{'length'}=8;
  
  # Optional: wordlist, maxlength
  # Acceptable type: word, character, alhpanumeric, numeric
  
  my $password;
  my $variants=1;       # Total possible passwords given specified criteria
  my $wlsize=0;
    
  ###################################################
  if($arg->{'type'} eq "word")
  {
    my @words = get_wordlist(
        {
          'maxlength' => $arg->{'maxlength'}, 
          'wordlist'  => $arg->{'wordlist'}
        }
    );
    $wlsize = scalar(@words);
    # A wordbank password
    for(my $i =1; $i <= $arg->{'length'}; $i++)
    {
      my $r = COPS::FUNC::rng::get_range(0,($wlsize-1),$arg->{'source'});
      my $word = $words[$r];
      $word or COPS::print::error("Failed to generate word for random number '$r'");
      $arg->{'case'} eq 'insensitive' and $word = lc($word);
      $password .= $word;
      $variants=$variants*$wlsize;
      $password .= $separator;
    }
    $separator and chop($password); # Remove the last erroneous character if required
  }
  elsif($arg->{'type'} =~ /^(character|numeric|alphanumeric)$/)
  {
    # A random character password. Case insensitivity here is a bit messy but it will do for now
    for(my $i=1;$i<=$arg->{'length'}; $i++)
    {
      if($arg->{'type'} eq "alphanumeric")
      {
        if($arg->{'case'} eq 'insensitive')
        {
          my $c = 'A';
          while($c =~ /[A-Z]/)
          {
            $c = COPS::FUNC::rng::alpha($arg->{'source'});
          }
          $password .= $c;
          $variants = $variants * 36;
        }
        else
        {
          $password .= COPS::FUNC::rng::alpha($arg->{'source'});
          $variants = $variants * 62;
        }
      }
      elsif($arg->{'type'} eq "numeric")
      {
        $password .= COPS::FUNC::rng::number($arg->{'source'});
        $variants = $variants * 10;
      }
      else
      {
        if($arg->{'case'} eq 'insensitive')
        {
          my $c = 'A';
          while($c =~ /[A-Z]/)
          {
            $c = COPS::FUNC::rng::character($arg->{'source'});
          }
          $password .= $c;
          $variants = $variants * 67;
        }
        else
        {
          $password .= COPS::FUNC::rng::character($arg->{'source'});
          $variants = $variants * 93;
        }
      }
    }
  }
  else
  {
    COPS::print::fatal("Unrecognized password type '$arg->{'type'}' passed to generate routine");
  }  
  $password or COPS::print::error("Failed to generate a password :(");
  COPS::print::debug("Password generated!");
  
  return(
    {
      password => $password, 
      wordlistsize => $wlsize, 
      variants => $variants
    }
  );
}

sub get_wordlist
{
  my $arg = shift;
  $arg->{'maxlength'} or COPS::print::fatal("Must provide maxlength to get_wordlist");
  $arg->{'wordlist'} or COPS::print::fatal("Must provide wordlist to get_wordlist");
  
  my @words;
  my $previous='';

  # We need to be able to sort this wordlist, so grab it in a raw format first
  my @raw_words = COPS::FUNC::file::readasarray($arg->{'wordlist'});

  foreach(sort @raw_words)
  {
    /^[A-Za-z]*$/ or next;
    ucfirst($_) eq $previous and next; # Weed out duplicates. This also discards duplicates where the only difference is caps
    length($_) > $arg->{'maxlength'} and next;
    length($_) < 3 and next;
    push @words, ucfirst($_);
    $previous = ucfirst($_);
  }
  @words or COPS::print::fatal("Defined wordlist $arg->{'wordlist'} does not contain any qualifying entries.");
  return @words;
}

1;
