# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::keyring;
use strict;

my $pin="";

# Interface to a local keyring. Initial support only for kdewallet via dbus

# We use a local wallet NOT to store the ssh key passphrase, but to store the
# encryption key for the passphrase. This means that an attacker has to compromise
# both local memory and the keyring to get the passphrase.

# It does mean that we can use the keyrings protected memory however.


#############################################################################################
sub get_passphrase
{
  my $ppek;
  if($COPS::args::param->{'keyring'} eq "kdewallet")
  {
    return COPS::KEYRING::kdewallet::get_passphrase();
  }
  else
  {
    COPS::print::fatal("Keyring type '$COPS::args::param->{'keyring'}' not supported\n");
  }
}

#############################################################################################
sub clear
{
  if($COPS::args::param->{'keyring'} eq "kdewallet")
  {
    return COPS::KEYRING::kdewallet::clear();
  }
  else
  {
    COPS::print::fatal("Keyring type not supported");
  }
}

#############################################################################################
sub get_pin
{
  my $msg = shift;
  $msg =~ /.*\s$/ or $msg .= " ";
  $COPS::args::param->{'keyringpin'} or return '';
  $pin and return $pin;
  $pin = COPS::FUNC::input::noecho("Provide ${msg}pin code");
  chomp($pin);
  $pin =~ /[^0-9a-zA-z]/ and COPS::print::fatal("Only a-zA-Z0-9 supported for pin entry");
  return $pin;
}

1;



