# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::print;
use strict;
use Term::ReadKey;

my $cv=0;

unless($COPS::args::param->{'noansi'})
{
  require "Term/ANSIColor.pm";
  import Term::ANSIColor;

  if((Term::ANSIColor->VERSION) > 2)
  {
    # Added in 2.01
    import Term::ANSIColor qw(colorvalid);
    $cv=1;
  }
}

# Usage ( in ascending order of precedence )

# debug   - Debug items
# info    - Items that are supressed with --quiet
# std     - Normal text ( the default )
# warn    - Warning notices. May include a countdown notice before proceding
# fatal   - An issue caused by the user has occurred that causes the script to terminate (no newline required on call)
# error   - An issue within the script has occurred that causes the script to terminate (no newline required on call)

# All ansi colour handling should be done here in this module. This is a change from 2.31 onwards


# Pass to this module with either the shortcut
#
# COPS::print::fatal("I borked");
#
# or the full method passing a hashref
#
# COPS::print::out({text => "text", type => "type", time => "time"}) type and time are optional

my $line=1; # Used to increment debug lines

my $stderr;
$stderr->{'warn'}=1;
$stderr->{'fatal'}=1;
$stderr->{'error'}=1;

my $die;
$die->{'fatal'}=1;
$die->{'error'}=1;

sub debug {COPS::print::out({text => shift, type => "debug"})}
sub info  {COPS::print::out({text => shift, type => "info"})}
sub std   {COPS::print::out({text => shift, type => "std"})}
sub notice{COPS::print::out({text => shift, type => "notice"})}
sub warn  {COPS::print::out({text => shift, type => "warn"})}
sub fatal {COPS::print::out({text => shift, type => "fatal"})}
sub error {COPS::print::out({text => shift, type => "error"})}

sub out
{
  my $arg = shift;
  my $text=$arg->{'text'};
  my $type=$arg->{'type'};
  my $time=$arg->{'time'};
  my $colour=$arg->{'colour'}; # Override built-in colours with another type
  
  # Allow noansi to be defined in COPS::args, or passed as a parameter to this function
  my $noansi=$COPS::args::param->{'noansi'};
  defined($arg->{'noansi'}) and $noansi=1;

  $arg->{'timer'} and COPS::print::error("It's 'time' ... not 'timer'"); # Reminder to me!

  !$arg->{'nolf'} and $type =~ /^(debug|fatal|error|warn)$/ and $type !~ /.*\n$/ and $text .= "\n";

  defined($type) or $type = "std";
  defined($time) or $time = 0;

  $type =~ /^(debug|info|std|notice|warn|fatal|error)$/ or die "Incorrect output type ($type) sent to cops print\n"; # Don't set up a recursive loop by hooking back into this function, just die

  $text or COPS::print::error("No text passed to COPS::print::out");

  # debug hook
  if($type eq "debug")
  {
    $COPS::args::param->{'debug'} or return;
    my @call = caller(1);
    $text = "[ ".sprintf("%05d",$line)." : ".sprintf("%-28s","$call[0]:$call[2]")." ]: $text";
    $line++;
  }

  ($type eq "info" and $COPS::args::param->{'quiet'}) and return;
  if($COPS::args::param->{'silent'})
  {
    $type eq "fatal" or $type eq "error" or $type eq "std" or return;
  }

  select(STDOUT);
  defined($stderr->{$type}) and select(STDERR);
  if($noansi)
  {
    $text=~s/##CC=.*?##//g; # Remove inline colour tags if they are there
  }
  else
  {
    if($cv)
    {
      # $cv is a flag to see if colorvalid is supported
      colorvalid($COPS::config::colours->{$type}) or COPS::print::error("Colour $COPS::config::colours->{$type} is invalid (check your ~/.copsrc?)");
    }
    my $c = color($COPS::config::colours->{$type});
    $colour and $c = color($COPS::config::colours->{$colour}); # Colour overrides

    my $reset=color('reset');
    if(defined($c))
    {
      # This type of text has a defined colour
      $text = $c.$text;
      $reset = color('reset').$c;
    }

    # Messy substitution method that accounts for pre-defined colours
    while($text =~ /^(.*)##CC=(.*?)##(.*)$/s)
    {
      if(lc($2) eq "reset")
      {
        $text = $1.$reset.$3;
      }
      else
      {
        $text = $1.color($2).$3;
      }
    }
  }

  ($type =~ /^(fatal|error)$/) and print uc($type).": ";
  print $text;  
  
  if($time)
  {
    $time > 99 and COPS::print::error("Timer set to greater than 99 seconds. Aborting"); # Won't recursive loop as time now not set
    print "\n";
    # Disable echo in case you start typing a passphrase by accident
    ReadMode(3);
    for (my $i = int($time); $i > 0; $i--)
    {
      print "Waiting [".sprintf("%02d", $i)."]\r";
      sleep(1);
    }
    ReadMode(0);
    print "Waiting [OK]\r";
    print "\n";
  }
    
  $noansi or print color('reset');
  if($type =~ /^(error|fatal)$/)
  {
    if($COPS::args::param->{'debug'})
    {
      print "------------------------------------------\n";
      print "System unwind: \n";

      my ($package, $filename, $line, $subroutine)=caller(0);
      my $err;
      my $n=0;
      while( $package )
      {
        $err->{$n}->{'package'}=$package;
        $err->{$n}->{'line'}=$line;
        $err->{$n}->{'called'}=$subroutine;
        ($package, $filename, $line, $subroutine) = caller(++$n);
      }
      foreach(reverse sort {$a <=> $b} keys %$err)
      {
        if($_ == 1)
        {
          # actual calling function
          print color($COPS::config::colours->{$type});
        }
        print "  ($_) $err->{$_}->{'package'}:$err->{$_}->{'line'} called $err->{$_}->{'called'}\n";
        print color('reset');
      }

      print "------------------------------------------\n";
      print "-Exit\n";
      $COPS::args::param->{'noansi'} or print color('reset');
    }

    $INPUT::loaded and COPS::FUNC::input::clearbuffer();
  }

  # If stderr type, print a newline ( so we don't have to add one for each message
  defined($die->{$type}) and exit(1);
}

sub die
{
  # Acts to catch unhandled errors, in particular to catch things like failed module imports
  my $error = "@_";

  # Buggy threads implementation throws an error. I *believe* this is due to proxy env checks but not going to worry too much about it
  $error =~ /Can't locate object method "tid" via package "threads" at/ and return;

  if($error =~ /^Can't locate ([^\s]+?) in \@INC/)
  {
    return if ($^S); # Return if contained within an eval block, as we're evaluating from within module::check
    COPS::print::error("Failed to find perl module for optional function. Missing module is $1\nYou may want to run '$COPS::root/environment_check' to test your environment? See README.md for further details");
  }
  else
  {
    # A generic failsafe error handler
    my $err;
    my $n = 0;

    my ($package, $filename, $line, $subroutine)=caller(0);
    while ( $package )
    {
      $err->{$n}->{'package'}=$package;
      $err->{$n}->{'line'}=$line;
      $err->{$n}->{'called'}=$subroutine;
      
      ($package, $filename, $line, $subroutine) = caller(++$n);
    }

    print "FATAL: There has been an unhandled error!\n";
    print "If you wish to report this, please include the following information:\n\n";
    print "Error: $error\n";
    print "System debug:\n\n";
    foreach(reverse sort {$a <=> $b} keys %$err)
    {
        print "  ($_) $err->{$_}->{'package'}, line $err->{$_}->{'line'} -> $err->{$_}->{'called'}\n";
    }
    exit(1);
  }
}

1;
