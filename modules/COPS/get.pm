# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::get;
use strict;

sub entry
{
  # Synopsis : Which file is required for an entry ?
  
  # Accepts hashref
  #   type
  #   text
  #   noprompt
  #
  # Returns hashref 
  #   file
  #   id
  
  my $arg = shift;
  $arg->{'type'} or COPS::print::fatal("No type passed to function");
  $arg->{'text'} or COPS::print::fatal("No text passed to function");
  my $passin; 
  
  $passin = ({
    text     => $arg->{'text'},
    type     => $arg->{'type'},
    store    => $COPS::args::param->{'store'},
    back     => $COPS::args::param->{'backwards'},
    username => $COPS::user::name
  });
  
  defined($COPS::args::param->{'exactmatch'}) and $passin->{'exactmatch'} = $COPS::args::param->{'exactmatch'};
  defined($COPS::args::param->{'fuzzymatch'}) and $passin->{'fuzzymatch'} = $COPS::args::param->{'fuzzymatch'};
    
  my $entry = COPS::FUNC::search::entries($passin);

  $entry or return 0;
  
  $entry->{'file'} =~ /.*\/(.*)\/.*/;
  $entry->{'name'} = $1;

  COPS::print::debug("Entry location '$entry->{'file'}' with id '$entry->{'id'}'\n");
 
  return $entry;
}
  
1;
