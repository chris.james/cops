# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::clipboard;
use strict;

# A wrapper for xsel

sub buffer
{
  my $password = shift;

  my $previous;
  my $get;
  my $inject;
  
  if($COPS::args::param->{'termux'})
  {
    my $tg = COPS::FUNC::which::binary('termux-clipboard-get');
    my $ts = COPS::FUNC::which::binary('termux-clipboard-set');
    $get = "$tg";
    $previous = COPS::FUNC::exec::cmd($tg);
    chomp($previous);
    $inject = $ts;
  }
  else
  {
    # Check xsel is installed
    my $xsel = COPS::FUNC::which::binary('xsel');

    my $bt = join("|",@COPS::config::allowed_buffer_types);
    ($COPS::args::param->{'buffer'} =~ /^($bt)$/ and $COPS::args::param->{'buffer'} =$1) || COPS::print::fatal("Incorrect parameter passed to the buffer");

    # Retrieve current clipboard
    $previous = COPS::FUNC::exec::cmd("$xsel -o");
    $get = "$xsel -o";

    # Inject password into clipboard
    $inject = "$xsel -i$COPS::args::param->{'buffer'} -t ".($COPS::args::param->{'timeoutbuffer'}*1000);
  }
  
  open(CB, "|$inject") or COPS::print::fatal("failed to open clipboard");
  print CB $password;
  close(CB);

  COPS::print::out({text => "Copied entry to clipboard buffer.", type => "notice", time => $COPS::args::param->{'timeoutbuffer'}});

  (COPS::FUNC::exec::cmd($get) eq "$password") or COPS::print::warn("Failed to remove password from clipboard");
  
  open(CB, "|$inject");
  print CB $previous;
  close(CB);
  
  COPS::print::info("Entry deleted from clipboard buffer and previous contents restored\n");

}

1;
