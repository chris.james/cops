# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::passphrase;
use strict;

sub get
{
  my $type = shift;
  defined($type) or $type = '';
  my $suf='';

  $type eq "endorse" and $suf = ' for endorsement'; # Make it clear why in this instance
  COPS::print::info("Passphrase required to unlock private key$suf\n");
  my $pp;
  if($COPS::args::param->{'keyring'})
  {
      $pp = COPS::keyring::get_passphrase();
  }
  else
  {
      $pp = COPS::passphrase::enter();
  }
  $pp or COPS::print::error("No passphrase available.");
  COPS::print::info("Passphrase loaded\n");
  return $pp;
}
  
sub enter
{
  my $pp = COPS::FUNC::input::noecho('Enter private key passphrase');
  check($pp);
  return $pp;
}

sub check
{
  my $pp = shift;
  chomp($pp);
  length($pp) > 256 and COPS::print::fatal("Sorry, we only support passphrases up to 256 characters long");   # TODO: Explain why
}
  
1;
