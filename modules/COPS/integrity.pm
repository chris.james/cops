# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::integrity;
use strict;

# Used for the file integrity checks for keys and groups
sub check
{
  # Accepts
  # hash: hash->{<name>}=<filename>
  # check_type: key or group

  my $files=shift;
  my $check_type=shift;
  
  COPS::print::debug("Checking $check_type file integrity");
  
  my $file   = COPS::FUNC::file::sanitize($COPS::args::param->{'userdir'}."/current_${check_type}s");
  
  my $known_files;
  my $loaded_files;
  
  # Grab a list of known keys and their md5sums
  if(COPS::FUNC::file::status($file)->{'type'} eq 'text')
  {
    foreach (split(/\n/,COPS::FUNC::file::readlocal($file)))
    {
      /^([a-f0-9]+?)\s+(.*)$/ or next;
      $known_files->{$2}=$1;
    }
  }
  
  # Grab the md5sum for keys in this store. We call this 'loaded_files'
  foreach my $name (keys %{$files})
  {
    $loaded_files->{$name}=COPS::FUNC::file::md5sum($files->{$name});
  }
  
  my $messaging;

  # Itterate through loaded_files and check if we know about them
  foreach my $k (keys %{$loaded_files})
  {
    unless(defined($known_files->{$k}))
    {
      $messaging .= "New $check_type added for '$k' with md5sum $loaded_files->{$k}\n";
      next;
    }

    unless(defined($loaded_files->{$k}))
    {
      $messaging .= ucfirst($check_type)." removed for '$k'\n";
      next;
    }
    
    if(defined($known_files->{$k}) and defined($loaded_files->{$k}))
    {
      ($known_files->{$k} ne $loaded_files->{$k}) and $messaging .= ucfirst($check_type)." '$k' has altered checksum ($loaded_files->{$k})\n";
    }
  }
  
  my $filelist;
  
  my $merged = {%$known_files, %$loaded_files}; # Preference given to known_files
  
  foreach my $k (sort keys %{$merged})
  {
    $filelist.="$merged->{$k} $k\n";
  }
    
  if($messaging)
  {
    if(COPS::FUNC::file::status($file)->{'type'} eq 'text')
    {
      COPS::print::warn("WARNING: One or more ${check_type}s have changed in your password store!");
      COPS::print::warn("------------------------------------");
      COPS::print::warn($messaging);
      COPS::print::notice("If you accept this message the current integrity check file will be updated with the new key information.\n");
      
    }
    else
    {
      COPS::print::warn("WARNING: No current_files file found!");
      COPS::print::warn("------------------------------------");
      COPS::print::notice("This file is an integrity check of stored user keys.\n\n");
      COPS::print::notice("You should only see this warning on an fresh install, or update from a version of cops less than 2.26.0\n\nIt is only checked when running cops <add|update|refresh>\n");
      COPS::print::notice("This integrity check file will be generated once you accept this message.\n");
      
    }
    COPS::print::notice("In the interest of security, you should probably confirm that the ${check_type}s stored in $COPS::args::param->{'store'}/.cops_keys/ are correct.\n");
    COPS::print::warn("------------------------------------");
    COPS::print::warn("Type 'accept' to continue");
    
    my $answer=COPS::FUNC::input::line();
    chomp($answer);
    (lc($answer) ne "accept") and COPS::print::fatal("Integrity check update aborted. Exiting");
    
    COPS::FUNC::file::writelocal({file => $file, content => $filelist});
  }
  
  
  
  

}

1;
