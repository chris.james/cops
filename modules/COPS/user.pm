# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::user;
use strict;

our $name;
set_username();

sub set_username
{
  $name and return $name; # Already defined elsewhere
  # Simple at the moment, but we can change this later if we want to
  my $n = getlogin || getpwuid($<);
  chomp($n);
  $n =~ /[^a-zA-Z0-9\.\-\_]/ and COPS::print::fatal("Invalid characters found in local username");
  $name = $n;
}

sub get_groups
{
  my $path = COPS::FUNC::file::sanitize($COPS::args::param->{'store'}."/.cops_keys/");
  my $keys;

  my $group;
  my $check;
  
  foreach (glob("$path/*.group"))
  {
    my $file = COPS::FUNC::file::sanitize($_);
    /.*\/(.*)\.group/ or next;
    my $name  = $1;
    $name =~ /^\s*#/ and next
    $name =~ /[^a-zA-Z0-9\.\-\_]/ and COPS::print::fatal("Invalid characters found in group '$name'");
    my @data = COPS::FUNC::file::readasarray($file);
    foreach(@data)
    {
      my $member = $_;
      $member =~ /[^a-zA-Z0-9\.\-\_]/ and COPS::print::fatal("Invalid characters found in group member '$member'");
      $group->{$name}->{$member}=1;
    }
    $check->{$name}=$file;
  }
  COPS::integrity::check($check, "group");
  return $group;
}
  

  
  
1;