# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::list;
use strict;

sub entries
{
  ($COPS::args::argument and !$COPS::args::param->{'filter'}) and $COPS::args::param->{'filter'} = $COPS::args::argument; # Convert a simple filter into the newer 'filter' format

  my $entries = COPS::FUNC::retrieve::all_entries(
  {
    user => $COPS::user::name,
    store => $COPS::args::param->{'store'},
    filter => $COPS::args::param->{'filter'},
    onlyme => $COPS::args::param->{'onlyme'}
  });

  if(keys %{$entries})
  {
    COPS::print::std(COPS::text::password_list($entries,$COPS::args::argument));
  }
  else
  {
    COPS::print::notice("No matching entries found in cops storage $COPS::args::param->{'store'}\n")
  }
}
  
1;
