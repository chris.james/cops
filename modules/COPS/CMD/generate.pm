# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::generate;
use strict;

sub start
{
  ($COPS::args::param->{'type'} eq "password") and COPS::CMD::generate::password();
  ($COPS::args::param->{'type'} eq "username") and COPS::print::std(COPS::CMD::generate::username()."\n");
}

sub password
{
  my $result;

  for(my $i=1; $i<=$COPS::args::param->{'xtimes'}; $i++)
  {
    $result = COPS::password::generate();
    COPS::output::entry($result->{'password'});

    my $wlsize   = COPS::FUNC::number::commify($result->{'wordlistsize'});
    $COPS::args::param->{'entropy'} and COPS::pwquality::entropy({
      password => $result->{'password'},
      wlsize => $wlsize,
      variants => $result->{'variants'}}
    );

    $COPS::args::param->{'hibp'} and COPS::pwquality::hibp($result->{'password'});
  }

}

sub username
{
  my $path = "${COPS::root}/wordlists/";
  my $type;
  $type->{'adjectives'} = [ COPS::FUNC::file::readasarray("$path/adjectives.txt") ];
  $type->{'nouns'} = [ COPS::FUNC::file::readasarray("$path/nouns.txt") ];

  my $un;
  my $size =0;

  foreach my $t ("adjectives","nouns")
  {
    my @array;
    if($COPS::args::param->{'maxlength'})
    {
      foreach(@{$type->{$t}})
      {
        length($_) > $COPS::args::param->{'maxlength'} and next;
        push @array, $_;
      }
    }
    else
    {
      @array = @{$type->{$t}};
    }
    my $size = scalar(@array);
    my $r = COPS::FUNC::rng::get_range(0,($size-1),$COPS::config::rngsource);
    $un .= ucfirst($array[$r]);
  }
  return $un;
}

1;


