# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::autofill;
use strict;

sub entry
{
  my $password = COPS::get::entry({type => 'password', text => $COPS::args::argument });
  
  $password or COPS::print::fatal("Cannot find a password for '$COPS::args::argument'");
  COPS::print::info("Retrieving entry '$password->{'name'}'\n");
  
  my $passphrase = COPS::passphrase::get();
  
  my $pass = COPS::FUNC::ssl::read({
    passphrase => $passphrase,
    privatekey => $COPS::args::param->{'privatekey'},
    base64 =>  COPS::FUNC::retrieve::base64data($password->{'file'}),
    store => $COPS::args::param->{'store'}
    });
  $pass or COPS::output::bad_passphrase();
  
  my $user;
  
  my $username = COPS::get::entry({type => 'username', text => $password->{'id'} });
  if($username)
  {
    $user = COPS::FUNC::ssl::read({
      passphrase => $passphrase,
      privatekey => $COPS::args::param->{'privatekey'},
      base64 =>  COPS::FUNC::retrieve::base64data($username->{'file'}),
      store => $COPS::args::param->{'store'}
    });
    $user or COPS::output::bad_passphrase();
  }
  else
  {
    $user = $COPS::user::name;
    COPS::print::notice("No username entry found. Defaulting to '$COPS::user::name'\n");
  }
  chomp($user);
  
  $user =~ /\n/ and COPS::print::fatal("Username contains a newline. That won't work!");
  
  my $out;
  if($COPS::args::param->{'jumpmethod'} or $COPS::config::jumpmethod->{$password->{'name'}})
  {
    $out = $COPS::config::jumpmethod->{$password->{'name'}};
    $COPS::args::param->{'jumpmethod'} and $out = $COPS::args::param->{'jumpmethod'}; # Override on the cli
    $out =~ /[\\']/ and COPS::print::fatal("Invalid character ( \\ or ' ) in jumpmethod\n");
    $out =~ s/#/\\/g;
    $out =~ s/\[username\]/$user/;
    $out =~ s/\[password\]/$pass/;
  }
  else  
  {
    my $jump = $COPS::args::param->{'jump'};
    $jump >= 1 or COPS::print::error("Jump parameter not set or < 1. That won't work!");
    my $spl = '';
    while($jump > 0)
    {
      $spl .= "\t";
      $jump--;
    }
    $out = "$user$spl$pass";
  }
    
  COPS::output::entry($out);
}

1;