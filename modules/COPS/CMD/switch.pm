# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::switch;
use strict;

# Handles switching on command
sub command
{
  if($COPS::args::param->{'version'})
  {
    # Allow for backwards compatible syntax of 'cops --version' as well as 'cops version' as of v2.50.0
    $COPS::args::command = 'version';
  }

  my $argument = $COPS::args::argument;
  
  ############################################
  
  if($COPS::args::command eq "help" or lc($argument)eq 'help' or $COPS::args::param->{'help'})
  { 
    # Allow for backwards compatible syntax of 'cops <command> help' as well as 'cops <command> --help' as of v2.50.0
    my $help = $argument;
    (lc($help) eq 'help' or !$help) and $help = $COPS::args::command; # Doesn't matter if you type 'cops help addkey' or 'cops addkey help'
    
    COPS::CMD::help::text($help); 
  }
  else
  {
    &{\&{"COPS::CMD::$COPS::args::cmds->{$COPS::args::command}"}}();
  }
    
  # Now do cleanup
  COPS::FUNC::input::clearbuffer();
  COPS::print::debug("Command switch completed");
  COPS::print::debug("Clearing up where required");
  my $tmp = $COPS::args::param->{'userdir'}."/cops.temp/";
  
  if(COPS::FUNC::file::status($tmp)->{'status'})
  {
    my @files = COPS::FUNC::file::dirlist($tmp);
    if(@files)
    {
      COPS::print::debug("Clearing temp folder:");
      foreach(@files)
      {
        COPS::print::debug("Deleting $_");  
        COPS::FUNC::file::delete($_);
      }
    }
  }
  COPS::print::debug("Cops finished. Exiting");
}
      
1;

