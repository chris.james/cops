# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::refresh;
use strict;

#TODO : entries updated count fix. Perhaps warn on number to be done ?

sub entries
{
  my $keys = COPS::keys::retrieve();

  my $entries;

  foreach my $user (keys %{$keys})
  {
    $entries->{$user} = COPS::FUNC::retrieve::all_entries({
      user => $user,
      store => $COPS::args::param->{'store'}
    });
  }
  my $passphrase = COPS::passphrase::get();

  my $done=0;

  foreach my $name ( keys %{$entries->{$COPS::user::name}} )
  {
    COPS::print::debug("Checking entry $name\n");
    $name =~ /^$COPS::user::name\-/ and next; # Skip personal passwords
    foreach my $type (@COPS::config::allowed_entry_types)
    {
      $entries->{$COPS::user::name}->{$name}->{$type}->{'file'} or next;
      my $file = COPS::FUNC::file::status($entries->{$COPS::user::name}->{$name}->{$type}->{'file'});
      $file->{'status'} or next;

      foreach my $user (keys %{$keys})
      {
        COPS::print::debug("Checking username for $user isn't myself\n");
        $user eq $COPS::user::name and next; # Ignore myself
        COPS::print::debug("Checking file '$file->{'path'}'\n");
        $entries->{$user}->{$name}->{$type}->{'file'} and next; # An entry already exists

        COPS::print::info("Adding $name/$type for '$user'\n");

        my $b64 = COPS::FUNC::retrieve::base64data($file->{'path'});
        my $content = COPS::FUNC::ssl::read({
          passphrase => $passphrase,
          privatekey => $COPS::args::param->{'privatekey'},
          base64 => $b64,
          store => $COPS::args::param->{'store'}
        });
        $content or COPS::output::bad_passphrase();

        my $enc = COPS::FUNC::ssl::encrypt({
            data => $content,
            publickey => $keys->{$user},
        });

        if($COPS::args::param->{'endorse'})
	{
          $enc .= COPS::FUNC::endorse::sign({
	    privatekey => $COPS::args::param->{'privatekey'},
	    passphrase => $passphrase,
	    publickey => $COPS::args::param->{'publickey'},
	    data => $enc
	  });
	}

        my $target = COPS::FUNC::time::stamp('long')."_".$user.".$type";
        $target = "$COPS::args::param->{'store'}/$name/$target";

        $target = COPS::FUNC::file::sanitize($target);
        
        $enc = COPS::FUNC::ssl::setversion($enc);

        COPS::FUNC::file::write({
          content => $enc,
          target  => $target
        });
        $done++;
      }
    }
  }
  if($done)
  {
    COPS::print::std("$done entries updated\n");
  }
  else
  {
    COPS::print::notice("No entries to refresh. Everything looks up to date!\n");
  }
}

1;
