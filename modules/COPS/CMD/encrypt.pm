# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::encrypt;
use strict;

# TODO : Allow --whoiam to change the public_key to one in the keystore easily

sub data
{
  my $key;
  if($COPS::args::param->{'encryptfor'})
  {
    $key = $COPS::args::param->{'store'}."/.cops_keys/$COPS::args::param->{'encryptfor'}.pem";
  }
  else
  {
    $key = COPS::keys::get_public();
  }

  my $name = $key;
  $name =~ s/.*\/(.*)$/$1/;
  
  my $data = $COPS::args::argument;
  unless($data)
  {
    if($COPS::args::param->{'stdin'})
    {
      $data = COPS::FUNC::input::stdin();
    }
    else
    {
      $data = COPS::FUNC::input::wall("Enter data (control-D to finish)");
    }
  }
  
  my $encoded = COPS::FUNC::ssl::encrypt({
    data => $data,
    publickey => $key,
    text => "Encrypted with $name"
  });
  
  if($COPS::args::param->{'endorse'})
  {
    $encoded .= COPS::FUNC::endorse::sign({
      publickey  => $COPS::args::param->{'publickey'},
      privatekey => $COPS::args::param->{'privatekey'},
      passphrase => COPS::passphrase::get('endorse'),
      data => $encoded
    });
  }
    
  COPS::print::out({
    type=>'std',
    noansi=>1,
    text=> COPS::FUNC::ssl::setversion($encoded)
  });
}

1;
