# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::help;
use strict;

sub params
{
  text("help-params");
}

sub text
{
  my $cmd  = shift;
  
  COPS::print::debug("$cmd/$COPS::args::command");
  $cmd eq 'help' and $cmd =''; # The only command we don't need specific help for ;)
  my $cmds = COPS::CONFIG::commands::list();
  my $parameters = COPS::CONFIG::parameters::list();
  my $help;
  
  if(!$cmd)
  {
    $help .= COPS::text::bold(
"-------------------------------------------------------------------------------------
COPS : Chris' Open Password System                                      (C) 2015-2019
-------------------------------------------------------------------------------------
");
    $help .= <<BLOCK;

COPS is a secure password storage mechanism based on the principle of utilising
ssh asymetric keypairs for encryption and decryption, allowing for safe central
password storage serving multiple users.

See README.md and the Docs/ directory for more details

-------------------------------------------------------------------------------------

Option defaults can be modified in ~/.copsrc in key/value pairs.
Syntax is to use the full variable name, eg

  randomchar=true
  whoiam=winston.smith
  show
  
'=true' is optional for booleans. To set to false though you MUST use '=false|0|off'
Command line options will always override copsrc configuration

NOTE: ssh key filenames must be unique per password store

BLOCK
    $help .= COPS::text::bold(
"-------------------------------------------------------------------------------------
'cops <command> help' for further details on specific commands
'cops params' for further details on all available flags/paramaters
-------------------------------------------------------------------------------------
");

    $help .= "\nCommand categories:\n";
    foreach my $t (@COPS::CONFIG::commands::types)
    {
      $help .= "  $t: ";
      my @list;
      foreach my $c (sort keys %{$cmds})
      {
        defined($cmds->{$c}->{'type'}) or next;
        $cmds->{$c}->{'type'} eq $t and push @list,$c;
      }
      $help .= join(", ",@list)."\n";
    }
  }

  if(lc($cmd) eq 'help-params')
  {
    # Pretend EVERYTHING is valid if we do a 'cops params'
    my @all = keys %{$parameters};
    $cmds->{$cmd}->{'valid'} = \@all; # Change to an array reference
  }
  else
  {
    $help .= COPS::text::bold("\nUsage:\n");
    foreach my $key (sort keys %{$cmds})
    {
      $cmd and $key ne $cmd and next;
      my $biglist;
      $help .= COPS::text::bold("  cops $key ");
      $cmds->{$key}->{'argument'} and $help .= "<".COPS::text::uline($cmds->{$key}->{'argument'})."> ";
      $cmds->{$key}->{'optionalarg'} and $help .= "[".COPS::text::uline($cmds->{$key}->{'optionalarg'})."] ";
      
      if(defined $cmds->{$key}->{'valid'} and $cmds->{$key}->{'valid'} ne '')
      {
        foreach my $word ( sort @{$cmds->{$key}->{'valid'}} )
        {
          my $bo="[";
          my $bc="]";

          if(defined $cmds->{$key}->{'requiredparam'})
          {
            foreach my $rp (@{$cmds->{$key}->{'requiredparam'}})
            {
              $rp eq $word or next;
              $bo="<";
              $bc=">";
            }
          }

          my $single = $parameters->{$word}->{'single'};
          if( $parameters->{$word}->{'type'} )
          {
            $help .= $bo.COPS::text::bold("--$word");
            defined($single) and $help .= COPS::text::bold("|-$single");

            if($parameters->{$word}->{'options'})
            {
              $help .= " ".COPS::text::uline($parameters->{$word}->{'options'});
            }
            else
            {
              $parameters->{$word}->{'type'} eq "i" and $help .= " ".COPS::text::uline("value");
              $parameters->{$word}->{'type'} eq "s" and $help .= " ".COPS::text::uline("string");
            }
            $help .= "$bc ";
          }
          else
          {
            $biglist .= "$bo--$word";
            defined($single) and $biglist .= "|-$single";
            $biglist .= "$bc ";
          }
        }
      }
      $biglist and $help .= COPS::text::bold("$biglist");
      $help .= "\n";
      
      
      if($cmd)
      {
        $help .= COPS::text::bold("\nDescription:\n");
        if(defined($cmds->{$key}->{'longdesc'}))
        {
          $help .= "  $cmds->{$key}->{'longdesc'}\n";
        }
        elsif(defined($cmds->{$key}->{'desc'})) # fallback
        {
           $help .= "  $cmds->{$key}->{'desc'}\n";
        }
      
      }
      else
      {
        $help .= "   $cmds->{$key}->{'desc'}\n";
        $help .= "\n"; # Extra newline for generic help page
      }
    }
  }

  # Per command help details
  if($cmds->{$cmd}->{'valid'})
  {
    $help .= COPS::text::bold("\nOptions:\n");

    my $params = COPS::CONFIG::parameters::list();
    
    my @param_list = @COPS::CONFIG::parameters::always_valid;
    push @param_list, @{$cmds->{$cmd}->{'valid'}};
    
    foreach my $name (sort {lc($a) cmp lc($b)} @param_list)
    {
      $help .= "  ";
      defined($params->{$name}->{'single'}) and $help .= COPS::text::bold("-$params->{$name}->{'single'}, ");
      $help .= COPS::text::bold("--$name");
      if($params->{$name}->{'type'})
      {
        $help .= " ";
        $params->{$name}->{'type'} eq "i" and $help .= COPS::text::uline("value");
        if($params->{$name}->{'type'} eq "s")
        {
          if($params->{$name}->{'options'})
          {
          $help .= COPS::text::uline(join("|",$params->{$name}->{'options'}));
          }
          else
          {
          $help .= COPS::text::uline("string");
          }
        }
      }
      $params->{$name}->{'default'} and $help .= " (Default '$params->{$name}->{'default'}')";
      $help .= "\n        $params->{$name}->{'desc'}\n\n";
    }
  }
  $help .= "\n";

  COPS::print::std($help);
}


1;
