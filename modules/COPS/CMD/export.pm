# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::export;
use strict;

sub entries
{
  # TODO: Allow different types of export encryption systems (eg, zipfile)
  my $dir = $COPS::args::argument;
  
  (-d $dir) or COPS::print::fatal("'$dir' is not a directory (or permission not found)");
  
  my $export_file = COPS::FUNC::file::sanitize("$dir/cops-export-".COPS::FUNC::time::stamp().".enc");
  
  (-e $export_file) and COPS::print::fatal("File '$export_file' already exists, will not overwrite");
  
  COPS::FUNC::file::write({target => "$export_file", content => "test"}); # Will bork on failure
  unlink($export_file);
  
  my $entries = COPS::FUNC::retrieve::all_entries(
  {
    user => $COPS::user::name,
    store => $COPS::args::param->{'store'},
    filter => $COPS::args::param->{'filter'},
    onlyme => $COPS::args::param->{'onlyme'}
  });

  my $passphrase = COPS::passphrase::get();
  
  my($header,@field_order) = export_format();

  my $exported=0;
  my $csv;

  COPS::print::info("Creating export ..... this may take some time\n");

  foreach my $name ( sort keys %{$entries} )
  {
    if($COPS::args::param->{'filter'})
    {
        (COPS::FUNC::filter::entry({"name" => $name, "filter" => $COPS::args::param->{'filter'}})) or next;
    }
    my $line;
    foreach my $type (@field_order)
    {
      if($type eq "entry")
      {
        $line .= '"'.$name.'",';
        next;
      }

      if($entries->{$name}->{$type}->{'file'})
      {
        my $file = COPS::FUNC::file::status($entries->{$name}->{$type}->{'file'});
        if($file->{'status'})
        {
            my $b64 = COPS::FUNC::retrieve::base64data($file->{'path'});
        
            my $content = COPS::FUNC::ssl::read({
                passphrase => $passphrase,
                privatekey => $COPS::args::param->{'privatekey'},
                base64 => $b64, 
                store => $COPS::args::param->{'store'}
            });

            if($content)
            {
                chomp($content);
                $content =~ s/\n/\\n/g; # Substitute newlines
                $content =~ s/\r/\\r/g; # Substitute CR's
                $content =~ s/"/\\"/g;

                $content =~ /\\\\/ and COPS::print::fatal("Found double escape characters in data stored in $entries->{$name}->{$type}->{'file'} OR sanity checking introduced double escape characters");
                $content = '"'.$content.'"';
                $line .= $content.",";
            }
            else
            {
                COPS::output::bad_passphrase();
            }
        }
        else
        {
            $line .= '"UNKNOWN FAILURE",';
        }
      }
      else
      {
        $line .= '"",';
      }
    }
    chop($line);
    $csv .= "$line\n";
    $exported++;
  }
  
  my $enckey = COPS::FUNC::crypt::create_enckey();
  
  my $enc=COPS::FUNC::crypt::cipher({"encrypt"=>1, "enckey"=>$enckey, "data"=>$header."\n".$csv});

  $enc = "# Decrypt this data with\n#   cat <this_file> | grep -v '\\#' | base64 -d | openssl enc -d -aes-256-cbc -$COPS::config::openssl_pbkdf\n# UUOC for readability\n# Will output on STDOUT, so pipe as required\n# Enter password when prompted\n$enc";
  COPS::FUNC::file::write({target => "$export_file", content => $enc});
  
  COPS::print::info("# Wrote file $export_file with $exported entries\n");
  COPS::print::info("# Encryption key follows\n");
  COPS::print::std($enckey."\n");
  
}


sub export_format
{
  my $header;
  my @cops_order;
  # Here we can define different formats for the export CSV depending on where we want to import it. Default is 'cops'
  # Format types are defined locally to allow users to define their own templates

  if($COPS::args::param->{'export-template'})
  {
    my $data = COPS::FUNC::file::readlocal($COPS::args::param->{'export-template'});
    foreach my $line (split(/\n/,$data))
    {
      $line =~ s/\s//g; # Strip whitespace
      $line =~ /^#/ and next; # Allow comments
      if($line =~/(.*)=(.*)/)
      {
        $header.= "$1,";
        push @cops_order, $2;

        my $ok=0;
        foreach(@COPS::config::allowed_entry_types,"entry")
        {
          $2 eq $_ and $ok = 1;
        }
        $ok or COPS::print::fatal("Invalid template field '$2' in $COPS::args::param->{'export-template'}");
      }
      else
      {
        $line =~ /[a-zA-Z0-9]/ or next;
        push @cops_order, '';
        $header.=$line.",";
      }
    }
  }
  else
  {
    $header .= "entry,";
    push @cops_order, "entry";
    foreach(@COPS::config::allowed_entry_types)
    {
      $header.= "$_,";
      push @cops_order,$_;
    }
  }
  chop($header);
  return ($header, @cops_order);
}



1;
