# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::general;
use strict;

# For those one or two line commands

sub version
{
  COPS::print::std("Cops version $COPS::config::version\n");
}

sub whoami
{
  # Simply reports who cops thinks you are!
  COPS::print::std("$COPS::user::name\n");
}

sub clearkeyring
{
  my $clear = COPS::keyring::clear();
  if($clear eq "empty")
  {
    COPS::print::notice("Keyring already empty\n");
  }
  elsif($clear eq "cleared")
  {
    COPS::print::std("Keyring cleared\n");
  }
  else
  {
    COPS::print::fatal("Failed to clear keyring. Investigate your system keyring and $COPS::args::param->{'userdir'}/cops.keyring/*.enc for reasons");
  }
}
  
sub clearfifo
{
  if(COPS::fifo::clear())
  {
    COPS::print::info("Stale entries removed\n");
  }
  else
  {
    COPS::print::fatal("Failed to remove stale fifo entries. Investigate '$COPS::args::param->{'userdir'}/cops.fifo/fifo_*' before proceeding");
  }
}
  
1;
