# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::get;
use strict;

sub entry
{
  my $entry = COPS::get::entry({ type => $COPS::args::param->{'type'}, text=>$COPS::args::argument });
  
  $entry or COPS::print::fatal("Unable to find a match for search term '$COPS::args::argument' with type '$COPS::args::param->{'type'}'");

  my $date='';
  if($entry->{'file'} =~ /.*\/(\d\d\d\d_\d\d_\d\d).*/)
  {
    $date=" from $1";
    $date =~ s/_/\//g;
  }

  COPS::print::info("Retrieving entry '$entry->{'name'}'$date\n");
  
  my $b64 = COPS::FUNC::retrieve::base64data($entry->{'file'});

  COPS::print::debug("Checking for verification");
  my $sign = COPS::FUNC::endorse::verify({
    data  => $b64,
    store => $COPS::args::param->{'store'}
  });
 
  my $passphrase = COPS::passphrase::get();
  COPS::print::debug("Retrieving entry");
  my $content = COPS::FUNC::ssl::read({
    passphrase => $passphrase,
    privatekey => $COPS::args::param->{'privatekey'},
    base64 => $b64, 
    store => $COPS::args::param->{'store'}
  });
  $content or COPS::output::bad_passphrase();
  
  $sign and COPS::output::verification($sign);

  $content =~ /##CC=/ and COPS::print::fatal("Internal use codes discovered in input. Rejected! Aborting!"); # Meh :(;

  $COPS::args::param->{'hibp'} and COPS::pwquality::hibp({password => $content});
 
  if($COPS::args::param->{'expirywarndays'} and $COPS::args::param->{'type'} eq "password")
  {
      my $expiry_file = COPS::FUNC::retrieve::file_by_id({
      id => $entry->{'id'}, 
      type => "expiry", 
      store => $COPS::args::param->{'store'}, 
      user => $COPS::user::name
    });
    if($expiry_file)
    {
      my $b64 = COPS::FUNC::retrieve::base64data($expiry_file);
      my $expiry_date = COPS::FUNC::ssl::read({
        passphrase => $passphrase,
        privatekey => $COPS::args::param->{'privatekey'},
        base64 => $b64, 
        store => $COPS::args::param->{'store'}
      });
      my $diff = COPS::FUNC::time::difference($expiry_date);
      if($diff < $COPS::args::param->{'expirywarndays'})
      {
        COPS::print::warn("Warning: This password will expire on $expiry_date!");
      }
    }
  }
 
 
  if($COPS::args::param->{'numbered'})
  {
    foreach my $i (split(",",$COPS::args::param->{'numbered'}))
    {
      my $char='Undefined';
      unless($i > length($content))
      {
        $char = substr($content,($i-1),1);
      }
      COPS::print::std("Character ".sprintf("%-3s",$i)." : $char\n");
    }
  }
  else
  {
    COPS::output::entry($content);
  }
 
}

1;
