# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::keychange;
use strict;

sub start
{
  my $entries = COPS::FUNC::retrieve::all_entries({user => $COPS::user::name, store => $COPS::args::param->{'store'}});
  my $passphrase = COPS::passphrase::get();
  my $publickey = COPS::keys::get_public();
  
  COPS::print::info("\nThis command will update your copstore by updating all available entries using\n  - public key '$COPS::args::param->{'publickey'}' (encryption) and\n  - private key '$COPS::args::param->{'privatekey'}' (decryption)\n");
  COPS::print::info("You would only do this to change the keypair you use with cops!\n\n");
  COPS::print::std("Type 'understood!' to continue: ");
  my $answer=COPS::FUNC::input::line();
  
  chomp($answer);
  (lc($answer) ne "understood!") and COPS::print::fatal("Aborting!");
  
  foreach my $name ( keys %{$entries} )
  {
    foreach my $type (@COPS::config::allowed_entry_types)
    { 
      $entries->{$name}->{$type}->{'file'} or next;
      my $file = COPS::FUNC::file::status($entries->{$name}->{$type}->{'file'});
      $file->{'status'} or next;

      COPS::print::debug("Checking age");
      if($COPS::args::param->{'olderthan'})
      {
        ($file->{'age'} < $COPS::args::param->{'olderthan'}*86400) and next;
      }
      
      COPS::print::info("Updating $name/$type\n");
      
      my $b64 = COPS::FUNC::retrieve::base64data($file->{'path'});
      
      my $content = COPS::FUNC::ssl::read({
        passphrase => $passphrase,
        privatekey => $COPS::args::param->{'privatekey'},
        base64 => $b64, 
        store => $COPS::args::param->{'store'}
      });
      
      $content or COPS::output::bad_passphrase();

      my $enc = COPS::FUNC::ssl::encrypt({
	  data => $content,
	  publickey => $publickey,
      });
	
      my $target = COPS::FUNC::time::stamp('long')."_".$COPS::user::name.".$type";
      $target = "$COPS::args::param->{'store'}/$name/$target";

      $target = COPS::FUNC::file::sanitize($target);
      
      COPS::FUNC::file::write({
	content => $enc,
	target  => $target
      });
    }
  }
  COPS::print::std("Keychange completed. Please update your default public/private keys in your store as required.");
}

1;
