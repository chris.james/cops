# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::search;
use strict;

sub entries
{
  my $entries = COPS::FUNC::retrieve::all_entries({user => $COPS::user::name, store => $COPS::args::param->{'store'}});
  my $passphrase = COPS::passphrase::get();
  
  my $entry = $COPS::args::argument;
  unless($entry)
  {
    COPS::print::std("Enter data to search for: ");
    $entry=COPS::FUNC::input::line();
    chomp($entry);
  }
  $COPS::args::param->{'exactmatch'} and $entry="^$entry\$";
  
  my $type = $COPS::args::param->{'type'};
  
  foreach my $name ( sort keys %{$entries} )
  {
      $entries->{$name}->{$type}->{'file'} or next;
      my $file = COPS::FUNC::file::status($entries->{$name}->{$type}->{'file'});
      $file->{'status'} or next;
      
      COPS::print::info("Searching: $name\n");
      
      my $b64 = COPS::FUNC::retrieve::base64data($file->{'path'});
      
      my $content = COPS::FUNC::ssl::read({
        passphrase => $passphrase,
        privatekey => $COPS::args::param->{'privatekey'},
        base64 => $b64, 
        store => $COPS::args::param->{'store'}
      });

      $content or COPS::output::bad_passphrase();
      
      if($content =~ /$entry/i)
      {	
        COPS::print::std("Match found in '$name'\n");
      }
  }
}

1;
