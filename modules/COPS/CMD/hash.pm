# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::hash;
use strict;

sub entry
{
  my $data = $COPS::args::argument;
  unless($data)
  {
    $data = COPS::FUNC::input::noecho("Enter data to hash");
  }
  #COPS::print::std("\n");
  COPS::output::hashed($data);
  if($COPS::args::param->{'show'})
  {
    COPS::print::info("Data: ");
    COPS::print::std("$data\n");
  }
}
  
1;
