# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::update;
use strict;

sub entry
{
  my $entry = $COPS::args::argument;
    
  # Taint sanitisation
  ($entry =~ /^([\-\w\_\.]+)$/ and $entry = $1) || COPS::print::fatal("Entry name failed sanitization. Only 0-9,a-Z,periods and underscores allowed.");
  if($entry =~ /^(\d+)$/)
  {
    $entry = COPS::FUNC::retrieve::name_by_id({
        id => $entry, 
        store => $COPS::args::param->{'store'}, 
        user => $COPS::user::name
      });
  }
  else
  {
    $entry =~ /\.\./ and COPS::print::fatal("Invalid double dots found in entry name.");
    
    # We search through all types to see if an entry exists.
    my $get;
    foreach my $type (@COPS::config::allowed_entry_types)
    {
      $get = COPS::get::entry({ type => $type, text => $entry });
      $get and last;
    }
    $get or COPS::print::fatal("No existing entry found for '$entry'. Perhaps you need to use add rather than update ?");
    $entry = COPS::FUNC::retrieve::name_by_id({id => $get->{'id'}, store => $COPS::args::param->{'store'}, user => $COPS::user::name});
  }
  
  $entry =~ /^(\d+)$/ and COPS::print::error("Entry name cannot consist entirely of numbers. Code failure; this should have been caught before this point!");
  
  COPS::print::info("Updating entry '$entry'\n");
  
  my $data = COPS::store::entry($entry);
  $COPS::config::inputtype->{$COPS::args::param->{'type'}} eq "secret" and COPS::output::entry($data);

}

1;
