# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::dump;
use strict;

sub params
{
  COPS::print::debug("Dumping current stanza");
  my $br = "-------------------------------------------------------------------------------------";
  COPS::print::std("$br\nDumping current config\n$br\n");
  foreach my $key (sort keys %{$COPS::args::param})
  {
    my $value;
    if(defined($COPS::args::param->{$key}))
    {
      $value = $COPS::args::param->{$key};
    }
    else
    {
      $value = '[unset]';
    }
    COPS::print::std(sprintf("%-20s",$key)." | $value\n")
  }
  COPS::print::std("$br\n");
  COPS::print::debug("Finished dump");
}

1;

