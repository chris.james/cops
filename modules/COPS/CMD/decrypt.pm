# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::decrypt;
use strict;

sub entry
{
  my $passin;

  if($COPS::args::argument)
  {
    # A filename has been passed
    my $file = COPS::FUNC::file::status($COPS::args::argument);
    if($file->{'status'})
    {
      $passin->{'base64'}=COPS::FUNC::retrieve::base64data($file->{'path'});
    }
    else
    {
      COPS::print::fatal("Supplied file path '$COPS::args::argument' does not exist!");
    }
  }
  else
  {
    if($COPS::args::param->{'stdin'})
    {
      $passin->{'base64'} = COPS::FUNC::input::stdin();
    }
    else
    {
      COPS::print::std("Enter base64 encoded data (Control-D to finish)\n");
      my $data = COPS::FUNC::input::wall();
      $data =~ s/[^a-zA-Z0-9\+\/\=\n\-\(\)\#\.\s]//g;
      ($data =~ /^([a-zA-Z0-9\+\/\=\n\-\(\)\#\.\s]+)$/ and $data = $1) or COPS::print::fatal("Unknown characters detected in input");
      $passin->{'base64'}=$data;
    }
  }
  
  # We will do a check before passing to the ssl module that this is actually cops data
  # This breaks compatibility with pre V2 of cops, but it's better than 
  # the base64 decrypt borking on the cli
  COPS::FUNC::ssl::getversion($passin->{'base64'}) or COPS::print::fatal("Invalid data block passed to 'decrypt'");
  
  
  $passin->{'passphrase'}=COPS::passphrase::get();
  $passin->{'privatekey'}=$COPS::args::param->{'privatekey'};

  my $sign = COPS::FUNC::endorse::verify({
    data=> $passin->{'base64'},
    key=> $COPS::args::param->{'verifywith'}
  });
  
  my $password = COPS::FUNC::ssl::read($passin);
  
  if($password)
  {
    $sign and COPS::output::verification($sign);
    COPS::output::entry($password);
    $COPS::args::param->{'crypt'} and COPS::output::hashed($password);
  }
  else
  {
    COPS::output::bad_passphrase();
  }
  
}
 
1; 


