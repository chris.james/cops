# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CMD::addkey;
use strict;

sub rsa
{
  my $key = $COPS::args::argument;
  my $keycheck = COPS::FUNC::file::status($key);
  $keycheck->{'status'} or COPS::print::fatal("Specified key '$key' does not exist");
  
  my $pem = COPS::keys::convert($keycheck->{'path'});
  
  my $file = $COPS::args::param->{'store'}."/.cops_keys/$COPS::user::name.pem";
  $file = COPS::FUNC::file::sanitize($file);
  
  COPS::FUNC::file::writelocal({file => $file, content => $pem});
  COPS::print::notice("Added key for '$COPS::user::name' to store '$COPS::args::param->{'store'}'\n");
}

1;