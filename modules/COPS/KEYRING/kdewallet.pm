# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::KEYRING::kdewallet;

sub handle
{
    my $arg = shift;
    
    $arg->{'app'} = "Cops (KDE Wallet interface)";
    $arg->{'wname'} = "kdewallet";
    
    my $qdbus = COPS::FUNC::which::binary("qdbus");
    my $pgrep = COPS::FUNC::which::binary("pgrep");
    my $grep = COPS::FUNC::which::binary("grep");
    COPS::FUNC::exec::cmd("$pgrep kwalletd") or COPS::print::fatal("KDEWallet background process not running and wallet=kdewallet specified. Aborting\n");

    my $call = '';
    
    my $kw = COPS::FUNC::exec::cmd("$qdbus | $grep org.kde.kwalletd");
    chomp($kw);
    $kw =~ /(org.kde.)(kwalletd.*)/ and $kw = $2;
    $kw or COPS::print::error("Failed to find kwallet org");

    $call = "$qdbus org.kde.$kw /modules/$kw org.kde.KWallet.";
    
    my $k = {
      "open"            => $call.'open ##wname## 0 "##app##"',
      "close"           => $call.'close ##wname## ##id##',
      "foldercreate"    => $call.'createFolder ##id## "CopsKeys" "##app##"',
      "folderlist"      => $call.'folderList ##id## "##app##"',
      "write"           => $call.'writePassword ##id## "CopsKeys" "##key##" "##value##" "##app##"',
      "read"            => $call.'readPassword ##id## "CopsKeys" "##key##" "##app##"',
      "delete"          => $call.'removeEntry ##id## "CopsKeys" "##key##" "##app##"',
      "list"            => $call.'entryList  ##id## "CopsKeys" "##app##"'
    };
    
    my $kde = $k->{$arg->{'action'}};
    
    # We do this substitution method in order to avoid use of non-initialised values from $arg ( i.e. if we pre-populated the above calls )
    foreach my $field ("id","key","value","app","wname")
    {
      if($kde =~ /##$field##/)
      {
        $arg->{$field} or COPS::print::error("Substitution fail in kdewallet string '$kde'");
        $kde =~ s/##$field##/$arg->{$field}/g;
      }
    }
    my $result = COPS::FUNC::exec::cmd($kde);
    return $result;
}

#############################################################################################
sub getid
{
  # Initilise kdewallet if required. Get ID of current session
  my $id   = COPS::KEYRING::kdewallet::handle({"action" => "open"});
  chomp($id);
  ($id =~ /^([0-9]+)$/ and $id = $1) or COPS::print::fatal("Failed to open kdewallet");
  
  $id > 0 or COPS::print::fatal("Failed to open kdewallet");
  
  my $fl   = COPS::KEYRING::kdewallet::handle({"action" => "folderlist", "id" => $id});
  my $exists;
  foreach(split(/\n/, $fl))
  {
    /^CopsKeys$/ and $exists=1;
  }
  unless($exists)
  {
    COPS::KEYRING::kdewallet::handle({"action" => "foldercreate", "id" => $id});
  }
  return $id;
}

sub get_passphrase
{
  my $id = COPS::KEYRING::kdewallet::getid();
    
    # Read the encryption key stored against the current ID if there is one
    my $ppek = COPS::KEYRING::kdewallet::handle({"action" => "read", "id" => $id, "key" => $id});
    chomp($ppek);
    
    # If there is a current key, read our encryted passphrase file and decrypt it
    if($ppek =~ /^[a-zA-Z0-9]+$/ and -e "$COPS::args::param->{'userdir'}/cops.keyring/$id.enc")
    {
      my $pin = COPS::keyring::get_pin("current");
      ($ppek =~ /^([a-zA-Z0-9]+)$/ and $ppek = $1) || COPS::print::fatal("Bad characters found in ppek '$ppek'"); # Regexp for untaint
      
      $ppek .= $pin;
      
      my $enc = COPS::FUNC::file::read("$COPS::args::param->{'userdir'}/cops.keyring/$id.enc");
      chomp($enc);

      my $pp = COPS::FUNC::crypt::cipher({"decrypt" => 1, "enckey" => $ppek, "data" => $enc});
      $pp =~ s/^(.*)\n.*$/$1/; # More untaint
      return $pp;
    }
    
    # If there isn't a current key, create one
    else
    {
      # Clear all enc pps in store as they are now legacy
      COPS::KEYRING::kdewallet::clear();      
     
      # Warn user that pp has expired, and prompt
      COPS::print::warn("Your kwallet cops session has expired");
      
      my $pp = COPS::passphrase::enter();
      $pp.="\n";
      while(length($pp) <= 256) # Pad out to 256 characters
      {
        $pp.= COPS::FUNC::rng::alpha("/dev/urandom");
      }
                  
      my $enckey = COPS::FUNC::crypt::create_enckey();
      my $pin = COPS::keyring::get_pin("new");
      
      # Requires untaint
      ($enckey =~ /^([a-zA-Z0-9]*)$/ and $enckey = $1) || COPS::print::fatal("Bad characters found in enckey '$enckey'");
      
      # Store the enckey in wallet
      COPS::KEYRING::kdewallet::handle({"action" => "write", "id" => $id, "key" => $id, "value" => $enckey});
     
      # And encrypt the passphrase
      my $enc=COPS::FUNC::crypt::cipher({"encrypt"=>1, "enckey"=>$enckey.$pin, "data"=>$pp});
            
      # Store pp
      COPS::FUNC::file::write({target => "$COPS::args::param->{'userdir'}/cops.keyring/$id.enc", content => $enc});
      
      # Return pp
      return $pp;
    }
}
      
sub clear
{
  my $id=COPS::KEYRING::kdewallet::getid();
  my $list = COPS::KEYRING::kdewallet::handle({"action" => "list", "id" => $id});
  
  # Clear all copskeys in kdewallet
  if($list)
  {
    foreach my $key (split(/\n/, $list))
    {
      ($key =~ /^([0-9]+)$/ and $key = $1) or COPS::print::fatal("Got an unrecogised key id of '$key' back from kdewallet. Clear the CopsKeys folder manually in kdewallet to resolve");
      COPS::KEYRING::kdewallet::handle({"action" => "delete", "id" => $id, "key" => $key});
    }
  }
  COPS::KEYRING::kdewallet::handle({"action" => "list", "id" => $id}) and COPS::print::fatal("Failed to clear all copskeys entries from kdewallet.");
      
  # And clear all local values
  my @keyrings = glob("$COPS::args::param->{'userdir'}/cops.keyring/*.enc");
  if(@keyrings)
  {
    foreach(@keyrings)
    {
      COPS::FUNC::file::delete($_);
    }
    return "cleared";
  }
  else
  {
    return "empty";
  }
}



1;
