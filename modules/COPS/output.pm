# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::output;
use strict;

sub entry
{
  my $entry = shift;
  
  if($COPS::args::param->{'autochomp'})
  {
    chomp($entry);
  }
  
  if($COPS::args::param->{'buffer'})
  {
    COPS::print::debug("Sending to clipboard");
    COPS::clipboard::buffer($entry);
  }
  elsif($COPS::args::param->{'xdo'})
  {
    COPS::print::debug("Sending to xdo");
    COPS::xdo::type($entry);
  }
  elsif($COPS::args::param->{'pipeto'})
  {
    COPS::print::debug("Sending to pipe '$COPS::args::param->{'pipeto'}'");
    my $file = COPS::FUNC::file::status($COPS::args::param->{'pipeto'});
    $file->{'status'} or COPS::print::fatal("Cannot pipe to non-existent handle '$COPS::args::param->{'pipeto'}'");
    
    COPS::FUNC::file::pipeto({file => $file->{'path'}, content => $entry})
  }
  elsif($COPS::args::param->{'writeto'})
  {
    COPS::print::debug("Sending to '$COPS::args::param->{'writeto'}'");
    my $file = COPS::FUNC::file::status($COPS::args::param->{'writeto'});
    
    # We don't care if the file exists
    COPS::FUNC::file::writelocal({file => $file->{'path'}, content => $entry})
  }
  # TODO: Add output to a UNIX socket directly?
  
  if($COPS::args::param->{'show'})
  {
    COPS::print::debug("Sending to stdout");
    COPS::print::std("$entry\n");
  }
  else
  { 
    COPS::print::info("Output was ".length($entry)." characters\n");
  }
  
  if($COPS::args::param->{'crypt'})
  {
    if(($COPS::args::param->{'crypt'} eq "mysql") and (length($entry) > 80))
    {
      COPS::print::warn("You have generated what appears to be a mysql password > 80 characters in length. The standard mysql client library has a 80 character limit");
    }
    COPS::output::hashed($entry);
  }
}

sub bad_passphrase()
{
  if($COPS::args::param->{'keyring'})
  {
    if($COPS::args::param->{'autoclearkeyring'})
    {
      COPS::print::warn("Keyring cleared!");
      COPS::keyring::clear();
    }
    else
    {
      COPS::print::warn("Keyring not cleared. Use 'cops clearkeyring' or set autoclearkeyring in ~/.copsrc or on the command line");
    }
  }
  COPS::print::fatal('Unable to load private key. Most likely cause is an incorrect passphrase.');
}
  
sub verification
{
  my $sign = shift;
  if($sign eq "notverified")
  {
    COPS::print::warn("Endorsement found, but no publickkey specified via --verifywith to confirm identity");
  }
  elsif($sign)
  {
    if($sign =~ /FAIL/i)
    {
      COPS::print::warn($sign);
    }
    else
    {
      $COPS::args::param->{'quiet'} or COPS::print::notice($sign."\n");
    }
  }
  else
  {
    # Do nothing. No endorsement found
  }
}
  
sub hashed
{
  my $data = shift;
  my $crypt = (COPS::FUNC::hash::crypt({data => $data, algo => $COPS::args::param->{'crypt'}, salt => $COPS::args::param->{'salt'}, nosalt => $COPS::args::param->{'nosalt'}}));
  
  # NOTE: This *could* go to the secret output and allow for piping to buffers etc. However, hashes *should* be non secret :)
  COPS::print::info("$COPS::args::param->{'crypt'} hash: ");
  COPS::print::std("$crypt\n");
}
  
1;
