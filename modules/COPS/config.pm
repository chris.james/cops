# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::config;
use strict;

# Major.Minor.Bugfix
# As of v1.27, odd minor numbers are development releases. Even minor numbers are stable

my $changelog = COPS::FUNC::file::read($COPS::root."/CHANGELOG.md");

our $version = 'UNKNOWN';

foreach(split(/\n/,$changelog))
{
  if(/##\s*([\d\.]+)\s\[(\d\d\d\d\/\d\d\/\d\d)\]/)
  {
    $version = $1." [$2]";
    last;
  }
}

our $nogui=0;
(-e ($COPS::root."/.nogui")) and $nogui=1; # File is only added in the -nogui rpm package

our $commit_age = 3600; # Number of seconds that we allow a file to age in the password store before we warn that it is untracked in git ( applies only to git controlled password stores )

our $padsize = 32; # Minimum number of random characters that we add to an entry prior to encrypting it. This is a simpler method than CBC in this instance and I don't believe we need the integrity provided by CBC on this size of data chunk

our $datafile_version = "2.0"; # As of cops v 1.36 we now tag datafiles with a version string. Increment if you change the formatting of written data and ensure you keep backwards compatability!

our $keysize_warn = 4096; # When we encrypt using a key that is SMALLER than this value, print a warning.

# Allow for new types to be easily added
our @allowed_entry_types = ("password","username","notes","expiry");

# And their data types. We default to 'normal', then explicitly set to 'secret' as required. Secret just means there is no echo on input with --nogen
our $inputtype;
foreach(@allowed_entry_types)
{
  $inputtype->{$_} = 'normal';
}
$inputtype->{'password'} = 'secret';

# Available flags to xsel buffer type
our @allowed_buffer_types = ("p","s","b");

# Defined types in hash.pm
our @allowed_crypt_types = ("sha512","bcrypt","sha1","md5","mysql","lanman","ntmd4");

our @supported_keyrings  = ("kdewallet","none");

our $rngsource='/dev/random'; # Default RNG

our $temp=''; # Ugh. OK, this breaks our model somewhat as this is defined as a param, but needs to be referenced in FUNC. No simple way round it though :(

our $copsrc; # For copsrc overrides. These should only be referenced in args.pm where they are itterated through to overide default options. An execption to that would be if you want a parameter that can only be set in copsrc and not on the commandline

our $colours = ({
  debug   => 'magenta',
  info    => 'green',
  std     => 'white',
  notice  => 'cyan',
  warn    => 'bold yellow',
  fatal   => 'red',
  error   => 'bold red',
});

our $jumpmethod; # For defining particular jumpmethods in copsrc

my $rc = $ENV{"HOME"} . "/.copsrc";
# First, allow overrides to defaults in ~/.copsrc prior to reading in cli args
# We do this here because some things like 'noansi' may need to be set before entering the cli args routine

if(-T "$rc")
{
  # This is the only time we should use 'die' rather than 'COPS::print::error'
  # and actually open a file rather than using the FILE handler
  # as we have not yet loaded print.pm or file.pm
  open(FH, "< $rc") or die("ERROR: ~/.copsrc exists but is not readable");

  while(<FH>)
  {
    chomp();
    unless(/^(#|\s*#)/ or /^\s*$/) # Ignore comments and blank lines
    {
      my $line = $_;
      $line =~ s/^(\s*)(.*)/$2/;
      # First, deal with ones where we don't want to actually use the data as a parameter value
      if($line =~ /^colour\-(\w*?)=(.*)/)
      {
        $colours->{$1}=$2; 
      }
      elsif($line =~ /^jumpmethod\-([\w\-\.\_]*?)=(.*)/)
      {
        $jumpmethod->{$1}=$2;
      }
      elsif($line =~ /^warnwait/)
      {
        COPS::print::warn("Deprecated parameter 'warnwait' found in ~/.copsrc");
      }
      # Now handle like parameters
      elsif($line =~ /^(\w*)=(false|no|off)/i)
      {
        $copsrc->{$1}=0;
      }
      elsif($line =~ /^([\w\-]*)=(.*)/)
      {
        $copsrc->{$1}=$2;
      }
      else
      {
        $copsrc->{$line}=1;
      }
    }
  }
}

our $internal_timeout = 15; # Seconds to wait till timeout on an internal operation.

our $keysym; # Used when defining an autotype jump sequence
  $keysym->{'t'}="Tab";
  $keysym->{'n'}="Return";
  $keysym->{'l'}="Left";
  $keysym->{'r'}="Right";
  $keysym->{'u'}="Up";
  $keysym->{'d'}="Down";
  $keysym->{'h'}="Home";
  $keysym->{'e'}="End";

# For the paramater help page
our $keysymlist;
foreach(keys %{$keysym})
{
  $keysymlist .= "\t\t#$_ - $keysym->{$_}\n";
}

# Do a version check on openssl, and vary some config based on that detail
my $v = COPS::FUNC::which::version('openssl');

our $openssl_pbkdf = 'opensslv1';
our $openssl_enc = 'rsautl';

if ($v =~ /^(\d+)\.(\d+)/)
{
  if( $1 > 1 )
  {
    $openssl_pbkdf = 'pbkdf2';
  }
  elsif($1 == 1 and $2 >=1)
  {
    $openssl_pbkdf = 'opensslv2';
  }
  elsif($1 < 1)
  {
    COPS::print::fatal("Openssl version is < 1.0 - Too many things will either not work, or will be insecure");
  }
  else
  {
    $openssl_pbkdf = 'opensslv1';
  }

  if($1 >= 3)
  {
    $openssl_enc = 'pkeyutl';
  }
}

require COPS::CONFIG::commands;
require COPS::CONFIG::arguments;
require COPS::CONFIG::parameters;

1;
