# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::xdo;
use strict;

my $xdo;
my $xwin;

sub type
{
  my $text = shift;
  chomp($text);

  $text =~ /[']/ and COPS::print::fatal("Cannot allow a single quote in data when using xdo. Sorry.");

  if($text =~ /([£\{\}\[\]\@~#<>\|])/)
  {
    COPS::print::warn("Some characters in parsed text *may* not auto-type correctly into some window managers");
    COPS::print::debug("First failed character is '$1'");
  }

  my $options = shift;
  $xdo = COPS::FUNC::which::binary('xdotool'); # Check that the binary exists
  $xwin = COPS::FUNC::which::binary('xwininfo');
  COPS::print::notice("Preparing ..... WAIT!\n");
  sleep(1); # So, with the cops2 rewrite I found that the name of window can change during the XDO call to reflect bash rather than cops. This sleep fixes that.
  my($current, $current_name) = getWindow();
  
  if($COPS::args::param->{'klick'})
  {
    COPS::print::info("Click on window to type into\n");
  }
  else
  {
    COPS::print::info("Focus on target window to auto-type text into ($COPS::args::param->{'countdown'} second delay after gaining focus)\n");
  }

  while(1)
  {
    my($w,$name) = COPS::xdo::getWindow();
    unless($w == $current and ($name eq $current_name))
    {
      COPS::print::debug("$w == $current and ('$name' eq '$current_name') no longer matches");
      COPS::print::out({'text' => "Control-C to cancel auto-type", 'type' => 'notice', 'time' => $COPS::args::param->{'countdown'} }); 
      
      COPS::print::notice("Auto-typing into '$name'\n");

      my $occ = $COPS::args::param->{'xdo'};
      while($occ)
      {
        COPS::print::debug("Itteration $occ");
        $occ--;
        if($text =~ /\\/)
        {
          # Contains special characters ( tab/enter/etc )
          while($text =~ /^(.*?)\\(.)(.*)$/)
          {
            $text = $3;
            COPS::print::debug("Sending text '$1'");
            sendviafifo($1);
            $COPS::config::keysym->{$2} or COPS::print::fatal("Invalid seperator of '$2' specified. See help for valid seperators");

            COPS::print::debug("Sending keysym '$2/$COPS::config::keysym->{$2}'");
            COPS::FUNC::exec::cmd("$xdo key --clearmodifiers $COPS::config::keysym->{$2}",{syscall => 1});
          }
        }
        else
        {
          sendviafifo($text);
        }
        $occ>0 and sendviafifo("\t");
      }
      return;
    }
  }
}

sub sendviafifo
{
  my $text = shift;
  
  my $fifo = COPS::fifo::create();
  COPS::fifo::make($fifo);
  
  my $out = "type --clearmodifiers --delay $COPS::args::param->{'xdo-delay'} '$text'";

  my $pid = fork();
  if($pid)
  {
    # Parent process
    my $cmd = "$xdo - < $fifo";
    COPS::FUNC::exec::cmd("$cmd",{syscall => 1});
    COPS::fifo::remove($fifo);
    return;
  }
  else
  {
      if(-p $fifo)
      {
        COPS::print::debug("Sending \"$out\" to fifo\n");
        COPS::fifo::write($fifo,$out);
        exit(0);
      }
      else
      {
        # Parent process has killed off the pipe
        exit(1);
      }
  }
}
      
sub getWindow
{
  my $id;
  my $name;
  until($id)
  {
    my $xdoarg = "getactivewindow";
    $COPS::args::param->{'klick'} and $xdoarg = "selectwindow";
    $id = COPS::FUNC::exec::cmd("$xdo $xdoarg 2>&1");
    chomp($id);
    
    # xdo seems to get twitchy sometimes and not pick up a window ID on the first try.
    # This used to generate secerrors, so we now ignore faulty entries.
    ($id =~ /^([0-9]+)$/ and $id = $1) or $id=0;
  }
  $name = COPS::FUNC::exec::cmd("$xwin -id '$id' 2>&1 | grep xwininfo | cut -f 2 -d '\"'");
  chomp($name);
  COPS::print::debug("Current active window is $id/$name");
  return ($id,$name);
}
  
  
1;
