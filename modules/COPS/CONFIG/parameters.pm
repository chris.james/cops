# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CONFIG::parameters;
use strict;

# Parameters that are likely to be defined in ~/.copsrc and we don't want to warn that they aren't valid for commands that don't actually need them
our @global = (
  "debug",
  "noansi",
  "quiet",
  "silent",
  "stagewhisper",
  "noprompts",
  "noshortcommands",
  "termux",
  "keyring",
  "keyringpin",
  "userdir",
  "expirywarndays",
  "store",
  "help",
  "use-asterixes",
  "easygen"
); 

# Paramaters that will always be valid and should be listed in help output
our @always_valid = (
  "debug",
  "noansi",
  "quiet",
  "silent",
  "stagewhisper",
);

# TODO: BUG: Any parameter that has a default setting is automatically accepted in the checks in COPS::args::check_valid. We don't want this as it allows users to supply ignored parameters. It's a style thing!

sub list
{ 
  my $params;
  # Type is s for string, i for int. Otherwise no value is allowed ( boolean only )

  $params->{'paranoia'}->{'desc'} = 'Paranoia level for password quality checks.';
  $params->{'paranoia'}->{'options'} = 'amateur|professional|state';
  $params->{'paranoia'}->{'default'} = 'professional';
  $params->{'paranoia'}->{'type'} = 's';

  $params->{'alphanumeric'}->{'single'} = 'a';
  $params->{'alphanumeric'}->{'desc'} = 'Only use alphanumerics [0-9a-zA-Z], i.e. disregard all special characters on generation. This option only applies when used with a random character password ( --randomchar )';
  $params->{'alphanumeric'}->{'default'} = '0';
  
  $params->{'noansi'}->{'single'} = 'A';
  $params->{'noansi'}->{'desc'} = 'Suppress the usage of Ansi colour encoding';
  $params->{'noansi'}->{'default'} = '0';

  $params->{'buffer'}->{'single'} = 'b';
  $params->{'buffer'}->{'desc'} = 'Add the output to your clipboard buffer. Default is to use xsel, unless --termux is set. Supresses output on stdout for a "get". Argument is a required buffer type to pass to xsel ( typically "b". Discarded if --termux is set). The previous contents of the clipboard are restored on exit.';
  $params->{'buffer'}->{'type'} = 's';
  $params->{'buffer'}->{'options'} = join("|",@COPS::config::allowed_buffer_types);
  $params->{'buffer'}->{'nogui'} = '1';
  
  $params->{'backwards'}->{'single'} = 'B';
  $params->{'backwards'}->{'desc'} = 'Go back a number of entries. eg, to retrieve an entry two itterations older then the current one use --backwards 2. Useful for retrieving old passwords';
  $params->{'backwards'}->{'type'} = 'i';
  $params->{'backwards'}->{'default'} = 0;

  $params->{'crypt'}->{'single'} = 'c';
  $params->{'crypt'}->{'desc'} = 'Generate a one way hash of the retrieved data or input, and show the result. Uses a randomised salt where applicable. Use --nosalt to generate an unsalted md5 hash.';
  $params->{'crypt'}->{'type'} = 's';
  $params->{'crypt'}->{'options'} = join("|",@COPS::config::allowed_crypt_types);

  $params->{'countdown'}->{'single'} = 'C';
  $params->{'countdown'}->{'desc'} = 'When used with --xdo specifies the number of seconds delay once focus is shifted prior to autotyping buffer contents';
  $params->{'countdown'}->{'type'} = 'i';
  $params->{'countdown'}->{'default'} = '2';
  $params->{'countdown'}->{'nogui'} = '1';

  $params->{'stdin'}->{'single'} = 'd';
  $params->{'stdin'}->{'desc'} = 'Provide data via stdin (eg via a pipe). Implies --nogen when used in conjunction with password generation, but the user will not be prompted and local echo will NOT be disabled.';
  $params->{'stdin'}->{'default'} = '0';

  $params->{'debug'}->{'single'} = 'D';
  $params->{'debug'}->{'desc'} = 'Run in debug mode. Warning, can expose sensitive data on the CLI!';
  $params->{'debug'}->{'default'} = '0';

  $params->{'exactmatch'}->{'single'} = 'e';
  $params->{'exactmatch'}->{'desc'} = 'When retrieving an entry, use your supplied argument as an exact match. EG, "mgmt" will no longer match entries such as "sysmgmt"';
  $params->{'exactmatch'}->{'default'} = '0';
  
  $params->{'expirywarndays'}->{'desc'} = 'When retrieving an entry, check the expiry date and warn if it is due to expire within the number of days specified';
  $params->{'expirywarndays'}->{'default'} = '0';
  $params->{'expirywarndays'}->{'type'} = 'i';

  $params->{'endorse'}->{'single'} = 'E';
  $params->{'endorse'}->{'desc'} = 'Sign the entry with your ssh private key. This can then be validated by other users.';
  $params->{'endorse'}->{'default'} = '0';
    
  $params->{'fuzzymatch'}->{'single'} = 'f';
  $params->{'fuzzymatch'}->{'desc'} = 'Counteract --exactmatch behaviour, specifically if you have implemented --exactmatch in your copsrc';
  $params->{'fuzzymatch'}->{'default'} = '0';

  # TODO: Breaking change - remove single as required to free it up as probably only useful in copsrc
  $params->{'autoclearfifo'}->{'single'} = 'F';
  $params->{'autoclearfifo'}->{'desc'} = 'If stale fifo\'s are found, clear them automatically rather than the default behaviour of prompting the user';
  $params->{'autoclearfifo'}->{'default'} = '0';

  $params->{'salt'}->{'single'} = 'g';
  $params->{'salt'}->{'desc'} = 'Manually specify the salt for generation of a password hash, rather than generate one randomly. This is useful for checking against a known hash with a known password.';
  $params->{'salt'}->{'type'} = 's';

  $params->{'nosalt'}->{'single'} = 'G';
  $params->{'nosalt'}->{'desc'} = 'Do not use a salt for generating a hash. Used specifically with md5 hashes to create a checksum';
  $params->{'nosalt'}->{'default'} = '0';

  $params->{'no-hyphens'}->{'single'} = 'H';
  $params->{'no-hyphens'}->{'desc'} = 'Suppress the usage of hyphens to seperate words';
  $params->{'no-hyphens'}->{'default'} = '0';

  # TODO: Breaking change - change to numerIc
  $params->{'pin'}->{'single'} = 'i';
  $params->{'pin'}->{'desc'} = 'Only use numbers. This option only applies when used with a random character password ( --randomchar )';
  $params->{'pin'}->{'default'} = '0';

  $params->{'insensitive'}->{'single'} = 'I';
  $params->{'insensitive'}->{'desc'} = 'Do not capitalise the first letter of each word when creating a wordbank password. Do not use capitals when creating a random character password.';
  $params->{'insensitive'}->{'default'} = '0';

  $params->{'jump'}->{'single'} = 'j';
  $params->{'jump'}->{'desc'} = 'When using autofill, specify the number of tabs to seperate the username and password';
  $params->{'jump'}->{'type'} = 'i';
  $params->{'jump'}->{'default'} = '1';
  
  $params->{'jumpmethod'}->{'single'} = 'J';
  $params->{'jumpmethod'}->{'desc'} = "Instead of using tabs to seperate lines when using autofill, describe your own method. Formatting string uses [password] and [username] to denote variables. Use any other characters you wish and the following special codes
  
  $COPS::config::keysymlist 
  \t\tExamples:
  \t\t--jumpmethod='somedomain+[username]#t[password]#t#n'
  \t\t--jumpmethod='[password]#t#t#n'";
  
  $params->{'jumpmethod'}->{'type'} = 's';
  
  $params->{'klick'}->{'single'} = 'k';
  $params->{'klick'}->{'desc'} = 'When using xdo, use the built in mouse click function to select the window you want to type into, rather than auto detecting a change in focus. Very useful if you are a sloppy-focus user. Warning: Does not work in kde/plasma!';
  $params->{'klick'}->{'default'} = '0';
  $params->{'klick'}->{'nogui'} = '1';

  $params->{'keyring'}->{'single'} = 'K';
  $params->{'keyring'}->{'desc'} = 'Use a system keyring for secure storage of your ssh key passphrase. Timeouts and storage criteria are defined by your keyring manager. Currently, only "kdewallet" is currently supported as a keyring (which is available under Gnome as well as KDE). Set to "none" to not use the keyring for a specific invocation.';
  $params->{'keyring'}->{'type'} = 's';
  $params->{'keyring'}->{'options'} = join("|",@COPS::config::supported_keyrings);
  $params->{'keyring'}->{'nogui'} = '1';

  $params->{'length'}->{'single'} = 'l';
  $params->{'length'}->{'desc'} = 'Define the length of the generated password. If using the wordbank, this is number of words not characters.';
  $params->{'length'}->{'type'} = 'i';
  $params->{'length'}->{'default'} = '8';

  $params->{'listwidth'}->{'single'} = 'L';
  $params->{'listwidth'}->{'desc'} = 'Width of the formatted list output in characters';
  $params->{'listwidth'}->{'type'} = 'i';
  $params->{'listwidth'}->{'default'} = '100';

  $params->{'maxlength'}->{'single'} = 'm';
  $params->{'maxlength'}->{'desc'} = 'Specify the maximum length that a single word can be (in characters)';
  $params->{'maxlength'}->{'type'} = 'i';
  $params->{'maxlength'}->{'default'} = '12';

  $params->{'nogen'}->{'single'} = 'n';
  $params->{'nogen'}->{'desc'} = 'Don\'t generate a password internally but prompt for one on stdin instead.';
  $params->{'nogen'}->{'default'} = '0';
  
  $params->{'numbered'}->{'single'} = 'N';
  $params->{'numbered'}->{'type'} = 's';
  $params->{'numbered'}->{'desc'} = 'Specify which characters of the password to display (comma seperated). Example 1,6,13,14. Output is to stdout only. Useful for systems such as banking logins that require specific parts of a "secret word"';  

  $params->{'onlyme'}->{'single'} = 'o';
  $params->{'onlyme'}->{'desc'} = 'When creating an entry, use only your own key for encryption (which allows for personal passwords to be stored in a multi-user cops system). Converesely, when used with "list" or "export", only return personal entries';
  $params->{'onlyme'}->{'default'} = '0';

  $params->{'olderthan'}->{'single'} = 'O';
  $params->{'olderthan'}->{'desc'} = 'When regenerating/keychanging entries, only proceed on entries older than X days. When set, this means that if cops borks part way through a regeneration it will not re-attempt entries that have already been updated.';
  $params->{'olderthan'}->{'type'} = 'i';
  $params->{'olderthan'}->{'default'} = '0';

  $params->{'publickey'}->{'single'} = 'p';
  $params->{'publickey'}->{'desc'} = 'Specify the public key to use for encryption. Will be converted on the fly if not in PKCS8 format. Will croak if that conversion fails.';
  $params->{'publickey'}->{'type'} = 's';
  $params->{'publickey'}->{'default'} = "$ENV{'HOME'}/.ssh/id_rsa.pub";

  $params->{'privatekey'}->{'single'} = 'P';
  $params->{'privatekey'}->{'desc'} = 'Specify the private key to use for decryption.';
  $params->{'privatekey'}->{'type'} = 's';
  $params->{'privatekey'}->{'default'} = "$ENV{'HOME'}/.ssh/id_rsa";

  $params->{'quiet'}->{'single'} = 'q';
  $params->{'quiet'}->{'desc'} = 'Supress the majority of messaging. Useful for pipes if you are not using --pipeto/--writeto. Implies --noansi unless --stagewhisper is set.';
  $params->{'quiet'}->{'default'} = '0';
  
  $params->{'silent'}->{'desc'} = 'Supress ALL messaging except for fatal errors. Implies --noansi unless --stagewhisper is set.';
  $params->{'silent'}->{'default'} = '0';

  $params->{'randomchar'}->{'single'} = 'r';
  $params->{'randomchar'}->{'desc'} = 'Cops will generate a random character password instead of a wordbank style password.';
  $params->{'randomchar'}->{'default'} = '0';

  $params->{'easygen'}->{'desc'} = 'Generate a strong password that will meet (nearly all) password complexity requirements where at least 24 characters are permitted. Result: 21 random alphanumerics (125 bits of entropy) prefixed with "$8.".';
  $params->{'easygen'}->{'default'} = '0';


  # TODO: Breaking change - remove single as required to free it up as probably only useful in copsrc
  $params->{'autoclearkeyring'}->{'single'} = 'R';
  $params->{'autoclearkeyring'}->{'desc'} = 'Automatically clear your keyring should your passphrase be found to be incorrect. Saves you having to type "cops clearkeyring"';
  $params->{'autoclearkeyring'}->{'default'} = '0';
  $params->{'autoclearkeyring'}->{'nogui'} = '1';

  $params->{'store'}->{'single'} = 's';
  $params->{'store'}->{'desc'} = 'The path to the password store to use.';
  $params->{'store'}->{'type'} = 's';
  $params->{'store'}->{'default'} = "$ENV{'HOME'}/.cops_passwords/";

  $params->{'show'}->{'single'} = 'S';
  $params->{'show'}->{'desc'} = 'Show the plaintext data on stdout after generation';
  $params->{'show'}->{'default'} = '0';

  $params->{'type'}->{'single'} = 't';
  $params->{'type'}->{'desc'} = 'The type of data to add, update or retrieve';
  $params->{'type'}->{'type'} = 's';
  $params->{'type'}->{'default'} = 'password';
  $params->{'type'}->{'options'} = join("|",@COPS::config::allowed_entry_types);

  $params->{'timeoutbuffer'}->{'single'} = 'T';
  $params->{'timeoutbuffer'}->{'desc'} = 'When used with --buffer this alters the length of time ( in seconds ) before the copy buffer is cleared. Stalls exit of cops until completed.';
  $params->{'timeoutbuffer'}->{'type'} = 'i';
  $params->{'timeoutbuffer'}->{'default'} = '10';
  $params->{'timeoutbuffer'}->{'nogui'} = '1';

  $params->{'use-spaces'}->{'single'} = 'u';
  $params->{'use-spaces'}->{'desc'} = 'When creating a wordbank based password, use spaces rather than hyphens to seperate the words';
  $params->{'use-spaces'}->{'default'} = '0';

  $params->{'use-asterixes'}->{'desc'} = 'When typing secret data into cops, show an asterix for each keystroke rather than nothing at all';
  $params->{'use-asterixes'}->{'default'} = '0';
  
  $params->{'userdir'}->{'single'} = 'U';
  $params->{'userdir'}->{'desc'} = 'The directory to store the keyring and fifo files in. Should not be visible to other users on the same system. Will be created if it does not exist';
  $params->{'userdir'}->{'type'} = 's';
  $params->{'userdir'}->{'default'} = "$ENV{'HOME'}/.cops";

  $params->{'verifywith'}->{'single'} = 'v';
  $params->{'verifywith'}->{'desc'} = 'If manually decrypting an endorsed entry this option allows you to specify which public key you wish to use to check the endorsement with.';
  $params->{'verifywith'}->{'type'} = 's';
  
  $params->{'weaker'}->{'single'} = 'w';
  $params->{'weaker'}->{'desc'} = 'Use /dev/urandom rather than /dev/random, i.e. don\'t use the stored entropy pool for password generation.';
  $params->{'weaker'}->{'default'} = '0';

  $params->{'whoiam'}->{'single'} = 'W';
  $params->{'whoiam'}->{'desc'} = 'Override your local username for use with the cops system. Use with --onlyme to create a password for someone else! (Note: This does not affect configuration generated from your username, e.g. password store location)';
  $params->{'whoiam'}->{'type'} = 's';
  
  $params->{'xtimes'}->{'single'} = 'x';
  $params->{'xtimes'}->{'desc'} = 'Number of passwords to generate';
  $params->{'xtimes'}->{'type'} = 'i';
  $params->{'xtimes'}->{'default'} = '1';

  $params->{'xdo'}->{'single'} = 'X';
  $params->{'xdo'}->{'desc'} = 'Autotype. Pipe the output to xdotool, which will then type the retrieved data into the next window you bring into focus. A value > 0 specifies number of times to output the password (tab seperated). Default if not specified is 1.';
  $params->{'xdo'}->{'type'} = 'i';
  $params->{'xdo'}->{'default'} = '0';
  $params->{'xdo'}->{'argument'} = 'optional';
  $params->{'xdo'}->{'nogui'} = '1';
  
  $params->{'wordlist-size'}->{'single'} = 'z';
  $params->{'wordlist-size'}->{'desc'} = 'Specify the strength of the wordlist to use on a scale of 1 to 3 (three being the strongest, but most complicated)';
  $params->{'wordlist-size'}->{'type'} = 'i';
  $params->{'wordlist-size'}->{'default'} = 2;

  $params->{'wordlist-file'}->{'single'} = 'Z';
  $params->{'wordlist-file'}->{'desc'} = 'Specify a different file for the wordlist source. Entries should be newline seperated and any line containing a character that is not [A-Za-z] is discarded.';
  $params->{'wordlist-file'}->{'type'} = 's';
  
  $params->{'groupentries'}->{'desc'} = "When using 'list', group together entries that share the same prefix followed by an underscore";
  $params->{'groupentries'}->{'default'} = 0;
  
  $params->{'stagewhisper'}->{'desc'} = "Allow ansi colour coding when using --quiet. Without this option, --quiet removes all ansi encoding.";
  $params->{'stagewhisper'}->{'default'} = 0;
  
  $params->{'autochomp'}->{'desc'} = "Automatically strip the last newline from any entries on output (be that stdin/xsel/xdo/etc). Useful for when (for example) a legacy password has been imported into Cops pre v2.18.0 as past this point entries are chomped on input. Newlines are still added for --show but this prevents double newlines in that instance";
  $params->{'autochomp'}->{'default'} = 0;
  
  $params->{'xdo-delay'}->{'desc'} = "Delay (in milliseconds) between keystrokes when outputting text via --xdo. Increase from the default value if you find output is garbled or missing some data.";
  $params->{'xdo-delay'}->{'default'} = 12;
  $params->{'xdo-delay'}->{'type'} = 'i';
  $params->{'xdo-delay'}->{'nogui'} = '1';
  
  $params->{'noprompts'}->{'desc'} = "Disables all prompts for user input. This is a feature to allow cops to be integrated into pipes and system calls. Errors if cops believes there is a requirement for user entry.";
  $params->{'noprompts'}->{'default'} = 0;
  
  $params->{'noshortcommands'}->{'desc'} = "As of version 2.24, normal behaviour for cops is to allow shortened commands, eg 'cops gen' will invoke 'generate' and 'cops v' will match 'cops version'. This option disables that behaviour and reverts to pre 2.24 functionality. Useful in .copsrc";
  $params->{'noshortcommands'}->{'default'} = 0;

  $params->{'termux'}->{'desc'} = "Modify some settings to work with termux on android. Useful in ~/.copsrc";
  $params->{'termux'}->{'default'} = 0;
  
  $params->{'entropy'}->{'desc'} = "Provide details on the entropy size of a generated password";
  $params->{'entropy'}->{'default'} = 0;

  $params->{'hibp'}->{'desc'} = "Check the haveIbeenPwned API for matching entries";
  $params->{'hibp'}->{'default'} = 0;

  $params->{'encryptfor'}->{'desc'} = "When adding an entry or encrypting some data, specify the user for whom the data is encrypted. Will fail if a key by the same name does not exist in your keys directory. Note: You can use -p/--publickey to specify a public key not in your keystore";
  $params->{'encryptfor'}->{'type'} = 's';
  
  $params->{'keyringpin'}->{'desc'} = "Use (and prompt for) a security pin when using a keyring. This is intended to be a balance between having to type a long passphrase each time you use your key and securely encrypting your passphrase.";
  $params->{'keyringpin'}->{'default'} = '0';
  $params->{'keyringpin'}->{'nogui'} = '1';
  
  $params->{'cost'}->{'desc'} = "The cost attribute to a hashing function. Currently only supported on bcrypt hashes";
  $params->{'cost'}->{'default'} = 16;
  $params->{'cost'}->{'type'} = 'i';
  
  $params->{'pipeto'}->{'desc'} = "Sends output for the *secret data only* to a file or filehandle of your choosing. That file should be an executable that accepts input on stdin. Equivalent to cops |";
  $params->{'pipeto'}->{'type'} = 's';
  
  $params->{'writeto'}->{'desc'} = "Sends output for the *secret data only* to a file of your choosing. That pipe can be an device that accepts input on stdin.";
  $params->{'writeto'}->{'type'} = 's';
  
  $params->{'group'}->{'desc'} = "Which group of keys to add/refresh an entry for. Groups are defined in your password storage directory under ./.cops_keys/. Specify groups as <groupname>.group as line delimited usernames.";
  $params->{'group'}->{'type'} = 's';

  $params->{'filter'}->{'desc'} = "Filter results on list or export. Allows for basic AND/NOT syntax. Encapsulate within quotes. Example; cops list --filter 'ilio AND test NOT foo'";
  $params->{'filter'}->{'type'} = 's';

  $params->{'export-template'}->{'desc'} = "Provide an CSV template when exporting entries. Allows you to specify order and alternate header namers for a cops export. See Docs/export-template.txt for further infomration on syntax";
  $params->{'export-template'}->{'type'} = 's';
  
  $params->{'help'}->{'desc'} = "Provide help on a command. Analogous to 'cops <command> help'";
  $params->{'help'}->{'default'} = 0;
  $params->{'help'}->{'single'} = 'h';

  $params->{'version'}->{'desc'} = "Provide version information. Analogous to 'cops version'";
  $params->{'version'}->{'single'} = 'V';
  $params->{'version'}->{'default'} = 0;


  if($COPS::config::nogui)
  {
    foreach my $key (keys %{$params})
    {
      $params->{$key}->{'nogui'} and delete($params->{$key});
    }
  }

  return $params;
}

1;
