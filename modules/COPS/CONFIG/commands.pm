# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::CONFIG::commands;
use strict;


sub list
{
  my $commands;
  our @types = ("entry","utility","keys","info");

#######

  $commands->{'add'}->{'desc'}                = "**Deprecated** Use 'cops create' instead";
  $commands->{'add'}->{'longdesc'}            = "**Deprecated** Use 'cops create' instead";

#######

  $commands->{'create'}->{'type'}             = "entry";
  $commands->{'create'}->{'desc'}             = "Create an entry in the cops storage system";
  $commands->{'create'}->{'argument'}         = "entry-name";
  $commands->{'create'}->{'longdesc'}         = "Used to create a new entry in the storage system. By default ( unless --onlyme or --group is specified ) adds an entry for each stored public key. ";
  $commands->{'create'}->{'valid'}            = [
    'alphanumeric',
    'autoclearfifo',
    'buffer',
    'entropy',
    'hibp',
    'countdown',
    'crypt',
    'endorse',
    'insensitive',
    'length',
    'maxlength',
    'no-hyphens',
    'nogen',
    'onlyme',
    'pin',
    'privatekey',
    'publickey',
    'randomchar',
    'show',
    'stdin',
    'store',
    'timeoutbuffer',
    'type',
    'use-spaces',
    'use-asterixes',
    'userdir',
    'weaker',
    'whoiam',
    'wordlist-file',
    'wordlist-size',
    'xdo',
    'xdo-delay',
    'encryptfor',
    'writeto',
    'pipeto',
    'group'
];

#######

  $commands->{'update'}->{'type'}          = "entry";
  $commands->{'update'}->{'desc'}          = "Update an existing entry in the cops storage system";
  $commands->{'update'}->{'argument'}      = "entry name/number";
  $commands->{'update'}->{'valid'}         = [
    'alphanumeric',
    'autoclearfifo',
    'buffer',
    'entropy',
	'hibp',
    'countdown',
    'crypt',
    'endorse',
    'exactmatch',
    'fuzzymatch',
    'insensitive',
    'length',
    'maxlength',
    'no-hyphens',
    'nogen',
    'pin',
    'privatekey',
    'publickey',
    'randomchar',
    'show',
    'stdin',
    'store',
    'timeoutbuffer',
    'type',
    'use-spaces',
    'use-asterixes',
    'userdir',
    'weaker',
    'whoiam',
    'wordlist-file',
    'wordlist-size',
    'xdo',
    'xdo-delay',
    'encryptfor',
    'writeto',
    'pipeto'
];

#######

  $commands->{'list'}->{'type'}            = "entry";
  $commands->{'list'}->{'desc'}            = "List all entries which are available to you, or those matching a search term";
  $commands->{'list'}->{'optionalarg'}     = "Search term";
  $commands->{'list'}->{'valid'}           = [
    'listwidth',
    'store',
    'whoiam',
    'groupentries',
    'filter',
    'onlyme'
];

#######

  $commands->{'get'}->{'type'}             = "entry";
  $commands->{'get'}->{'desc'}             = "Get an entry. Accepts partial matches or numeric ID as detailed in 'list'";
  $commands->{'get'}->{'argument'}         = "entry name/number";
  $commands->{'get'}->{'valid'}            = [
    'autoclearfifo',
    'autochomp',
    'backwards',
    'buffer',
    'entropy',
	'hibp',
    'countdown',
    'crypt',
    'exactmatch',
    'fuzzymatch',
    'keyring',
    'klick',
    'nosalt',
    'numbered',
    'privatekey',
    'salt',
    'show',
    'store',
    'timeoutbuffer',
    'type',
    'userdir',
    'whoiam',
    'xdo',
    'xdo-delay',
    'writeto',
    'pipeto'
];

#######

  $commands->{'autofill'}->{'type'}        = "entry";
  $commands->{'autofill'}->{'desc'}        = "Retrieves both the username and the password for an entry from cops, then uses XDO to paste to a webform or similar";
  $commands->{'autofill'}->{'argument'}    = "entry name/number";
  $commands->{'autofill'}->{'longdesc'}    = "Retrieves both the username and the password for an entry from cops, then uses xdotool to paste the username, then a tab, then the password, finalised with an 'enter' keystroke. Useful for a standard web login form.\nYou can also specify your own method of autotyping using the 'jumpmethod' parameter. Using this method can allow for complex login systems to be automated. 
  
  Note, this command is not a replacement for the -X flag when using 'cops get'.";
  $commands->{'autofill'}->{'nogui'}       = 1;
  $commands->{'autofill'}->{'valid'}       = [
    'autoclearfifo',
    'autochomp',
    'countdown',
    'exactmatch',
    'fuzzymatch',
    'jump',
    'jumpmethod',
    'keyring',
    'klick',
    'privatekey',
    'store',
    'userdir',
    'whoiam'
];

#######

  $commands->{'hash'}->{'type'}            = "utility";
  $commands->{'hash'}->{'desc'}            = "Use the built-in hashing functions to encrypt an entry with a one way hash. If data is not supplied as an argument this function will prompt for the data you wish to hash";
  $commands->{'hash'}->{'optionalarg'}     = "data to hash";
  $commands->{'hash'}->{'requiredparam'}   = [ 'crypt' ];
  $commands->{'hash'}->{'valid'}           = [
    'crypt',
    'nosalt',
    'salt',
    'show',
    'use-asterixes',
    'cost'
];


#######

  $commands->{'generate'}->{'type'}        = "utility";
  $commands->{'generate'}->{'desc'}        = "Generate a randomised string without storing it. Implies --show unless --buffer is set.";
  $commands->{'generate'}->{'valid'}       = [
    'alphanumeric',
    'buffer',
    'countdown',
    'crypt',
    'insensitive',
    'klick',
    'length',
    'maxlength',
    'no-hyphens',
    'nosalt',
    'pin',
    'randomchar',
    'salt',
    'show',
    'timeoutbuffer',
    'use-spaces',
    'weaker',
    'wordlist-file',
    'wordlist-size',
    'xdo',
    'xdo-delay',
    'xtimes',
	'hibp',
	'entropy',
    'type',
    'writeto',
    'pipeto',
    'cost'
];

#######

  $commands->{'decrypt'}->{'type'}         = "utility";
  $commands->{'decrypt'}->{'desc'}         = "Decrypt a base64 cops entry via stdin and display it to stdout";
  $commands->{'decrypt'}->{'optionalarg'}  = "filename";
  $commands->{'decrypt'}->{'valid'}        = [
    'autoclearfifo',
    'crypt',
    'keyring',
    'privatekey',
    'show',
    'userdir',
    'verifywith',
    'stdin'
];

#######

  $commands->{'encrypt'}->{'type'}         = "utility";
  $commands->{'encrypt'}->{'desc'}         = "Encrypt a text entry on stdin and display it to stdout.";
  $commands->{'encrypt'}->{'optionalarg'}  = "data to encrypt";
  $commands->{'encrypt'}->{'valid'}        = [
    'autoclearfifo',
    'endorse',
    'publickey',
    'userdir',
    'whoiam',
    'encryptfor',
    'stdin'
];

#######

  $commands->{'version'}->{'type'}         = "info";
  $commands->{'version'}->{'desc'}         = "Display the current version";

#######

  $commands->{'help'}->{'type'}            = "info";
  $commands->{'help'}->{'desc'}            = "Display help information for cops";
  $commands->{'help'}->{'optionalarg'}     = "command";

#######

  $commands->{'clearfifo'}->{'type'}       = "entry";  
  $commands->{'clearfifo'}->{'desc'}       = "Clear the fifo directory of any stale entries";

#######

  $commands->{'clearkeyring'}->{'type'}    = "entry";
  $commands->{'clearkeyring'}->{'desc'}    = "Clear the system keyring of all entries";
  $commands->{'clearkeyring'}->{'nogui'}   = 1;
  $commands->{'clearkeyring'}->{'valid'}   = [
    'keyring',
    'userdir'
];


#######

  $commands->{'refresh'}->{'type'}         = "keys";
  $commands->{'refresh'}->{'desc'}         = "Regenerate any missing encrypted keyfiles for other group users";
  $commands->{'refresh'}->{'longdesc'}     = "Use this facility when (for example) a new member is added to the public .cops_keys store. It will only generate keys that it thinks are missing.\nIf a member updates their key this facility will not track that an entry is missing as it cannot decode an entry based on the public key\n\nThis must be run by someone with access to the password store.";
  $commands->{'refresh'}->{'valid'}        = [
    'endorse',
    'store',
    'userdir',
    'privatekey',
    'publickey',
    'group'
];

#######

  $commands->{'keychange'}->{'type'}       = "keys";
  $commands->{'keychange'}->{'desc'}       = "Update your passwords to use a new public key";
  $commands->{'keychange'}->{'longdesc'}   = "Reads the latest versions of all passwords that you can decrypt and creates new keyfiles using the key stored in cops_keys. So, to update your ssh key first place the new public key in cops keys, run this utility then update your private key path";
  $commands->{'keychange'}->{'valid'}      = [
    'olderthan',
    'privatekey',
    'publickey',
    'store',
    'userdir',
    'whoiam'
];

#######

  $commands->{'export'}->{'type'}          = "entry";
  $commands->{'export'}->{'desc'}          = "Export all entries to an encrypted csv file. Returns the encryption key";
  # TODO: Ability to specify encryption key
  $commands->{'export'}->{'longdesc'}      = "Decrypts all entries available to you and saves them in an encrypted (AES256) csv (base64 encoded) with the name format 'cops-export-yyyy_mm_dd.enc'. Returns the randomised encryption key on STDOUT";
  $commands->{'export'}->{'argument'}      = "Directory to store created file in";
  $commands->{'export'}->{'valid'}         = [
    'privatekey',
    'store',
    'userdir',
    'whoiam',
    'filter',
    'export-template',
    'onlyme'
];


#######

  $commands->{'search'}->{'type'}          = "entry";
  $commands->{'search'}->{'desc'}          = "Search (i.e. decrypt and scan) all available entries for a string.";
  $commands->{'search'}->{'longdesc'}      = "Reads through all available entries in the password store matching (loosely) against your search string. --exactmatch can be used to stipulate no loose/wildcard searches. Depending on the size of your password store and/or ssh key this can take a long time. Use --quiet to reduce output. **It is recommended that you only search for a portion of the string you are looking for if you can. This reduces the exposure of your search term in process space**";
  $commands->{'search'}->{'optionalarg'}   = "search term";
  $commands->{'search'}->{'valid'}         = [
    'privatekey',
    'store',
    'userdir',
    'whoiam',
    'type',
    'exactmatch'
];

#######

  $commands->{'whoami'}->{'type'}          = "info";
  $commands->{'whoami'}->{'desc'}          = "Report the username cops believes you are using";
  $commands->{'whoami'}->{'valid'}         = "";
  $commands->{'whoami'}->{'longdesc'}      = "Cops relies on the ability to determine your username. You can override this at the command line or in ~/.copsrc which will allow you to specify a different username to that which you use to log in, or to rectify any environment issues.";

#######

  $commands->{'dump'}->{'type'}            = "info";
  $commands->{'dump'}->{'desc'}            = "Dump the current values of all configuration parameters to stdout";

#######

  $commands->{'params'}->{'type'}          = "info";  
  $commands->{'params'}->{'desc'}          = "Show a full list of every command line parameter available to cops";
  
#######

  $commands->{'addkey'}->{'desc'}          = "Convert a ssh public key into PKCS8 format and place it in the keystore for your user (override this username with --whoiam)";
  $commands->{'addkey'}->{'valid'}         = [
    'store',
    'whoiam'
];
  $commands->{'addkey'}->{'type'}          = "keys";
  $commands->{'addkey'}->{'argument'}      = "ssh rsa public key file";
  $commands->{'addkey'}->{'longdesc'}      = <<LONGDESC;
This uses the linux ssh-keygen binary to convert a public ssh key into an openssl format that can be used by cops.

  If your installed version of ssh-keygen cannot convert a public ssh key to the required
  format, you can generate it manually if you are the owner of your private key by using openssl directly.

  eg

  openssl rsa -in ~/.ssh/id_rsa -pubout -out `whoami`.pem

  Copy this to the .cops.keys directory of the password store.
LONGDESC

#######

  $commands->{'revertpem'}->{'type'}        = "keys";
  $commands->{'revertpem'}->{'argument'}    = "Cops username";
  $commands->{'revertpem'}->{'valid'}       = ['store'];
  $commands->{'revertpem'}->{'desc'}        = "Output named stored public key in ssh-rsa format";
  $commands->{'revertpem'}->{'longdesc'}    = "Converts a named public key stored in cops back to its ssh-rsa format and outputs it on stdout. Useful for checking fingerprints etc against known keys\n";
  
  if($COPS::config::nogui)
  {
    foreach my $key (keys %{$commands})
    {
      $commands->{$key}->{'nogui'} and delete($commands->{$key});
    }
  }

  return $commands;
}

1;
