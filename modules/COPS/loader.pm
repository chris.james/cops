# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::loader;
use strict;

# Must load first, in order
require COPS::FUNC::module;
require COPS::print;
require COPS::FUNC::file;
require COPS::FUNC::exec;
require COPS::FUNC::which;
require COPS::config;
require COPS::args;

# Now in any order

require COPS::FUNC::rng;
require COPS::FUNC::hash;
require COPS::FUNC::input;
require COPS::FUNC::filter;
require COPS::FUNC::time;
require COPS::FUNC::number;
require COPS::FUNC::debug;
require COPS::FUNC::retrieve;
require COPS::FUNC::ssl;
require COPS::FUNC::search;
require COPS::FUNC::endorse;
require COPS::FUNC::check;
require COPS::FUNC::crypt;

require COPS::CMD::switch;
require COPS::CMD::decrypt;
require COPS::CMD::generate;
require COPS::CMD::help;
require COPS::CMD::dump;
require COPS::CMD::hash;
require COPS::CMD::general;
require COPS::CMD::list;
require COPS::CMD::get;
require COPS::CMD::create;
require COPS::CMD::update;
require COPS::CMD::encrypt;
require COPS::CMD::autofill;
require COPS::CMD::addkey;
require COPS::CMD::keychange;
require COPS::CMD::refresh;
require COPS::CMD::search;
require COPS::CMD::export;
require COPS::CMD::revertpem;

require COPS::KEYRING::kdewallet;

require COPS::clipboard;
require COPS::fifo;
require COPS::user;
require COPS::output;
require COPS::keyring;
require COPS::text;
require COPS::passphrase;
require COPS::xdo;
require COPS::keys;
require COPS::password;
require COPS::store;
require COPS::get;
require COPS::version;
require COPS::integrity;
require COPS::pwquality;

1;

