# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::hash;
use strict;

# A handler for various one-way hashing functions

# As these are more niche-use, we use require then import (runtime, not compile time) to give
# a soft fail if the user has not installed the various modules needed here

sub crypt
{
  my $arg    = shift;
  my $data   = $arg->{'data'};
  my $type   = $arg->{'algo'};
  my $salt   = $arg->{'salt'};
  my $nosalt = $arg->{'nosalt'};
  
  chomp($data);
  
  $nosalt and $type ne "md5" and COPS::print::warn("--nosalt is irrelevant unless using an md5 hash");

  if($type eq 'sha512')
  {
    $salt or $salt=salt(16);
    return crypt($data,"\$6\$$salt\$");
  }
  elsif($type eq "bcrypt")
  {
    require Digest;
    import Digest;
    
    if(length($data) > 72)
    {
      COPS::print::warn("WARNING: Secret truncated to 72 characters due to limitations of bcrypt");
    }
    
    # TODO: COPS::args should not be used in FUNC/
    my $cost = $COPS::args::param->{'cost'};
    ($cost > 0 and $cost < 32 and $cost =~ /^\d+$/) or COPS::print::fatal("Bcrypt cost should be a value >1 and <32");
    $salt or $salt=salt(16);
    length($salt) == 16 or COPS::print::fatal("Salt passed to bcrypt hashing function must be 16 octets");
    my $bcrypt = Digest->new('Bcrypt', cost=>$cost, salt=>$salt);
    my $settings = $bcrypt->settings(); 
    my $pass_hash = $bcrypt->add($data)->bcrypt_b64digest;
    return $settings.$pass_hash;
  }
  elsif($type eq 'lanman' or $type eq 'ntmd4')
  {
    $salt and COPS::print::warn("--salt defined, but is irrellevant with the lanman or ntmd4 hashing algorithm");
    require Crypt::SmbHash;
    import Crypt::SmbHash qw(lmhash nthash);
    $type eq "lanman" and return lmhash($data);
    return nthash($data);
  }
  elsif($type eq 'md5')
  {
    if($nosalt)
    {
      require Digest::MD5;
      import Digest::MD5 qw(md5_hex);
      return md5_hex($data);
    }
    $salt or $salt=salt(8);
    return crypt($data,"\$1\$$salt\$");
  }
  elsif($type eq 'sha1')
  {
    $salt and COPS::print::warn("--salt defined, but is irrellevant with the sha1 hashing algorithm");
    require Digest::SHA;
    import Digest::SHA qw(sha1_hex sha1);
    return uc(sha1_hex($data));
  }
  elsif($type eq 'mysql')
  {
    $salt and COPS::print::warn("--salt defined, but is irrellevant with the mysql hashing algorithm");
    require Digest::SHA;
    import Digest::SHA qw(sha1_hex sha1);
    return "*".uc(sha1_hex(sha1($data)));
  }
  else
  {
    COPS::print::fatal("Unknown hash type '$type' passed to function");
  }
}

sub salt
{
  my $l = shift;
  $l or COPS::print::fatal('Must specify salt length');
  my @chars = ('A'..'Z', 'a'..'z', '0'..'9');
  my $salt;

  # We don't really need to be cryptographically secure for salt generation :)
  for(my $i=1; $i<=$l; $i++)
  {
    $salt .= $chars[rand(62)]
  }
  return $salt;
}

1;
