# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::exec;
use strict;

sub cmd
{
  my $exec = shift;
  my $arg = shift;

  my @call = caller();
  COPS::print::debug("Executing [$call[0]:$call[2]]: $exec");

  if(defined($arg->{'syscall'}))
  {
    # Don't wait for return and pipe output to /dev/null
    `$exec >> /dev/null 2>&1`;
    return;
  }
  if(defined($arg->{'bool'}))
  {
    # Just return bool
    my $result = `$exec &> /dev/null && echo 0 || echo 1`;
    return $result;
  }
  else
  {
    my $result = `$exec 2>&1`;
    COPS::print::debug("Executed call returned '$result'");
    return $result;
  }
}
  
1;
