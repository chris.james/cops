# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::check;
use strict;

sub hibp
{
  (COPS::FUNC::module::check("HTTP::Tiny") and
   COPS::FUNC::module::check("IO::Socket::SSL")
  ) or return -1;
  require HTTP::Tiny;
  import HTTP::Tiny;

  # Interface to haveibeenpwnd api
  my $pw = shift;
  my $sha = COPS::FUNC::hash::crypt({data => $pw, algo => 'sha1'});
  $sha =~ /^(.{5})(.*)$/; # Get first five characters
  my $five = $1;
  my $rest = $2;
  $five =~ /[^0-9a-fA-F]/ and COPS::print::error("Internal hash generation failed. Aborting");
  
  my $url = "https://api.pwnedpasswords.com/range/$five";
  COPS::print::debug("Retrieving data from $url");
  
  my $hl = HTTP::Tiny->new->get($url);
  $hl->{success} or COPS::print::error("Failed to retrieve upstream data from api.pwnedpasswords.com");

  my $r = 0; 
  ($hl->{'content'} =~ /${rest}:(\d+)/) and $r = $1;
  return $r;
}

1;
