# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::retrieve;
use strict;
use MIME::Base64;

sub all_entries
{
  my $arg = shift;
  # Accepts
  #    store - path to password directory
  #    user - username
  # Returns - hash reference of all entries which are available to the user
  my $store = $arg->{'store'};
  my $user = $arg->{'user'};
  my $filter = $arg->{'filter'};
  my $onlyme = $arg->{'onlyme'};

  my $entries;
  
  # Sanity check
  defined($store) or COPS::print::error("Storage directory 'store' must be passed to routine");
  defined($user)  or COPS::print::error("Username 'user' must be passed to routine");

  my $file = COPS::FUNC::file::status($store);
  ($file->{'type'} eq "directory") or COPS::print::fatal("Provided storage directory of '$store' does not exist or is not a directory");
  $store = $file->{'path'};

  undef($file);

  my $pw_id=1;

  COPS::print::debug("Examining '$store' for entries"); 

  foreach my $pwdir (sort { lc $a cmp lc $b } (glob("$store/*")))
  {
    COPS::FUNC::file::status($pwdir)->{'type'} eq 'directory' or next;

    # Get the password name ( unique )
    $pwdir =~ /(.*)\/(.*)/;          # Glob gives a full path
    my $name = $2;
    $name =~ /^_/ and next; # Skip those that are disabled with a leading underscore

    my @dirlist = COPS::FUNC::file::dirlist($pwdir);
    foreach my $type (@COPS::config::allowed_entry_types)
    {
       my $backwards=$COPS::args::param->{'backwards'}; # Yes, this breaks our model of what a FUNC should be. It's much easier than refactoring though and I'm short on time :(
       $backwards or $backwards=0;
       
      # Examine the entries directory for items readable by us
      ENTRY: foreach my $file (reverse sort @dirlist)
      {
        # Ignore anything that isn't readable by us
        $file =~ /.*\/(\d\d\d\d_\d\d_\d\d)(|_\d\d_\d\d_\d\d)_$user\.($type$|$type\.\d\d$)/ or next;
        # For clarity

        my $date = $1;
        my $time = $2;
        my $ext  = $3;

        # If this is a 'new' password then increment our id.
        # This also means different types end up with the same id, regardless of anything else

        # We do this pre-filter to retain ID numbers

        unless($entries->{$name})
        {
          $entries->{$name}->{'id'}            = $pw_id; # ID is agnostic to type
          $pw_id++;
        }

        if($backwards>0)
        {
          COPS::print::debug("Going backwards one step ($file)");
          $backwards--;
          next ENTRY;
        }

        COPS::print::debug("Readable entry found for '$name'");

        if($filter)
        {
          COPS::FUNC::filter::entry({"name" => $name, "filter" => $filter}) or next;
        }
        if($onlyme)
        {
          $name =~ /^$user\-/ or next;
        }

        $entries->{$name}->{$type}->{'date'} = $date;
        $entries->{$name}->{$type}->{'date'} =~ s/_/\//g;
        $entries->{$name}->{$type}->{'file'} = $file; # Some older passwords didn't have a 'time' section in the filename
        $entries->{$name}->{'valid'}=1;
        last;
      }
    }
    exists($entries->{$name}->{'valid'}) or delete($entries->{$name});
  }
  return $entries;
}

############################################################################
sub file_by_id
{
  # Given an id and type, what file do we want?
  my $arg = shift;
  $arg->{'id'}   or COPS::print::error("No id passed to COPS::FUNC::retrieve::file_by_id");
  $arg->{'type'} or COPS::print::error("Type not passed to COPS::FUNC::retrieve::file_by_id");
  
  my $entries = all_entries({
      store => $arg->{'store'}, 
      user => $arg->{'user'}
    });
   
  foreach my $key (keys %{$entries})
  {
    $entries->{$key}->{'id'} == $arg->{'id'} or next;
    return $entries->{$key}->{$arg->{'type'}}->{'file'};
  }
  return 0;
}
  
############################################################################
sub name_by_id
{
  # Given an id, what is the name of the entry?
  my $arg = shift;
  $arg->{'id'}   or COPS::print::error("No id passed to COPS::FUNC::retrieve::file_by_id");
  
  my $entries = all_entries({
      store => $arg->{'store'}, 
      user => $arg->{'user'}
    });
   
  foreach my $key (keys %{$entries})
  {
    $entries->{$key}->{'id'} == $arg->{'id'} or next;
    return $key;
  }
  return 0;
}
  
############################################################################
sub id_by_name
{
  # Given a name. what is the id of the entry?
  my $arg = shift;
  $arg->{'name'}   or COPS::print::error("No name passed to COPS::FUNC::retrieve::file_by_id");
  
  my $entries = all_entries({
      store => $arg->{'store'}, 
      user => $arg->{'user'}
    });
   
  foreach my $key (keys %{$entries})
  {
    (lc($key) eq lc($arg->{'name'})) and return $entries->{$key}->{'id'};
    (lc($key) eq lc($COPS::user::name."-".$arg->{'name'})) and return $entries->{$key}->{'id'};
  }
  return 0;
}
  
############################################################################
sub file_by_name
{
  # Given name and type, what file do we want ?
  my $arg = shift;
  $arg->{'name'} or COPS::print::error("No name passed to COPS::FUNC::retrieve::file_by_name");
  
  my $entries = all_entries({
      store => $arg->{'store'}, 
      user => $arg->{'user'}
    });
  
  foreach my $key (keys %{$entries})
  {
    # Because we allow names to be stored in mixed case, but we want to search case-insensitive, we itterate through the list
    if(defined($arg->{'type'}))
    {
      (lc($key) eq lc($arg->{'name'})) and return $entries->{$key}->{$arg->{'type'}}->{'file'};
      (lc($key) eq lc("$arg->{'user'}-$arg->{'name'}")) and return $entries->{$key}->{$arg->{'type'}}->{'file'};
    }
    else
    {
      # if arg->type was not passed, then all we are doing is checking for the existence of an entry
      (lc($key) eq lc($arg->{'name'})) and return 1;
      (lc($key) eq lc("$arg->{'user'}-$arg->{'name'}")) and return 1;
    }   
  }
  return 0;
}
    
sub base64data
{
  my $file = shift;
  
  COPS::print::debug("base64data retrieve called for file='$file'");
  
  my $data = COPS::FUNC::file::status($file);
  my $b64  = COPS::FUNC::file::read($data->{'path'});
  ($data->{'type'} eq "text") or $b64 = encode_base64($b64);
  COPS::print::debug("base64data returned was '$b64'");
  return $b64;
}

  
1;
