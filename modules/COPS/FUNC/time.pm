# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::time;
use strict;


sub stamp
{
  my $ts = localtime()."\n";
  my $long = shift;

  $ts =~ /(...) (...) (.\d) (\d\d:\d\d:\d\d) (\d\d\d\d)/ or return "Failure\n";
  my $months;
  $months->{'Jan'}="01";
  $months->{'Feb'}="02";
  $months->{'Mar'}="03";
  $months->{'Apr'}="04";
  $months->{'May'}="05";
  $months->{'Jun'}="06";
  $months->{'Jul'}="07";
  $months->{'Aug'}="08";
  $months->{'Sep'}="09";
  $months->{'Oct'}="10";
  $months->{'Nov'}="11";
  $months->{'Dec'}="12";

  my $day = $3;
  my $year = $5;
  my $mon = $2;
  my $time = $4;
  $time=~ s/:/_/g;
  $day =~ s/[^\d]//;
  $day < 10 and $day ="0$day";

  $ts = $year."_".$months->{$mon}."_".$day;
  $long and $ts.= "_$time";
  return $ts;
}

sub difference
{
  my $date = shift;
  require "Date/Simple.pm";
  import Date::Simple ('date', 'today');
  
  my $today = today();
  my $diff = date($date) - date($today);
  return $diff;
}

sub duration_in_english
{
  my $time = shift;
  
  my $word='seconds';
  
  my $d;
  # Define time names and add thresholds before we move to the next unit

  $d->{1}->{'n'}='Seconds';
  $d->{1}->{'t'}=180; # No longer define this period by this name when > this threshold
  $d->{1}->{'u'}=60;  # Divider once we move to the next Unit

  $d->{2}->{'n'}='Minutes';
  $d->{2}->{'t'}=180;
  $d->{2}->{'u'}=60;

  $d->{3}->{'n'}='Hours';
  $d->{3}->{'t'}=72;
  $d->{3}->{'u'}=24;

  $d->{4}->{'n'}='Days';
  $d->{4}->{'t'}=14;
  $d->{4}->{'u'}=7;

  $d->{5}->{'n'}='Weeks';
  $d->{5}->{'t'}=52;
  $d->{5}->{'u'}=52;

  $d->{6}->{'n'}='Years';
  $d->{6}->{'t'}=1000;
  $d->{6}->{'u'}=100;

  $d->{7}->{'n'}='Centuries';
  $d->{7}->{'t'}=1000;
  $d->{7}->{'u'}=10;

  $d->{8}->{'n'}='more than 1,000 centuries';

  foreach my $i (sort {$a <=> $b} keys %{$d})
  {
    my $name = $d->{$i}->{'n'};

    $d->{$i}->{'t'} or return $name; # Last one!
    if($time < $d->{$i}->{'t'})
    {
        $time = int($time);
        # Within our threshold
        $time <= 1 and $name =~ s/(.*)s/$1/;
        #$time = COPS::FUNC::number::commify(int($time));
        $time < 1 and $time = "< 1";
        return "$time $name";
    }
    $time = int($time/$d->{$i}->{'u'});
  }
  COPS::print::fatal("logic failure");
}

1;
