# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.


package COPS::FUNC::crypt;
use strict;
use MIME::Base64;

# Our interface to symmetric encryption routines and related ancilaries

#############################################################################################
sub cipher
{
  # We use aes256 stored as base64 for storing data locally
  my $arg = shift;

  # Gives a soft fail if Crypt::CBC cannot  be found. It's an optional module if not installed from rpm
  require Crypt::CBC;
  import  Crypt::CBC;

  my $cipher = Crypt::CBC->new( -key => $arg->{'enckey'}, -cipher => 'Cipher::AES', -pbkdf => $COPS::config::openssl_pbkdf, -chain_mode => "cbc");
  my $val;
  if($arg->{'encrypt'})
  {
    $val = $cipher->encrypt($arg->{'data'});
    $val = encode_base64($val);
  }
  elsif($arg->{'decrypt'})
  {
    $arg->{'data'} = decode_base64($arg->{'data'});
    $val = $cipher->decrypt($arg->{'data'});
  }
  else
  {
    COPS::print::error("Either encrypt or decrypt must be passed");
  }
  return $val;
}

#############################################################################################
sub create_enckey
{
  # Create new encryption key
  # 44 * [a-zA-Z0-9] gives us [7.33 *10^78] variants / [262] bits of entropy
  # We state our absolute trust in > 256 bit in generate.pm
  my $enckey = undef;
  for(my $i=1;$i<=44; $i++)
  {
    $enckey .= COPS::FUNC::rng::alpha("/dev/urandom");
  }
  return $enckey;
}

1;
