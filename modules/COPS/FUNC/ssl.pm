# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::ssl;
use strict;

sub read
{
  my $arg = shift;
  
  (defined($arg->{'passphrase'}) and defined($arg->{'privatekey'})) or COPS::print::error("Passphrase and privatekey not defined");
  
  $arg->{'base64'} or COPS::print::error("No base64 data sent or retrieved.");
  
  my $version = COPS::FUNC::ssl::getversion($arg->{'base64'});
 
  my $plaintext = COPS::FUNC::ssl::decrypt({ 
    'data' => $arg->{'base64'}, 
    'passphrase' => $arg->{'passphrase'}, 
    'privatekey' => $arg->{'privatekey'}, 
    'version' => $version 
  });
  
  $plaintext or return 0;
  return COPS::FUNC::ssl::copsdata({ 'content' => $plaintext, 'version' => $version });
}  
  
####################################################################################
sub decrypt
{
  # Do not call directly, use 'read'
  # requires passphrase, data
  # returns decrypted data
  my $arg    = shift;
  my $fifo   = COPS::fifo::create();
  my $base64 = COPS::FUNC::which::binary('base64'); 
  
  my $data = $arg->{'data'};

  COPS::print::debug("decrypt called!");
  
  if($arg->{'version'} >= 2.0)
  {
    $data =~ /.*#---- COPS ENCRYPTION START ----\n(.*)#---- COPS ENCRYPTION END ----.*/s or COPS::print::error("Invalid format of data");
    $data = $1;
  }
  else
  {
    $data =~ s/#.*\n//g;
  }
  $data =~ s/[\n\r\s]//g;
  
  COPS::print::debug("Received datastream to decrypt");
  COPS::print::debug($data);

  ($data =~ /^([a-zA-Z0-9\=\/\+]+)$/ and $data = $1) || COPS::print::fatal("Non base64 characters found in data");
  my $decrypted = COPS::FUNC::ssl::exec(
    {
      'fifo'       => $fifo,
      'data'       => $arg->{'passphrase'},
      'params'     => "$COPS::config::openssl_enc -decrypt -inkey $arg->{'privatekey'} -passin file:$fifo",
      'prefix'     => "echo '$data' | $base64 -d |"
    }
  );

  return ($decrypted);
}

####################################################################################   
sub copsdata
{
  # Gets the cops data out of the decrypted content
  my $arg = shift;
  
  # MUST remain backwards compatible!
  my $content = $arg->{'content'};
  my $version = $arg->{'version'};
  
  $version or $version = 0; # Taint check 
  COPS::print::debug("Content passed to copsdata with version '$version'\n$content\n");

  if($version)
  {
    if($version eq "1.1" or $version eq "2.0")
    {
      $content =~ s/.*?\x00\n(.*?)\n\x00.*/$1/s or COPS::print::error("Invalid content for file version");
      return $1;
    }
    elsif($version eq "1.0")
    {
      # Version 1.0 had a very brief existence, but we must retain that compatibility!
      $content =~ s/.*?\x00(.*?)\x00.*/$1/s or COPS::print::error("Invalid content for file version");
      return $1;
    }
    else
    {
      COPS::print::error("Version string present but does not match a known version");
    }
  }
  else
  {
    # Should work pre 1.36
    $content =~ /^(.*?)\x00.*/s and return $1;
    # Really REALLY old versions of cops had no padding at all :(
    return $content;
  }
}

####################################################################################
sub encrypt
{
  # requires key, data
  # returns base64 encoded encrypted data
  my $arg     = shift;
  my $fifo    = COPS::fifo::create();
  my $base64  = COPS::FUNC::which::binary('base64'); 
  
  my $data = $arg->{'data'};
  
  defined($arg->{'publickey'}) or COPS::print::error("No publickey passed to encrypt function");
  defined($data) or COPS::print::error("No data passed to encrypt function");
  
  my $key = COPS::FUNC::file::status($arg->{'publickey'});
  $key->{'status'} or COPS::print::fatal("Specified public key of $key->{'path'} does not exist!");
  
  my $publickey = $key->{'path'};
  COPS::FUNC::file::read($publickey) =~ /BEGIN PUBLIC KEY/ or COPS::print::fatal("Publickey should be in .pem format!");
  
  (keylength($publickey) < $COPS::config::keysize_warn) and COPS::print::warn("Public key '$publickey' is less than '$COPS::config::keysize_warn' bits in size");
  my $maxlength= COPS::keys::maxlength($publickey)-$COPS::config::padsize-4; 
  (length($data) > $maxlength) and COPS::print::fatal("publickey is too small to encrypt requested data. Either reduce the datasize or use a larger key");

  # We pad each secret with random data, both before and after the secret.
  # This negates the Chinese_remainder_theorem attack on RSA encryption with the same cleartext

  $data = COPS::FUNC::ssl::padin().COPS::FUNC::ssl::padout($data,$maxlength+1);
  
  my $enc = COPS::FUNC::ssl::exec(
    {
      'fifo' => $fifo,
      'data' => $data,
      'params' => "$COPS::config::openssl_enc -encrypt -pubin -inkey $publickey -in $fifo 2>/dev/null| $base64"
    }
  );
  
  $enc = "#---- COPS ENCRYPTION START ----\n$enc\n#---- COPS ENCRYPTION END ----\n";
  
  ($arg->{'text'}) and $enc = "#---- $arg->{'text'}\n$enc";
  return $enc;
}

####################################################################################
sub exec
{
  COPS::print::debug("exec called!");
  my $arg = shift;
  my $fifo = $arg->{'fifo'};
  
  defined($arg->{'data'}) or COPS::print::error("No data passed to ssl::exec");
  
  my $openssl = COPS::FUNC::which::binary('openssl'); # Check that the openssl binary exists

  (-p '$fifo') and COPS::print::error("$fifo already exists. Stale pipe?");
  COPS::fifo::make($fifo);
  
  (defined($arg->{'params'})) or COPS::print::error("params not defined");
  (defined($arg->{'prefix'})) or $arg->{'prefix'} = '';

  ####################################################################################
  # Now we fork off in order to use a named pipe to transport data to openssl
  my $pid = fork();
  if($pid)
  {
    COPS::print::debug("Parent process spawned to create FIFO");
    # Parent process
    my $result = COPS::FUNC::exec::cmd("$arg->{'prefix'} $openssl $arg->{'params'} 2>&1");
    
    COPS::print::debug("Openssl command returned result of\n$result\n");
    COPS::fifo::remove($fifo);
    $result =~ /(unable to load Private Key|Could not read private key|Could not find private key)/ and return 0;
    $result =~ /^Usage: $COPS::config::openssl_enc / and COPS::print::error('Internal error piping to openssl');
    $result =~ /RSA operation error/ and COPS::print::fatal('Key loaded but decryption failed (perhaps an incorrect private key loaded?)');
    chomp($result);
    return $result;
  }
  else
  {
    # Child process to write to named pipe
    COPS::print::debug("Child process spawned for pipe to FIFO!");
    while(1)
    {
      if(-p $fifo)
      {
            COPS::fifo::write($fifo,$arg->{'data'}); 
      }
      else
      {
            # Parent process has killed off the pipe
            exit(0);
      }
      sleep(1);
    }
  }
}

####################################################################################
sub keylength
{
  # Here, not in keys.pm as we call it or a check in this module, and calling outside of FUNC breaks our model
  my $key = shift;
  my $openssl = COPS::FUNC::which::binary('openssl'); # Check that the openssl binary exists
  $key = COPS::FUNC::file::sanitize($key);
  my $cmd = "$openssl rsa -in '$key' -pubin -text 2>&1 | grep Public-Key";
  my $r = COPS::FUNC::exec::cmd("$cmd");
  $r =~ s/\((.*)\)/$1/ or COPS::print::error("Cannot determine key length for key '$key'. Perhaps it is not in pkcs8 format?");
  $r =~ s/[^\d]//g;
  return $r;
}

sub padout
{
  my $arg = shift;
  my $padto = shift;
  $arg.="\n\x00";
  while(length($arg) < $padto)
  {
    # We don't really need to cryptographically secure random pad data.
    $arg.= COPS::FUNC::rng::alpha("/dev/urandom");
  }
  return $arg;
}

sub padin
{
  my $pad = 0; 
  while(length($pad) <= $COPS::config::padsize)
  {
    # We don't really need to cryptographically secure random pad data.
    $pad .= COPS::FUNC::rng::alpha("/dev/urandom");
  }
  return $pad."\x00\n";
}
  
####################################################################################
sub getversion
{
  my $content = shift;
  $content =~ /#---- COPS \(dfv(.*?)\)/ and return $1;
  return 0;
}

sub setversion
{
  my $content = shift;
  $content =~ /.*\n$/ or $content .= "\n";
  return "#---- COPS (dfv$COPS::config::datafile_version) START ----\n$content#---- COPS (dfv$COPS::config::datafile_version) END ----\n";
}

#sub convertkey
#{
  # Unused fallback if ssh-keygen doesn't work. 
#  my $arg = shift;
#  ($arg->{'passphrase'} and $arg->{'privatekey'}) or COPS::print::error("Missing either passphrase or publickey");
#  
#  my $public = COPS::FUNC::ssl::exec(
#    {
#      'fifo'       => $fifo,
#      'data'       => $arg->{'passphrase'},
#      'params'     => "rsa -in $arg->{'privatekey'} -pubout -passin file:$fifo",
#    }
#  );
#  
#  return $public;
#}

1;
