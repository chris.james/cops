# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::filter;
use strict;

sub entry
{
  my $arg = shift;

  # No filter, no filtering
  $arg->{'filter'} or return 1;

  my $logicRXP = "AND|NOT"; # Very simple AND/NOT logic to begin with. TODO: Add 'OR' and 'AND NOT'

  $arg->{'filter'} =~ /^NOT\s/ or $arg->{'filter'} = "AND $arg->{'filter'}"; # Simplify the flow control

  my $query = $arg->{'filter'};

  # Process in order from left to right
  while($query =~ /^($logicRXP)\s(.*)$/)
  {
    my $logic = $1;
    my $remain = $2;
    my $test = $remain;

    if($remain =~ /^(.*?)\s($logicRXP)\s(.*)/)
    {
      $remain = "$2 $3";
      $test = $1;
    }

    $test = quotemeta($test);

    if($logic eq "NOT")
    {
        $arg->{'name'} =~ /$test/i and return 0;
    }
    else
    {
        $arg->{'name'} !~ /$test/i and return 0;
    }
    $query = $remain;
  }
  return 1;
}

1;
