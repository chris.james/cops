# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::number;
use strict;

# Makes numbers pretty

sub expo
{
  my $arg = shift;
  $arg =~ /(\d*)x10^(\d*)/;
  $1 and $2 or COPS::print::error("Wrong format passed to internal function");

  my $result = 10 ** $2;
  $result = $1 * $2;

  return $result;
}

sub commify
{
  my $arg = shift;
  $arg or return $arg; # zero or null
  1 while $arg =~ s/^(-?\d+)(\d{3})/$1,$2/;
  $arg =~ /(\d\.\d*)e\+(\d+)/ and $arg = (int($1*100)/100)." *10^$2";

  return $arg;
}

1;