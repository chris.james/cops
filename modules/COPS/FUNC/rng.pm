# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::rng;
use strict;
use POSIX;

my $arch_limit=0;

sub generate
{
  # Accepts
  #   limit: Function generates an integer from 0 to this value
  #   source: rng source
  my $arg = shift;
  
  $arg->{'limit'} > 0 or COPS::print::fatal("Must provide limit to COPS::FUNC::rng");
  $arg->{'source'} or COPS::print::fatal("Must provide RNG source to generate function");

  unless($arch_limit)
  {
    # Only calculate this once!
    
    # Determine largest value obtainable, assuming 32 bit is minimum. If you're running this on 16 bit arch, well done for getting anything running at all! :)
    
    # We *shouldn't* ever hit this limit in cops, we don't go above an 8 bit request. But, we may code re-use at some point!

    $arch_limit = 2**32;
    my $arch=(POSIX::uname)[4];
    $arch =~ /_64/ and $arch_limit=2**64;
  }

  $arg->{'limit'} > $arch_limit-1 and COPS::print::error("Invalid limit (larger than your architecture supports) specified in internal function"); # We have to say -1, because we increment by one to force the initial loop and don't want to excede arch limits ever
  
  my $chunks=0;
  
  # Calculate the number of 4 bit chunks required to encompass '$arg->{'limit'}'.
  # We use 4 bit chunks so that we can utilise any rng that can generate at least a
  # 5 bit integer. We need 5 bit values because we offset by one in case of an
  # odd/even bias from our rng. This leaves us with a 4 bit integer as required.
  #
  # We could use 6 or 7 bit chunks ( most rngs output 8 bit integer ) but that feels messy
  # Four bits is fine.

  while($arg->{'limit'} > (16**$chunks)) { $chunks++; } # 16 (0-15) is the limit of a four bit integer. $chunks is number of 4 bit chunks required
  
  open(RNG, "<", $arg->{'source'}) or COPS::print::error("Failed to open RNG source $arg->{'source'}");
  
  my $val=$arg->{'limit'}+1; # Make val > limit to force first itteration
  # As we work on defined integer sizes, we need to discard values larger than the required limit and retry.
  until($val <= $arg->{'limit'})
  {
    my $bin=undef;

    if($arg->{'source'} eq "/dev/random" and COPS::FUNC::rng::entropy_pool() < 5)
    {
      # Sometimes, we just need to let the pool refill
      COPS::print::debug("Entropy pool diminished and blocking RNG selected. Waiting for refill");
      sleep(1);
      (COPS::FUNC::rng::entropy_pool() < 5) and COPS::print::fatal("Entropy pool diminished and blocking RNG selected. Try --weaker to resolve?");
    }

    # Generate the required number of 4 bit binary data chunks
    for(my $i=1; $i <= $chunks; $i++)
    {
      eof(RNG) and COPS::print::error("RNG filehandle closed unexpectedly!");
      my $x = getc(RNG);
      $x = ord($x);

      $x = sprintf("%05b", $x); # Convert to bin and add leading zeros if required to pad out to at LEAST 5 bits
      $x = substr($x, -5, -1);  # Drop the last bit now to remove any odd/even bias from our RNG

      # We now have a four bit random number with no odd/even bias
      $bin .= $x; # Concatenate values
    }

    # Now we convert that binary representation back to decimal
    defined($bin) or COPS::print::error("\$bin is undefined :( Internal logic fail!");
    $val = oct("0b$bin");
  }
  close(RNG);
  COPS::print::debug("RNG Output:$val");
  return $val;
}

sub get_range
{
  # A friendly UI to the main routine with some sanity checking
  # Accepts start and end value ... and also an optional rng source
  
  my ($a,$b,$source) = @_;
  
  ($b-$a < 2) and COPS::print::error("get_range must be called with > 1 range");

  $source or $source = $COPS::config::rngsource;
  
  $b or COPS::print::error("Endpoint must be positive");
  $a < $b or COPS::print::error("Start must be lower than finish! ($a < $b)");

  my $range = $b-$a;
  my $rn = generate({limit => $range, source => $source});
  $rn = $rn+$a;
  return $rn;
}

sub bool
{
  my $arg = shift;
  $arg->{'source'} or $arg->{'source'}=$COPS::config::rngsource;
  
  my $rn = generate({limit => 7, source => $arg->{'source'}}); # Flatten out any strangeness in our rng
  $rn = $rn % 2;
  return $rn;
}  

#############################################################################################
sub alpha
{
  my $source = shift; 
  my $r = get_range(0,61,$source);
  $r > 35 and $r = $r+61; # Lowercase letters
  ($r <= 35 and $r > 9) and $r = $r + 55; # Uppercase letters
  $r <=9 and $r = $r + 48; # Numbers
  return chr($r);
}

sub number
{
  my $source = shift;
  my $r = get_range(0,9,$source);
  $r = $r + 48;
  return chr($r);
}

sub character
{
  my $source = shift;
  my $r = 39;
  while($r == 39 or $r == 92) # Ignore single quotes as we explicitly don't like them with xdo. Also ignore backslashes, let's be compatible with ourself! :)
  {
    $r = get_range(33,126,$source);
  }
  return chr($r);
}
  
sub entropy_pool
{
  COPS::print::debug("Checking entropy pool size");
  my $ent_size = COPS::FUNC::file::readlocal("/proc/sys/kernel/random/entropy_avail");
  chomp($ent_size);
  COPS::print::debug("Stored entropy pool size is currently at ".(int(($ent_size/4096)*100))."%");
  return $ent_size;
}
1;
