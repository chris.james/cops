# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

# Just a simple routine to dump data regardless of type
package COPS::FUNC::debug;
use strict;

sub dump
{
  my @data=@_;
  my $lines;
  foreach(@data)
  {
    if( ref $_ eq "HASH")
    {
      $lines .= "HASH Ref\n";
      foreach my $key ( keys %{$_} )
      {
        if(ref $_->{$key} eq "HASH")
        {
          $lines .= "\tHASH Ref : $key\n";
          foreach my $key2 ( keys %{$_->{$key}} )
          {
            $lines .= "\t\tScalar : $key2 = $_->{$key}->{$key2}\n";
          }
        }
        else
        {
          $lines .= "\tScalar : $key = $_->{$key}\n";
        }
      }
    } 
    elsif( ref $_ eq "ARRAY")
    {
      $lines .=  "ARRAY Ref : $_\n";
    }
    else 
    {
      $lines .= "Scalar : $_\n";
    }
  }
  return $lines;
}

1;