# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.


package COPS::FUNC::endorse;
use strict;

# Endorsement works as follows
#
# Sign the encrypted file entry
# Base64 encode that signature
# Add a section to the file entry that consists of
#  A: An md5sum of the publickey ( simply to verify which publickey matches )
#  B: The endorsement ( signature ) in its base64 encoding


sub sign
{
  my $arg = shift;
  ($arg->{'passphrase'} and $arg->{'data'} and $arg->{'privatekey'} and $arg->{'publickey'}) or COPS::print::error("Missing parameter");
  my $fifo   = COPS::fifo::create();
  
  my $base64 = COPS::FUNC::which::binary('base64');
  my $pkey = COPS::FUNC::file::sanitize($arg->{'privatekey'});
  
  my $temp = COPS::FUNC::file::writetemp({
    data => $arg->{'data'}}
  );
  
  my $md5 = COPS::FUNC::file::md5sum($temp);
  COPS::FUNC::file::delete("$temp");
  
  my $endorse = COPS::FUNC::ssl::exec({
    'fifo'       => $fifo,
    'data'       => $arg->{'passphrase'},
    'params'     => "pkeyutl -sign -passin file:$fifo -inkey $pkey 2> /dev/null | $base64",
    'prefix'	 => "echo '$md5' |"
  });
  
  $endorse or COPS::print::fatal("Endorsement failed. Probable cause is an invalid private key passphrase");
  my $md5key = '00000000000000000000000000000000';
  if($arg->{'publickey'})
  {
    my $pubkeyfile = COPS::FUNC::file::status($arg->{'publickey'});
    if($pubkeyfile->{'status'})
    {
      my $pubkey = $pubkeyfile->{'path'};
      unless($pubkey=~/.*\.pem$/)
      {
        COPS::print::debug("Converting key $pubkey to pem");
        my $data=COPS::keys::convert($pubkey);
        $pubkey = COPS::FUNC::file::writetemp({
          data => $data
        });
      }
      $md5key = COPS::FUNC::file::md5sum($pubkey);
    }
  }
  
  $endorse or COPS::print::error("Failed to set endorsement");
  $endorse = "#---- COPS ENDORSEMENT START ----\n#---- Endorsed by $COPS::user::name $md5key ----\n$endorse\n#---- COPS ENDORSEMENT END ----";
  
  return $endorse;
}
  
sub check_endorsement
{
  my $data = shift;
	
  defined($data) or return 0;
  
  if($data =~ /.*#---- COPS ENDORSEMENT START ----\n#---- Endorsed by (.*?) ([0-9a-z]+) ----\n(.*)#---- COPS ENDORSEMENT END ----.*/s)
  {
    my $endorsed;
    $endorsed->{'by'}=$1;
    $endorsed->{'md5'}=$2;
    $endorsed->{'key'}=$3;
    
    $data =~ /.*(#---- COPS ENCRYPTION START ----\n.*\n#---- COPS ENCRYPTION END ----\n).*/s or COPS::print::error("Endorsement found, but no actual encrypted data");
    $endorsed->{'data'}=$1;
    return $endorsed;
  }
  return 0;
}
  
sub verify
{
  # requires data source and either 'store' or 'publickey'
  my $arg = shift;
  my $end = check_endorsement($arg->{'data'});
  $end or return 0;
 
  COPS::print::debug("Endorsement signature found\n");
  
  my $key = COPS::FUNC::file::writetemp({
    data => $end->{'key'}
  });
  
  my $sign = COPS::FUNC::file::writetemp({ data => '' });
  
  my $b64 = COPS::FUNC::which::binary("base64");
  COPS::FUNC::exec::cmd("$b64 -d $key > $sign");

  my $data = COPS::FUNC::file::writetemp({
    data => $end->{'data'}
  });
  
  my $md5 = COPS::FUNC::file::md5sum($data);
  
  my $pubkey;
  if($arg->{'store'})
  {
    $pubkey = $arg->{'store'}."/.cops_keys/$end->{'by'}".".pem";
  }
  elsif($arg->{'key'})
  {
    $pubkey = COPS::FUNC::file::sanitize($arg->{'key'});
    unless($pubkey=~/.*\.pem$/)
    {
      COPS::print::debug("Converting key $pubkey to pem");
      my $data=COPS::keys::convert($pubkey);
      $pubkey = COPS::FUNC::file::writetemp({
        data => $data
      });
    }
  }
  else
  {
    return "notverified";
  }

  my $openssl = COPS::FUNC::which::binary('openssl');
  my $verify = COPS::FUNC::exec::cmd("echo '$md5' | $openssl pkeyutl -verify -pubin -inkey $pubkey -sigfile $sign");

  $arg->{'key'} and $pubkey=$arg->{'key'}; # If we convert a key, we don't want to report the converted keyname with it's cops temp path :(

  $verify =~ /Signature Verified Successfully/ or return "Failed to verify endorsement by $end->{'by'}";
  
  COPS::print::debug("Signature verified");
  
  my $md5key =  COPS::FUNC::file::md5sum($pubkey);
  $end->{'md5'} eq $md5key and return "Verified endorsement by $end->{'by'} using key '$pubkey'";
  return "Failed to verify endorsement due to md5sum missmatch only. Signing OK!";
  
}
  


1;
