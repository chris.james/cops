# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::input;
use strict;
use Term::ReadKey;

our $loaded =1;

################################
sub wall
{
  $COPS::args::param->{'noprompts'} and COPS::print::fatal("User input requested, but cops is running with --noprompts. Aborting!");
  my $message = shift;
  defined($message) and COPS::print::std("Enter data ( control-D to finish )\n");
  my $resp;
  while(1)
  {
    my $line = COPS::FUNC::input::line();
    $line and $resp .= $line;
    $line or last;
  }
  return $resp;
}

################################
sub stdin
{
  my $data;
  while(<STDIN>)
  {
    $data .= $_;
  }
  return $data;
}

################################
sub date
{
  $COPS::args::param->{'noprompts'} and COPS::print::fatal("User input requested, but cops is running with --noprompts. Aborting!");
  COPS::print::std("Enter date of expiry in format YYYY-MM-DD\n");
  
  my $date='';
  
  while($date !~ /\d\d\d\d\-\d\d\-\d\d/)
  {
    $date = COPS::FUNC::input::line({allow=>"0-9\-"});
    $date !~ /\d\d\d\d\-\d\d\-\d\d/ and COPS::print::warn("Invalid date format. Please try again");
    $date =~ /(\d\d\d\d)\-\d\d\-\d\d/;
    # TODO: check for valid (and future) date format
  }
  return $date;
}
  
################################
sub line
{
  $COPS::args::param->{'noprompts'} and COPS::print::fatal("User input requested, but cops is running with --noprompts. Aborting!");
  my $arg = shift;

  my $line='';
  my $key='';
  
  until($key =~ /[\n\r]/)
  {
    #eof(STDIN) and COPS::print::error("STDIN has reached EOF but there is interactive data required");
    
    ReadMode 4;
    $key = ReadKey(0);
    ReadMode 0;
    if(defined $key)
    {
      my $ord = ord($key);

      # Handle controls first
      if($ord == 3)
      {
        COPS::print::fatal("Exiting on control-c");
      }
      # control D to finish
      if($ord == 4)
      {
        print "\n"; # Don't pass to COPS::print
        return $line;
      }

      # Backspace
      if($ord==127 or $ord==8)
      {
        if(length($line) > 0)
        {
          $arg->{'noecho'} or print chr(8).' '.chr(8);
          $arg->{'noecho'} and $arg->{'use-asterixes'} and print "*";
          chop($line);
        }
      }

      elsif($arg->{'allow'})
      {
        if($key =~ /[$arg->{'allow'}]/ or $key =~ /\n/)
        {
          $line .= $key;
          $arg->{'noecho'} or print $key;
          $arg->{'noecho'} and $arg->{'use-asterixes'} and print "*";
        }
      }
      else
      {
        $line .= $key;
        $arg->{'noecho'} or print $key;
        $arg->{'noecho'} and $arg->{'use-asterixes'} and print "*";
      }
    }
    else
    {
      $key = '';
    }
  }
  close(MYTERM);
  $arg->{'noecho'} and print "\n"; # Don't pass to COPS::print
  return $line;
}

################################
sub noecho
{
  # Sub to accept input on stdin without echoing to screen
  # Don't worry about stdin being set, we should never accept sensitive data from pipes

  # Breaking our 'no params in funcs' model again :(
  $COPS::args::param->{'noprompts'} and COPS::print::fatal("User input requested, but cops is running with --noprompts. Aborting!");


  my $message = shift;
  $message and COPS::print::out({type=>'warn', nolf=>1, text=>"$message: "});
  my $input = COPS::FUNC::input::line({"noecho"=>1, "use-asterixes"=>$COPS::args::param->{'use-asterixes'}});
  
  ($input =~ /^([^\\']+)$/ and $input = $1) || COPS::print::fatal("Bad characters found in input. Disallowed characters are ' and \\");

  return $input;
}

################################
sub clearbuffer
{
  # Exists in case anyone has started typing their passphrase or similar whilst not noticing that the countdown signal is running (or similar). Should not affect pipes into the utility as it is done LAST
  ReadMode 4;
  until(not defined(ReadKey(-1)) )
  {
    1;
  }
  ReadMode 0;
}
1;
