# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::which;
use strict;

sub binary
{
  my $binary = shift;
  my $nonfatal = shift;
  
  $binary or COPS::print::error("Failed to pass argument to function");
  $binary =~ s/'//g; # Make safe

  my $which = COPS::FUNC::exec::cmd("which '$binary' 2>&1");
  $which =~ s/\.\.//g;
  chomp($which);
  
  if($which =~ /(No such file or directory|which: not found)/)
  {
    COPS::print::error("Cannot find required binary which");
  }
  elsif($which =~ /\/usr\/bin\/which: no/ or $which eq "")
  {
    $nonfatal and return 0;
    COPS::print::fatal("Cannot find required external binary '$binary'");
  }
  
  # Which has returned a result, but it doesn't actually exist :(
  (-e $which) or COPS::print::fatal("Cannot find required external binary '$binary' found by 'which'");

  return COPS::FUNC::file::sanitize($which);
}

sub version
{
  my $arg = shift;
  # Make no assumption is that path etc has already been sanitized via which::binary

  my $bin = COPS::FUNC::which::binary($arg);
  my $version;

  if($arg eq "openssl")
  {
    my $v = COPS::FUNC::exec::cmd("$bin version");
    $v =~ /^(openssl|libressl) ([\d\.]+)/i or COPS::print::fatal("Cannot determine OpenSSL version (response: '$v')");
    $version = $2;
  }

  $version or COPS::print::fatal("Binary '$bin' passed to version check, but no logic found to determine version");

  return $version;
}


1;
