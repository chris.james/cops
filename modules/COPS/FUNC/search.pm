# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::search;
use strict;

sub entries
{
  my $arg = shift;
  
  # Required - text,store,username,type 
  # Optional - exactmatch, fuzzymatch, 
  
  # Returns hash file => filename, id => id
  
  ($arg->{'text'} and $arg->{'store'} and $arg->{'username'} and $arg->{'type'})
  or COPS::print::fatal("Failed to pass a required argument to COPS::FUNC::search");
  
  my $search = $arg->{'text'};
  
  $search =~ /[^0-9a-zA-Z\-\._]/ and COPS::print::fatal("Invalid characters in search term '$search'");
  
  $search =~ /\.\./ and COPS::print::fatal("Double dots are not permitted in a search term");
  
  my $entry_file;
  my $entry_id;
  
  my $type = $arg->{'type'};
  
  if($search =~ /^\d+$/)
  {
    # If the request is only digits, we treat it as an ID
    $entry_id=$search;
  }
  elsif($arg->{'exactmatch'} and !$arg->{'fuzzymatch'})
  {
     # Default is fuzzymatch, but can be passed as a parameter to override exactmatch
     $entry_id = COPS::FUNC::retrieve::id_by_name({
        name => $search, 
        store => $arg->{'store'}, 
        user => $arg->{'username'}
      });
  }
  else
  {
    # Do a search, and prompt if multiple matches
    my $entries = COPS::FUNC::retrieve::all_entries({
      store => $arg->{'store'}, 
      user => $arg->{'username'}
    });
  
    my @ids;
    foreach my $name (keys %{$entries})
    {
      # We don't want to match on any part of a user name, so discard that
      my $s = $name;
      $s =~ s/(.*)\-(.*)/$2/;
      $s =~ /$search/i or next;
      $entries->{$name}->{$arg->{'type'}}->{'file'} or next;
      push @ids,$entries->{$name}->{'id'};
    }
  
    if($entries)
    {
      my $display;
      foreach my $name (keys %{$entries})
      {
        foreach my $id (sort { $a <=> $b } @ids)
        {
          ($entries->{$name}->{'id'} eq $id) or next;
          $display->{$name} = $entries->{$name};
        }
      }
      
      if(keys %{$display} == 1)
      { 
        $entry_id = $display->{(keys %{$display})[0]}->{'id'};
      }
      elsif(keys %{$display} > 1)
      {
        my $content = COPS::text::password_list($display);
        COPS::print::std($content);
        COPS::print::notice("Multiple matches found. Please elaborate by entering an ID: ");
        $entry_id = COPS::FUNC::input::line({allow=>'0-9'});
        $entry_id =~ /\d/ or COPS::print::fatal("ID not specified");
        $entry_id > 0 or COPS::print::fatal("ID can not be zero");
      }
    }
  }
  
  # Once we are at this point, we should have a unique id, regardless of search method
  $entry_id or return 0;
  
  chomp($entry_id);
  if(defined($arg->{'backwards'}))
  {
    my $i = $arg->{'backwards'};
    while($i>0)
    {
    }
  }

  $entry_file = COPS::FUNC::retrieve::file_by_id({
      id => $entry_id, 
      type => $type, 
      store => $arg->{'store'}, 
      user => $arg->{'username'}
    });
        
  $entry_file or return 0;
  
  return ({ file => $entry_file, id => $entry_id });
}

1;