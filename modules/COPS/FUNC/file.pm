# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::FUNC::file;
use strict;

# Exceptions are fine where the call *should* die on failure.
# routines such as 'check' should pass status back.
# This is cleaner than passing a fatal/nofatal parameter from the caller.

my $file_status;

#############################################################################################  
sub read
{
  # requires scalar: source
  return COPS::FUNC::file::fetch(shift);
}

sub readasarray
{
  my $content = COPS::FUNC::file::fetch(shift);
  return split("\n",$content);
}

sub readoneline
{
  my $file = shift;
  my @content = COPS::FUNC::file::readasarray($file);
  return $content[0];
}

#############################################################################################  
sub write
{
  # requires hashref: target, content
  COPS::FUNC::file::send(shift);
}

sub delete
{
  my $arg = shift;
  COPS::print::debug("Deleting file '$arg'");
  $arg = sanitize($arg);
  return COPS::FUNC::exec::cmd("rm '$arg' -f");
}

sub dirlist
{
  my $arg = shift;
  my $dir = status($arg);
  $dir->{'type'} eq "directory" or COPS::print::fatal("Call to dirlist for non directory ($arg).");
  
  my @files=glob($dir->{'path'}."/*");
  
  return @files;
}  
  
#############################################################################################  
sub fetch
{
  my @call = caller(0);
  # Safety check
  $call[0] eq "COPS::FUNC::file" or COPS::print::error("Fetch called from external module $call[0]");
  # Fetch ( and its counterpart 'send' ) are used as abstraction layers where the storage source could be external to the local system.
  my $source = shift;
  if($source =~ /^(http|https):\/\//)
  {
    die "Future feature placeholder\n";
  }
  else
  {
    # default to a normal file based store.
    return COPS::FUNC::file::readlocal($source);
  }
}

sub send
{
  my $arg = shift;
  my @call = caller(0);
  $call[0] eq "COPS::FUNC::file" or COPS::print::error("Send called from external module $call[0]");
  
  my $target = $arg->{'target'};
  my $content = $arg->{'content'};
  
  if($target =~ /^(http|https):\/\//)
  {
    die "Future feature placeholder\n";
  }
  else
  {
    return COPS::FUNC::file::writelocal({file => $target, content => $content});
  }
}

sub readlocal
{
  my $file = shift;
  my $content;

  COPS::print::debug("Reading file '$file'");
  open(FH, "<", $file) or COPS::print::error("Failed to open '$file' for reading");
  while(<FH>)
  {
    $content .= $_;
  }
  close(FH);
  return $content;
}

sub pipeto
{
  my $arg = shift;
  ($arg->{'file'} and $arg->{'content'}) or COPS::print::error("'file' or 'content' not passed to function");
  
  COPS::print::debug("Piping to '$arg->{'file'}'");
  open(FH, "|$arg->{'file'}") or COPS::print::error("Failed to open '$arg->{'file'}' pipe");
  print FH $arg->{'content'};
  close(FH);
}

sub writelocal
{
  my $arg = shift;
  ($arg->{'file'} and $arg->{'content'}) or COPS::print::error("'file' or 'content' not passed to function");
  
  COPS::print::debug("Writing file '$arg->{'file'}'");
  open(FH, ">", $arg->{'file'}) or COPS::print::error("Failed to open '$arg->{'file'}' for writing");
  print FH $arg->{'content'};
  close(FH);
}

sub writetemp
{
  my $arg = shift;
  my $temp = $COPS::config::temp."/temp.".time().".$$.".int(rand(10000)).".cops";
  $temp = sanitize($temp);
  COPS::print::debug("Writing file $temp");
  open(FH, ">", $temp) or COPS::print::error("Failed to open '$temp' for writing");
  defined($arg->{'data'}) and print FH $arg->{'data'};
  close(FH);
  
  return $temp;
}


#############################################################################################  
sub sanitize
{
  my $arg = shift;
  ($arg =~ /^([\w\/\.\-\_]+)$/ and $arg = $1) || COPS::print::fatal("Bad characters found in path '$arg'");
  return $arg;
}

sub status
{
  # This runs the taint sanitiser. It also states if a path actually exists, which we may not care about.
  my $arg = shift;
  defined($file_status->{$arg}) and return $file_status->{$arg}; # Cache lookups

  my @call = caller(1);
  COPS::print::debug("Passed file '$arg' to examine status from $call[0]::$call[2]");
  defined($arg) or COPS::print::error("No argument passed to status. Aborting");
  $arg = COPS::FUNC::file::sanitize($arg);
  my $type='';
  my $status='';
  my $age=0;
  
  if(-e $arg)
  {
    $status=1;
    # ordering is important! A directory is also binary!
    (-f $arg) and $type='file';
    (-B $arg) and $type='binary';
    (-l $arg) and $type='symlink';
    (-T $arg) and $type='text';
    (-d $arg) and $type='directory';
    $type or $type = 'unknown';
    
    $age = time()-(stat($arg))[9];
  }
  else
  {
    $status=0;
  }
  $file_status->{$arg} = {'path' => $arg, 'type' => $type, 'status' => $status, 'age' => $age };
  return $file_status->{$arg};
}
  
sub mkdir
{
  my $dir = shift;
  $dir = sanitize($dir);
  (-d $dir) and return 1;
  COPS::FUNC::exec::cmd("mkdir '$dir'; chmod 0700 '$dir'");
  (-d $dir) and return 1;
  return 0;
}

sub md5sum
{
  my $file = shift;
  my $md5sum = COPS::FUNC::which::binary('md5sum');
  $file = sanitize($file);
  my $md5 = COPS::FUNC::exec::cmd("$md5sum $file");
  chomp($md5);

  if($md5 =~ /MD5 \(.*?\) = (.*)/)
  {
    # Mac :(
    $md5 = COPS::FUNC::exec::cmd("$md5sum -r $file");
  }
  $md5 =~ /^(.*?)\s.*/ and $md5 = $1;
  $md5 =~ /[^0-9a-f]/ and COPS::print::error("Invalid result from $md5sum");
  return $md5;
}

1;
