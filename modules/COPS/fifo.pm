# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::fifo;
use strict;
use POSIX qw(mkfifo);

sub create
{
  # Create a new named pipe entry. Actual creation is in 'make', this just gives us a 
  # unique ident
  COPS::fifo::check() and COPS::print::fatal("Found stale fifo files in $COPS::args::param->{'userdir'}/cops.fifo/ Remove and re-execute (cops clearfifo) or enable 'autoclearfifo' in ~/.copsrc\nIf you have already done this then investigate manually!");
  my $fifo = "$COPS::args::param->{'userdir'}/cops.fifo/fifo_"."_$$"."_".(int(rand(1000000))).".fifo";
  return $fifo;
}

sub check
{
  # Check if there is already anything in the fifo folder. There shouldn't be
  COPS::print::debug("Checking for stale fifo");
  my @fifocheck = glob("$COPS::args::param->{'userdir'}/cops.fifo/*.fifo");
  if(@fifocheck and $COPS::args::param->{'autoclearfifo'})
  {
      if(COPS::fifo::clear())
      {
        COPS::print::warn("Stale fifo entries found in $COPS::args::param->{'userdir'}/cops.fifo/ have been automatically cleared");
        return 0;
      }
      else
      {
        COPS::print::fatal("Stale fifo entries found in $COPS::args::param->{'userdir'}/cops.fifo/ that cannot be cleared automatically")
      }
  }
  return @fifocheck;
}

sub make
{
  my $fifo = shift;
  mkfifo("$fifo", 0700) or COPS::print::error("Cannot create named pipe $!");
  (-p $fifo) or COPS::print::error("Failed to create named pipe '$fifo' '$!'");
}

sub remove
{
  my $fifo = shift;
  unlink($fifo) or COPS::print::error("Failed to unlink $fifo. Remove manually and ensure child processes are killed off");
}

sub write
{
  my $fifo = shift;
  my $fifodata = shift;
  open(FIFO, "> $fifo");
  print FIFO "$fifodata";
  close(FIFO);
}

sub clear
{
  $COPS::args::param->{'autoclearfifo'}=0; # Reset to prevent infinite loop.
  foreach(COPS::fifo::check())
  {
    COPS::FUNC::file::delete("$_");
  }
  COPS::fifo::check() and return 0;
  return 1;
}

1;
