# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::store;
use strict;

sub entry
{
  my $entry = shift;
  
  # Data input
  #############
  my $data = '';

  if(lc($COPS::args::param->{'type'}) eq "expiry")
  {
    $data = COPS::FUNC::input::date();
  }
  elsif($COPS::config::inputtype->{$COPS::args::param->{'type'}} eq "secret")
  {
    if($COPS::args::param->{'stdin'})
    {
      $data =  COPS::FUNC::input::noecho();
    }
    elsif($COPS::args::param->{'nogen'})
    {
      $data = COPS::FUNC::input::noecho("Enter $COPS::args::param->{'type'}");
      my $pass_check =COPS::FUNC::input::noecho("Enter $COPS::args::param->{'type'} again");
      ($data ne $pass_check) and COPS::print::fatal("Entries do not match");
    }
    else
    {
      my $result = COPS::password::generate();
      my $wlsize   = COPS::FUNC::number::commify($result->{'wordlistsize'});
      $COPS::args::param->{'entropy'} and COPS::pwquality::entropy({
        password => $result->{'password'},
        wlsize => $wlsize,
        variants => $result->{'variants'}}
      );
      $data=$result->{'password'};
    }
  }
  else
  {
    COPS::print::std("Enter $COPS::args::param->{'type'} ( Control D to finish )\n");
    # Allow multiline input
    $data = COPS::FUNC::input::wall();
  }

  chomp($data);
  $data =~ /\x00/ and COPS::print::fatal("Null character(s) found in input. Rejected! Aborting!");
  $data =~ /##CC=/ and COPS::print::fatal("Internal use codes discovered in input. Rejected! Aborting!"); # Meh :(
  
  $COPS::args::param->{'onlyme'} and $entry = $COPS::user::name."-".$entry;
  
  $data eq "" and COPS::print::error("No data generated or passed to function");
  
  $COPS::args::param->{'hibp'} and COPS::pwquality::hibp($data);


  my $keys;
  
  if($entry =~ /^$COPS::user::name\-/)
  {
    $keys->{$COPS::user::name}=$COPS::args::param->{'store'}."/.cops_keys/$COPS::user::name.pem";
  }
  else
  {
    $keys = COPS::keys::retrieve();
  }
  
  # Make sure each entry has the same timestamp by setting it first
  my $ts = COPS::FUNC::time::stamp("long");
  
  my $dir = "$COPS::args::param->{'store'}/$entry/";
  COPS::FUNC::file::mkdir($dir) or COPS::print::error("Unable to create directory '$dir'");
  
  foreach my $key (sort keys %{$keys})
  {
    my $file;
    COPS::print::info("Encrypting data for $key\n");
    
    my $pp;
    
    my $enc = COPS::FUNC::ssl::encrypt({
        data => $data,
        publickey => $keys->{$key},
    });
    
    if($COPS::args::param->{'endorse'})
    {
      COPS::print::info("Endorsing entry with '$COPS::args::param->{'privatekey'}' : ");
      $enc .= COPS::FUNC::endorse::sign({
      privatekey => $COPS::args::param->{'privatekey'},
      publickey => $COPS::args::param->{'publickey'},
      passphrase => COPS::passphrase::get('endorse'),
      data => $enc
      });
    }
    
    my $target = $dir.$ts."_$key.$COPS::args::param->{'type'}";
    $target = COPS::FUNC::file::sanitize($target);
  
    COPS::FUNC::file::write({
      content => COPS::FUNC::ssl::setversion($enc),
      target  => $target
    });
  }
  return $data;
}
    
1;
