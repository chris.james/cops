# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::text;
use strict;

sub bold
{
   my $arg = shift;
   return "##CC=bold##$arg##CC=Reset##";
}
sub highlight
{
   my $arg = shift;
   return "##CC=".$COPS::config::colours->{'error'}."##$arg##CC=Reset##";
}

sub uline
{
   my $arg = shift;
   return "##CC=underline##$arg##CC=Reset##";
}

sub break
{
  # Creates a nice 'break' graphic depending on width
  my $break ='';
  $break =~ s/^(.*)/'-' x $COPS::args::param->{'listwidth'}/e;
  chop($break);chop($break);
  $break = "+$break+\n";
  return $break;
}

sub left
{
  my $text = shift;
  my $width = $COPS::args::param->{'listwidth'};
  $width = $width-4;
  return "| ".sprintf("%-".$width."s", $text)." |\n";
}

sub center
{
  my $text = shift;
  my $width = $COPS::args::param->{'listwidth'};
  $width = $width-4;

  my $left = sprintf("%".($width/2)."s",substr($text,0,(length($text)/2)));
  my $right = sprintf("%-".($width/2)."s",substr($text,length($text)/2));

  return "| $left$right |\n";
}

sub password_list
{
  my $entries = shift;
  my $search = shift;
  
  defined($entries) or COPS::print::error("No entries passed to password_list function");
  my $content;
  my $break = COPS::text::break();
  $content .= $break;

  # Dynamically work out the length of the header bar as we can then alter spans simply
  my $head =  "|   ID |  | ";

  # Add per field
  foreach my $type (@COPS::config::allowed_entry_types)
  {
    $head .= sprintf("%11s",ucfirst($type))." | ";
  }
  my $dynl = length($head)-1;

  # Offset by the initial header amount
  my $dync = sprintf("%-".($COPS::args::param->{'listwidth'}-$dynl)."s","Entry");

  # Add the enclosing pipes
  $head =~ s/\|  \|/\| $dync \|/;
  $content .= "$head\n";
  $content .= $break;

  my @order = sort {$entries->{$a}->{'id'} <=> $entries->{$b}->{'id'}}keys %{$entries};

  my $personal; # Hook for personal passwords
  my $index = 0;
  my $multi = 0;
  
  
  foreach my $pwname ( @order )
  {
    $index++;
    my $prefix='';
    my $boldprefix='';
    my $line;

    if(lc($pwname) =~ /^(.*?_)/ and $COPS::args::param->{'groupentries'})
    {
      $prefix = lc($1);
      if(defined($order[$index]) and lc($order[$index]) =~ /^$prefix/)
      {
        $boldprefix = bold($prefix);
        $multi or $line .= $break;
        $multi=1;
      }
      else
      {
        $multi and $boldprefix = bold($prefix);
        $multi=0;
      }
    }

    my $padded_pwname;
    if($COPS::args::param->{'listwidth'}-$dynl >= length($pwname))
    {
      $padded_pwname = sprintf("%-".($COPS::args::param->{'listwidth'}-$dynl)."s", $pwname);
    }
    else
    {
      $padded_pwname = substr($pwname,0,($COPS::args::param->{'listwidth'}-$dynl-2));
      $padded_pwname .= "..";
    }
    
    $boldprefix and $padded_pwname =~ s/^$prefix/$boldprefix/i;
    if(defined($search) and $search)
    {
      if($padded_pwname =~ /($search)/i)
      {
        my $m = $1;
        my $highlight=highlight($m);
        $padded_pwname =~ s/$m/$highlight/;
      }
    }

    $line .= "| ".sprintf("%4d",$entries->{$pwname}->{'id'})." | ".$padded_pwname." | ";
    foreach my $type (@COPS::config::allowed_entry_types)
    {
      (defined $entries->{$pwname}->{$type}->{'date'}) or $entries->{$pwname}->{$type}->{'date'} = '';
      $line .= sprintf("%11s",$entries->{$pwname}->{$type}->{'date'})." | ";
    }
    $line .= "\n";

    if($pwname =~ /\-/)
    {
      $personal .= $line;
    }
    else
    {
      $content .= $line;
    }
  }
  $content .= $break;

  if($personal)
  {
    $content .= COPS::text::center("-- Personal Passwords --");
    $content .= $break;
    $content .= $personal;
    $content .= $break;
  }

  return $content;
}

1;
