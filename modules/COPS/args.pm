# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.



package COPS::args;
use strict;
use Getopt::Long qw(:config bundling);

our $command  = '';
our $argument = undef;
our $param = undef;
our $wordlist;

# This module handles ALL command, argument and parameter input.
# It does not handle user input 

# Arguments are 'commands'
# There should only ever be zero or one arguments. Zero arguments will output the default help page

# There may be zero, or many parameters



# Define what is called when a command is used
our $cmds;
$cmds->{'params'} = "help::params";
$cmds->{'generate'} = "generate::start";
$cmds->{'decrypt'} = "decrypt::entry";
$cmds->{'dump'} = "dump::params";
$cmds->{'hash'} = "hash::entry";
$cmds->{'whoami'} = "general::whoami";
$cmds->{'version'} = "general::version";
$cmds->{'list'} = "list::entries";
$cmds->{'get'} = "get::entry";
$cmds->{'clearkeyring'} = "general::clearkeyring";
$cmds->{'clearfifo'} = "general::clearfifo";
$cmds->{'create'} = "create::entry";
$cmds->{'update'} = "update::entry";
$cmds->{'encrypt'} = "encrypt::data";
$cmds->{'autofill'} = "autofill::entry";
$cmds->{'addkey'} = "addkey::rsa";
$cmds->{'keychange'} = "keychange::start";
$cmds->{'refresh'} = "refresh::entries";
$cmds->{'search'} = "search::entries";
$cmds->{'revertpem'} = "revertpem::key";
$cmds->{'export'} = "export::entries";
$cmds->{'help'} = 'null'; # Placeholder

sub load
{
  # We must do getopts first
  COPS::args::init();

  $command  = $ARGV[0];
  $argument = $ARGV[1];
  $ARGV[2] and COPS::print::fatal("Unwanted command line argument $ARGV[2]");
  
  defined($command) or $command = 'help';
  if($command eq "add")
  {
    COPS::print::warn("'cops add' is now deprecated, please use 'cops create'");
    $command = 'create'; # Backwards compatability
  }
  defined($argument) or $argument = '';

  $command=lc($command);

  $command  =~ /[^a-z]/ and COPS::print::fatal("Invalid command '$command'");
  
  # Confirm that our second argument is specified if required. If not, change it to 'help'
  my $commands = COPS::CONFIG::commands::list();
  ($commands->{$command}->{'argument'} and !$argument) and $argument='help';
  
  $command = COPS::args::check_command($command); # Must be first as we set some variables depending on this
  COPS::args::check_valid();
  COPS::args::check_copsrc();
  COPS::args::check_options();
  COPS::args::autoset();
  COPS::args::parse();

  $command =~ /^(generate|create|update)$/ and COPS::args::check_wordlist();
  $command =~ /^(generate|help|hash|version)$/ or COPS::args::check_dirs();
  $command =~ /^(get|decrypt|refresh|autofill|keychange|search|export)$/ and COPS::args::check_sshkey('private');
  $command =~ /^(create|update)$/ and COPS::args::check_sshkey('public');
  $command =~ /^(create|update|get|autofill|addkey|encrypt|list|refresh|keychange)$/ and COPS::args::check_git();
}

#############################################################################################
sub check_command
{
  my $cmd = shift;
  my $dym='See "cops help" for usage.';
  
  # Check for shortened unambiguous commands
  unless($param->{'noshortcommands'} or $cmds->{$cmd})
  {
    my @matches;
    foreach my $k (sort keys %{$cmds})
    {
      $k =~ /^$cmd.*/ and push @matches, $k;
    }
    
    if(scalar @matches == 1)
    {
      unless($cmds->{$cmd})
      {
        $cmd = $matches[0];
        COPS::print::info("Accepting command as '$cmd'\n");
      }
    }
    elsif(scalar @matches > 1)
    {
      COPS::print::fatal("Ambiguous command. Did you mean '".join("' or '",@matches)."' ?");
    }
  }
  
  $cmds->{$cmd} and return $cmd;
  
  # Test for char swap typos
  for(0 .. -2 + length $cmd) 
  {
    substr((my $test = $cmd), $_, 2, reverse substr $cmd, $_, 2);
    ($cmds->{$test}) and $dym = "Did you mean 'cops $test'?";
  }
  COPS::print::fatal("Unknown command '$cmd'. $dym");
}

#############################################################################################
sub init
{
  # Here we initialise parameters based on defaults, copsrc or cli
  my $param_list = COPS::CONFIG::parameters::list();
  my %getopts;
  
  # Assign options to the parameter hash using getopts
  
  foreach my $name (keys %{$param_list})
  {
    # Assign both the single letter (if assigned) and long version names to getopt function.
    # We only worry about the full name within our code though
    
    my $getopt = $name;
    defined $param_list->{$name}->{'single'} and $getopt = "$param_list->{$name}->{'single'}|$getopt";
    
    # Set a default value if one exists
    # TODO: This should be a check to see if a default is defined, because it could be zero. 
    # I've made too many assumptions on defined elsewhere though and this would currently break many things :(
    
    (defined($param_list->{$name}->{'default'}) and $name ne 'xdo') and $param->{$name}=$param_list->{$name}->{'default'}; # xdo is a special case. See below

    # Override that if it was set in copsrc
    if($COPS::config::copsrc->{$name})
    {
      $param->{$name}=$COPS::config::copsrc->{$name};
    }

    my $match = '=';
    if(exists($param_list->{$name}->{'argument'}))
    {
      ($param_list->{$name}->{'argument'} eq 'optional') and $match = ':'; 
    }
    
    # Command line input will always override defaults or copsrc
    $param_list->{$name}->{'type'} and $getopt.="$match$param_list->{$name}->{'type'}";
    $getopts{"$getopt"}=\$param->{$name};
  }
  GetOptions(%getopts) or exit 1;
}

#############################################################################################
sub check_valid
{
  # Validate that any entered parameters are valid if they deviate from default or default is normally null
  my $config = COPS::CONFIG::commands::list();
  my $params = COPS::CONFIG::parameters::list();

  ($COPS::args::command eq "help" or lc($COPS::args::argument)eq 'help' or $COPS::args::param->{'help'}) and return;


  # If the command does not have any valid params, we'll ignore these checks.
  if(defined($config->{$command}->{'valid'}))
  {
    CHECK: foreach my $name (keys %{$COPS::args::param})
    {
      # Some paramaters are globally accepted
      grep(/^$name$/, @COPS::CONFIG::parameters::global) and next;
      
      defined($param->{$name}) or next; # Not set
            
      if(defined ($params->{$name}->{'default'}))
      {
        # Does it match the default setting?
        $params->{$name}->{'default'} eq $param->{$name} and next;
      }
      else
      {
        # No default set, so if this isn't set etiher ... ignore
        $param->{$name} or next;
      }
      
      # Ignore if set in copsrc and matches
      if(defined($COPS::config::copsrc->{$name}))
      {
        ($COPS::config::copsrc->{$name} eq $param->{$name}) and next; 
      }
      
      # Finally, this means it was supplied on the command line
      # Is it listed as a valid parameter?
      foreach(@{$config->{$command}->{'valid'}})
      {
        $name eq $_ and next CHECK;
      }
      
      my $letter='';
      defined $params->{$name}->{'single'} and $letter = "/-$params->{$name}->{'single'}";
      COPS::print::fatal("Supplied parameter --$name$letter is not valid with command '$command'");
    }  
  }

  # Check any required arguments are present
  my $check = COPS::CONFIG::arguments::check({command => $command, argument => $argument });
  if(ref($check) eq 'ARRAY')
  {
    COPS::print::fatal("Missing required argument(s) '".join(", ", @{$check})."'. Type 'cops $command help' for usage.");
  }
  $check == 1 and COPS::print::fatal("Command '$command' does not accept arguments");
  $check == 2 and COPS::print::fatal("Argument supplied to command '$command' is invalid");
  $COPS::args::cmds->{$command} or COPS::print::error("Internal logic error");

  my $commands = COPS::CONFIG::commands::list();

  # Check if the command has any required paramaters and bork if it does, and paramater not set
  if($commands->{$command}->{'requiredparam'})
  {
    foreach my $p (@{$commands->{$command}->{'requiredparam'}})
    {
      $COPS::args::param->{$p} or COPS::print::fatal("Missing required parameter '--$p'");
    }
  }
}

#############################################################################################

sub check_copsrc
{
  foreach(keys %{$COPS::config::copsrc})
  {
    $param->{$_} or COPS::print::warn("Ignoring unknown parameter in ~/.copsrc '$_'");
  }
}
 
#############################################################################################
sub parse
{
  # This is where we actually sanitise and check any supplied parameters that we need to
  $param->{'store'} =~ /.*\/$/ or $param->{'store'}.="/"; #Trailing slash
  
  if($param->{'listwidth'} < 60)
  {
    COPS::print::warn("--listwidth set too low. Resetting to '60'"); 
    $param->{'listwidth'}=60;
  }
  
  my @home= ("store","wordlist","publickey","privatekey","userdir","writeto","pipeto");
  # Convert ~/ where required. We must do this prior to creating those that need creating
  foreach(@home)
  {
    $param->{$_} or next;
    $param->{$_} =~ s/\~\//$ENV{"HOME"}\//g;
  }
  
  defined($param->{'type'}) and $param->{'type'} = lc($param->{'type'});
  
  (defined($param->{'xdo'}) and $param->{'xdo'} > 3) and COPS::print::fatal("Maximum limit of 3 itterations allowed to xdo!");
  ($param->{'klick'} and !$param->{'xdo'}) and COPS::print::warn("'klick' option is useless without --xdo");
  
  # Allow overiding local username via options, but because we use the name to construct file paths, sanitise it!
  if($param->{'whoiam'})
  {
       $param->{'whoiam'} =~ /[^a-zA-Z0-9\.\-\_]/ and COPS::print::fatal("Invalid characters found in local username");
       $COPS::user::name=$param->{'whoiam'};
  }
  
  if(defined $param->{'numbered'})
  {
    $param->{'numbered'} =~ /[^,\d]/ and COPS::print::fatal("Invalid characters in --numbered (only digits and commas allowed)");
  }
  
  $param->{'weaker'} and $COPS::config::rngsource='/dev/urandom';
  
  (defined( $param->{'countdown'}) and $param->{'countdown'} < 1) and COPS::print::fatal("Parameter 'countdown' cannot be less than 1");
  
  ($param->{'nosalt'} and $param->{'salt'}) and COPS::print::fatal("--salt and --nosalt are mutually exclusive. Define one, not both");
  
  ($param->{'xtimes'} > 1 and ($command eq "create" or $command eq "update") ) and COPS::print::fatal("--xtimes parameter invalid with create/update");
  
  (defined($param->{'keyring'}) and lc($param->{'keyring'}) eq "none") and $param->{'keyring'} = 0;
  
}  

#############################################################################################  
sub autoset
{
  # Some parameters to set automatically where required ( i.e. the usage of 
  # another parameter or command implies this should be set
  
  # --xdo is a strange one. The argument is optional and needs to default to 1.
  # But, because we actually hard set defaults above we cannot actually specify '1'
  # as a default in COPS::config as this would then make --xdo the default output method! :)
  # 
  # So, slightly hacky, if someone has passed -X without an argument it is 
  # set to 0. We want that to be 1
  if(defined $param->{'xdo'})
  {
    ($param->{'xdo'} eq '0') and $param->{'xdo'}=1;
  }

  if($command =~ /^(generate|decrypt|get|dump)$/)
  {
     # An explicit --show will still work regardless
     my $noshow;
     foreach("buffer","xdo","crypt","writeto","pipeto")
     {
        $param->{$_} and $noshow=1;
     }
     $noshow or $param->{'show'}=1;
  }
  $param->{'stdin'} and $param->{'nogen'}=1;
  
  $command eq "autofill" and $param->{'xdo'} = 1;
  
  if(($param->{'quiet'} or $param->{'silent'}) and !$param->{'stagewhisper'})
  {
    $param->{'noansi'} = 1;
  }
}  
  
#############################################################################################  
sub check_dirs
{
  # Check for the existence of required directories
  # Create those that need creating
  
  # These directories will be needed for nearly everything
  my @dirs = 
  (
    "$param->{'userdir'}",
    "$param->{'userdir'}/cops.fifo",
    "$param->{'userdir'}/cops.temp",
    "$param->{'userdir'}/cops.keyring",
    "$param->{'store'}",
    "$param->{'store'}/.cops_keys/",
  );
  
  # Run off and create them all
  foreach my $dir (@dirs)
  {
    COPS::FUNC::file::status($dir)->{'type'} eq 'directory' and next;
    COPS::FUNC::file::mkdir($dir) or COPS::print::fatal("Unable to create directory $dir. Create manually or alter --userdir as required");
    COPS::print::info("Auto-created directory '$dir'\n");
  }
  
  $COPS::config::temp="$param->{'userdir'}/cops.temp/";
  
  # Now pathcheck these to ensure they were created AND sanitise the variable
  foreach('userdir','store')
  {
    $param->{$_} = COPS::FUNC::file::sanitize($param->{$_});
  }
}
  
#############################################################################################
sub check_sshkey
{
  my $check = shift;
  defined($param->{$check."key"}) or COPS::print::fatal("parameter '${check}key' not defined");
  my $file = COPS::FUNC::file::status($param->{$check."key"});
  
  if($check eq 'private')
  {
    if($file->{'status'})
    {
       $param->{'privatekey'}=$file->{'path'};
    }
    else
    {
        COPS::print::fatal("Your ssh private key at location '$param->{'privatekey'}' could not be found, and is required for function '$command'. Either add your private key or change the parameter '--privatekey=<path>' to point to the correct location");
    }
  }
  elsif($check eq "public")
  {
    defined(COPS::keys::retrieve()) or COPS::print::fatal("The function '$command' requires at least one public key to be available ( and none were found !). Either add keys to $param->{'store'}/.cops_keys or use --publickey=<path>");
  }
}
  
#############################################################################################
sub check_wordlist
{
  if($param->{'wordlist-file'})
  {
    $COPS::args::wordlist = $param->{'wordlist-file'};
  }
  else
  {
    ($param->{'wordlist-size'} > 0 and $param->{'wordlist-size'} <4) or COPS::print::fatal("Specified wordlist size is out of range (1-3)");
    $COPS::args::wordlist = "${COPS::root}/wordlists/wordlist.$param->{'wordlist-size'}.txt";
  }
  my $check = COPS::FUNC::file::status($COPS::args::wordlist);
  $check->{'status'} or COPS::print::fatal("Specified wordlist of '$COPS::args::wordlist' cannot be found!");
  $COPS::args::wordlist = $check->{'path'};
}

#############################################################################################
sub check_git
{
  (-d $param->{'store'}.".git") or return;
  my $git = COPS::FUNC::which::binary("git",'nonfatal');
  if($git)
  {
    my $result = COPS::FUNC::exec::cmd("$git --work-tree='$param->{'store'}' --git-dir='$param->{'store'}/.git' status -s");
    if($result)
    {
      # There are untracked changes in git.
      # Itterate through those untracked files and see how old they are
      foreach my $file (split("\n",$result))
      {
        $file =~ s/.*\s(.*)$/$1/;
        $file = "$param->{'store'}/$file";
        if(COPS::FUNC::file::status($file)->{'status'})
        {
          my $age = time()-(stat($file))[9];
          # Ignore those changes if they are newer than our defined commit_age
          # This prevents an immediate warning when you add a new entry
          ($age < $COPS::config::commit_age) and next;
        }
        COPS::print::warn("Untracked changes in password store! Review and commit as required");
        last;
      }
    }
  }
}
        
#############################################################################################        
sub check_options
{
  # getopts automatically handles checking of parameter input, except where I'm using a string
  # and specifying my own option list. 
  
  # So, we'll check any parameter that matches that criteria and ensure any option 
  # passed is valid
  
  my $param_list = COPS::CONFIG::parameters::list();
  foreach my $key (keys %{$param_list})
  {
    $param_list->{$key}->{'options'} or next;
    $param->{$key} or next;

    my $option_ok=0;
    my @options = split(/\|/,$param_list->{$key}->{'options'});

    foreach(@options)
    {
      ($param->{$key} eq $_) and $option_ok=1;
    }
    $option_ok or COPS::print::fatal("Parameter '--$key' specified with invalid value of '$param->{$key}'\n  Valid options are '$param_list->{$key}->{'options'}'\n");
  }
}
1;
