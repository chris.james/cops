# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::keys;
use strict;

our $length;

sub retrieve
{
  my $path = COPS::FUNC::file::sanitize($COPS::args::param->{'store'}."/.cops_keys/");
  my $keys;

  my $group;
  my $groupname = $COPS::args::param->{'group'};
  if($groupname)
  {
    $group=COPS::user::get_groups();
    $group->{$groupname} or COPS::print::fatal("Group '$groupname' does not exist");
  }
  
  foreach (glob("$path/*.pem"))
  {
    /(.*)\/(.*)\.pem/ or next;
    my $name = $2;
    $name =~ /[^a-zA-Z0-9\.\-]/ and COPS::print::fatal("Invalid characters in username '$name'");
    $keys->{$name}=COPS::FUNC::file::sanitize($_); # Name -> path
  }

  $keys or COPS::print::fatal("No public keys found in '$path'. Aborting");
  
  # Now integrity check
  COPS::integrity::check($keys,"key");
  
  # Then, filter keys we actually want to use by group if required
  my $new_keys;
  foreach my $name (keys %{$keys})
  {
    ($group and !$group->{$groupname}->{$name}) and next;
    $new_keys->{$name}=$keys->{$name};
  }
  
  # Return the 'good' list
  return $new_keys;
}

sub maxlength
{
  # The maximum amount of bytes that can be encrypted with this particular key
  my $key = shift;
  $length->{$key} and return $length->{$key}; # Already set for this key

  my $len = COPS::FUNC::ssl::keylength($key);
  my $maxlength = (int($len - 384) / 8) + 37;
  $length->{$key} = $maxlength;
  return int($maxlength);
}

sub convert
{
  my $key = shift;
  my $content = COPS::FUNC::file::read($key);
  $content =~ /ssh\-rsa\s[a-zA-Z0-9\+\/\=]*(\s.*\n|\n)/ or COPS::print::fatal("SSH rsa public key ('$key') is not in an expected standard format");
  my $sshk = COPS::FUNC::which::binary('ssh-keygen','nofatal');
  
  if($sshk)
  {
    # First, see if we can do a direct convert using ssh-keygen
    my $sshcall = COPS::FUNC::exec::cmd("$sshk -f $key -e -m PKCS8 2>&1");
    $sshcall =~ /BEGIN PUBLIC KEY/ and return $sshcall;  

    # Failing that, go for a two pronged approach
    my $temp = COPS::FUNC::file::writetemp();
    my $cmd = "$sshk -f '$key' -e -m pem > $temp";
    my $result = COPS::FUNC::exec::cmd($cmd);
    my $openssl = COPS::FUNC::which::binary('openssl');
    $cmd = "$openssl rsa -RSAPublicKey_in -in '$key' -pubout";
    $result = COPS::FUNC::exec::cmd($cmd);
    $result =~ /BEGIN PUBLIC KEY/ and return $result;  
  }
  # If none of that works, advise the user on a manual method
  # We don't do this for them because otherwise we've got to prompt for their private key,
  # which isn't immediately obvious as to why!
  COPS::print::fatal("Failed to convert public key '$key' to PKCS8 format. See 'cops addkey help' for assistance on a manual method using openssl. Alternatively, you may be able to specify a different key with --publickey");
}

sub revert
{
  my $key = shift;
  $key = $COPS::args::param->{'store'}."/.cops_keys/$key.pem";
  if($key =~ /^([a-zA-Z0-9\/\._\-]+)$/)
  {
    $key = $1;
  }
  else
  {
    COPS::print::fatal("'$key' failed sanity check\n");
  }
  my $content = COPS::FUNC::file::read($key);
  $content =~ /\-{4}BEGIN PUBLIC KEY\-{4}/ or COPS::print::fatal("PEM public key ('$key') is not in an expected standard format");
  my $sshk = COPS::FUNC::which::binary('ssh-keygen');
  
  if($sshk)
  {
    my $sshcall = COPS::FUNC::exec::cmd("$sshk -f $key -i -mPKCS8");
    $sshcall =~ /^ssh-rsa\ / and return $sshcall;  
  }
  COPS::print::fatal("Failed to convert public key '$key' to RSA format.");
}

sub get_public
{
  my $key = $COPS::args::param->{'publickey'};
  $key or COPS::print::fatal("--publickey parameter must be used with this function");
  my $k = COPS::FUNC::file::status($key);
  
  $k->{'status'} or COPS::print::fatal("Unable to load required public key of '$key'");
  $key = $k->{'path'};
  
  unless($key =~ /.*\.pem$/)
  {
    my $temp = COPS::FUNC::file::writetemp({ 
      dir => $COPS::args::param->{'userdir'},
      data => COPS::keys::convert($key)
    });
    $key=$temp;
  }
  return $key;
}
1;
