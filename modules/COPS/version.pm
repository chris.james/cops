# LICENSE    This file is part of COPS
# LICENSE
# LICENSE    COPS is free software: you can redistribute it and/or modify
# LICENSE    it under the terms of the GNU General Public License as published by
# LICENSE    the Free Software Foundation, either version 3 of the License, or
# LICENSE    (at your option) any later version.
# LICENSE
# LICENSE    COPS is distributed in the hope that it will be useful,
# LICENSE    but WITHOUT ANY WARRANTY; without even the implied warranty of
# LICENSE    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LICENSE    GNU General Public License for more details.
# LICENSE
# LICENSE    You should have received a copy of the GNU General Public License
# LICENSE    along with COPS.  If not, see <http://www.gnu.org/licenses/>.

package COPS::version;
use strict;

sub checkset
{
  my $version='';
  my $vfile = COPS::FUNC::file::sanitize($COPS::args::param->{'userdir'}."/last_version");
  if(COPS::FUNC::file::status($COPS::args::param->{'userdir'})->{'type'} eq 'directory')
  {
    if(COPS::FUNC::file::status($vfile)->{'type'} eq 'text')
    {
      $version = COPS::FUNC::file::readlocal($vfile);
      chomp($version);
    }
  
    if($version ne $COPS::config::version)
    {
      # This fails on a downgrade, but I don't care. Who would downgrade huh ? :)s
      COPS::print::info("## You have recently updated to a newer version of cops.\n## Please check the changelog at $COPS::root/CHANGELOG.md for details\n");
    }
    COPS::FUNC::file::writelocal({file => $vfile, content => $COPS::config::version});
  }
}

1;
