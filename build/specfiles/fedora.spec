%define version VERSION

Name: cops
Version: %{version}
Release: fedora
Summary: Chris' Open Password System
BuildArch: noarch
BuildRoot: $RPM_BUILD_DIR/Cops2
Group: Cops
License: GPL

Requires: perl
Requires: openssl
Requires: coreutils
Requires: openssh
Requires: xsel
Requires: xdotool
Requires: perl-Term-ANSIColor
Requires: perl-TermReadKey
Requires: perl-Time-Out
Requires: perl-Digest-SHA
Requires: perl-Digest-MD5
Requires: perl-Digest-MD4
Requires: perl-Digest-Bcrypt
Requires: perl-Crypt-Rijndael
Requires: perl-Crypt-SmbHash
Requires: perl-Crypt-CBC
Requires: perl-Getopt-Long
Requires: perl-Moo
Requires: perl-List-AllUtils
Requires: perl-List-SomeUtils-XS
Requires: perl-HTTP-Tiny
Requires: perl-IO-Socket-SSL
Requires: perl-Module-Runtime
Requires: perl-Devel-GlobalDestruction
Requires: perl-Module-Implementation
Requires: perl-Try-Tiny

Conflicts: cops-nogui

%description
COPS is a secure password storage mechanism based on the principle of utilising
ssh asymetric keypairs for encryption and decryption, allowing for safe central
password storage serving multiple users.

%prep
exit 0

%build
exit 0

%post
%{__rm} -f /usr/bin/cops
ln -s /opt/Cops2/cops /usr/bin/cops

%preun
if [ $1 -lt 1 ] ; then
  %{__rm} -f /usr/bin/cops
fi


%install
mkdir $RPM_BUILD_ROOT/opt/Cops2/ -p
cp -a $RPM_BUILD_DIR/Cops2/ $RPM_BUILD_ROOT/opt/
exit 0

%files
%defattr(-,root,root)
/opt/Cops2/

%changelog

