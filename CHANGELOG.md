# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/)

Major.Minor.Bugfix
As of v1.27, odd minor numbers are development releases. Even minor numbers are stable

Dates are in UK format (yyyy/mm/dd)

## 2.60.0 [2024/10/22]
### Added
'--easygen' option for create/generate/update. This vaguely mimics the '--be-useless' function in cops V1 in that it allows the user to generate a strong password, whilst meeting arbitrary (and likely useless) legacy password complexity requirements.


## 2.58.3 [2024/09/30]
### Fixed
Random character passwords could incorrectly include ASCII 127 (DEL), which caused issues with some copy/paste actions

## 2.58.2 [2024/09/20]
### Changed
Caching of file status lookups for efficiency

## 2.58.1 [2024/09/20]
### Fixed
Incorrect messaging for failed key passphrase on some distros

## 2.58.0 [2024/07/07]
### Merged 2.57.0 into production

## 2.57.0 [2024/06/19]
### Jumped additional dev version due to major changes on pwquality
### Changed
- Removed all of the additional 'fluff' around checking for password strength
- As zxcvnb library is now no longer used, removed the required modules from checks
- Changed --check into --entropy and --hibp for the entropy calcs and hibp api checks respectively
- Moved 'check::key' into 'revertpem' command (used for easily reverting pems into rsa ssh keys)

## 2.55.1 [2024/06/18]
### Fixed
- Colour coding issue on 'list'

## 2.55.0 [2024/04/19]
### Changed
## Lots of the --check functionality for better reporting/analysis

## 2.54.0 [2024/04/16]
### Merged Dev to Live

## 2.53.0 [2024/03/18]
### Added
- --use-asterixes option to echo asterixes to the user when typing secret data

### Fixed
- Cops was not correctly reporting when no entries were found based on a search
- Help text was incorrect when invoked in the format 'cops help unknown-command'

## 2.52.3 [2024/03/14]
### Fixed
- Openssl version detection on MacOS
- MD5sum operation on MacOS

## 2.52.1 [2024/02/27]
### Fixed
- Password list ID's incorrectly starting at '0' causing inability to retrieve first entry

## 2.52.0 [2024/02/15]
### Merged 2.51.x into production

## 2.51.3 [2024/02/14]
### Fixed
- Case (in)sensitivity with filtering

## 2.51.2 [2024/01/03]
### Fixed
- Further issues with filtering, and specifically the -B flag

## 2.51.1 [2024/01/03]
### Fixed
- The updated filter function broke the entry id persistence. Now fixed.

## 2.51 [2023/12/11]
### Added
- '--onlyme' now works as a filter for 'list' and 'export'

### Fixed
- longdesc incorrectly used for a couple of paramaters
- filter code now moved into FUNC::retrieve

## 2.50.2 [2023/09/18]
### Added
- Fixed checking for valid args prior to checking for --help syntax
- Cleaned up use of defunct local scope $command var in switch.pm

## 2.50.1 [2023/09/16]
### Added
- '-h' and '-V' flags for --help and --version

## 2.50.0 [2023/09/13]
### Added
- "--filter" flag for list/export commands
- "--export-format" flag to allow csv exports to be customised (see help for details)
- cops <command> --help now also works and is analogous to cops <command> help
- packages.csv file to potentially help provide details of required packages for new deployments

### Changed
- crypt::cipher to use updated algorithms
- Global OpenSSL version check for various flags/options/parameters. NOTE: Your keyring integration may be cleared (if used) on updating to this version
- Binary version check functionality

## 2.48.4 [2023/08/11]
### Fixed
- Automatic polling of CHANGELOG.md for latest version number


## 2.48.3 [2023/08/04]
### Changed
- Clearer messaging for the get_pin function
- Format of version/date in CHANGELOG.md
- Automatic polling of CHANGELOG.md for latest version number

## 2.48.2 [2023/07/10]
### Fixed
- Resolved parameter of --maxlength was having no effect with -t username on generate.

## 2.48.1 [2023/06/15]
### Fixed
- Deprec warnings for openssl >= v3

## 2.48.0 [2022/12/22]
### Fixed
- Warning message in crypt for passphrases on some OS
- Warning message for > 72 characters for a bcrypt hash

### Added
- --stdin facility for encrypt/decrypt
- --silent flag (quieter than --quiet)

## 2.46.4 [2022/12/15]
### Fixed
- Temporary fix for rsautl deprecation in openssl V3

## 2.46.3 [2022/12/14]
### Fixed
- Incorrect output for bad passphrase on some OS implementation

## 2.46.2 [2022/11/16]
### Fixed
- Output order for --check

## 2.46.1 [2022/08/17]
### Fixed
- Cut/paste error causing 'cops hash' to fail

## 2.46.0 [2022/08/08]
### Changed
- Changed 'cops add' to 'cops create'
- Updated the examples document
- Moved some hashing functionality into 'crypt.pm' for re-usability
- Modified level 1 wordlist to be only five letter words
### Added
- Added export functionality
### Fixed
- Minor code improvements
- environment_check script

## 2.44.3 [2022/02/11]
### Fixed
- Some formatting issues
- Required paramater checks

## 2.44.2 [2022/02/08]
### Fixed
- Now ignores blank lines in ~/.copsrc

## 2.44.1 [2021/10/03]
### Fixed
- Breaking issues with move from 'add' to 'create'

## 2.44.0 [2021/09/30]
### Merged Dev in Master

## 2.43.5 [2021/09/14]
### Dev
### Added
Functionality for expiry field check/warn

## 2.43.4 [2021/09/13]
### Dev
### Added
--checkkey option
expiry date field (TODO: Auto check expiry on cops get)
'cops add' deprecated in favour of 'cops create' as it prevents ambiguity. Backwards compatibilty retained.

## 2.43.3 [2021/06/29]
### Dev
### Fixed
- Keys integrity check now allows for the same key to be used in different password stores.
- --pipeto messaging

## 2.43.2 [2020/11/29]
### Dev
### Added
- New parameter --group allows you to use the same filestore for different users and segregate those users by groups. Further info in /Docs

## 2.43.1 [2020/07/17]
### Dev
### Fixed
- 'which' handling for binary search
- Nonfatal error for missing binary 'git'

## 2.43.0 [2020/02/22]
### Dev
### Fixed
- Incorrect hashed_entry call
- More sanitisation of binary checks in environment_check
- Check filename passed to file::status prior to examining it

### Added
- --writeto and --pipeto parameter for get/update/add/etc

## 2.42.4 [2020/03/02]
### Fixed
- Bug in xdoout - apparent on some OS types

## 2.42.3 [2020/02/18]
### Changed
- Moved Time::Out to a static local copy to solve dependency/package issues on some distros

## 2.42.2 [2020/01/30]
### Fixed
- more Kwalletd detection - Failures on centos7 - must add regression tests!

## 2.42.1 [2020/01/30]
### Fixed
- Kwalletd detection - Failures on ubuntu 19.10

## 2.42.0 [2020/01/16]
### Changed
- Much more functionality to the environment_check script. This now gives much more detailed information on failures

### Fixed
- A lot of documentation and error handling
- Changelog markdown :(

## 2.40.0 [2019/12/19]
### Added
- bcrypt hashing functionality
- Much better handling of unhandled errors, including prompts for missing modules

## 2.38.3 [2019/12/17]
### Fixed
- Erroneous newline on hash command

## 2.38.2 [2019/12/07]
### Fixed
- KWallet handler check for CentOS7 etc

## 2.38.1 [2019/11/06]
### Fixed
- KWallet handler for F31 onwards

## 2.38.0 [2019/10/09]
### Added
- Option to 'generate' to allow generation of usernames via -t username

## 2.36.0 [2019/07/30]
### Added
- keyringpin option: Allows you to provide a short pin to further secure your key and passphrase when using a keyring

### Fixed
- Added more global options

## 2.34.0 [2019/06/11]
### Merged dev to live

## 2.33.1 [2019/06/11]
### Fixed
- kdewallet process check before invocation of qdbus calls

## 2.33.0 [2019/02/28]
### Added
- '--encryptfor' parameter added

### Fixed
- Minor typo in COPS/keys.pm
- Control-D without enter caused zero return (and error) on things like cops update -t username

## 2.32.1 [2019/02/21]
### Fixed
- Resolved issues with dependencies on various packages

## 2.32.0 [2019/02/21]
### Merged
- Released 2.31.4 to live version 2.32.0

## 2.31.4 [2019/02/20] - Dev release
### Added
- You can now pass --check to add/update/get

### Changed
- A lot of function calls in the backend to sanitise flow
- cops generate no longer outputs strength info by default. Use --check as required
- cops strength output now maxs out at 'more than a hundred centuries' to bring it inline with zxcvbn output.

## 2.31.3 [2019/02/06] - Dev release
### Changed
- Colour encoding for 'highlight' now matches 'error'
- Updated some documentation in Docs/
- Modified termux installer

## 2.31.2 [2019/02/06] - Dev release
### Fixed
- Issue with custom colours on commands such as 'list' where inline highlighting was required

## 2.31.1 [2019/02/03] - Dev release
### Fixed
- Soft fail if missing required libraries from zxcvbn library. Broke centos 7 implementation with no simple upstream rpm for Moo

## 2.31.0 [2019/01/31] - Dev release
### Added
- cops check function and --check to generate. Uses HIBP API and zxcvbn library
- Change cops generate messaging

### Fixed
- undefined error when --keyring not specified

## 2.30.3 [2019/01/23]
### Added
- private and publickey options to refresh
- keyring to global options

## 2.30.2 [2019/01/03]
### Added
- keyring can now be set to 'none' on the command line

## 2.30.1 [2018/12/19]
### Fixed
- privatekey allowed on add and update as may be endorsed

## 2.30.0 [2018/11/28]
### Added
- Termux support (beta)
- You can now add a search term to 'cops list'. Matching terms are highlighted. Useful for seeing what entries are available, eg password, username etc

## 2.28.0 [2018/11/15]
### Added
- '--use-spaces' param to use spaces instead of hyphens to separate words

## 2.26.2 [2018/11/02]
### Fixed
- Minor bug in list output that put  alinebreak in the wrong place when using --groupentries. Personal passwords were not being seperated correctly.

## 2.26.1 [2018/09/16]
### Fixed
- Bug in file.pm where COPS::PRINT was called rather than COPS::print. Another in store.pm that was missed.
- Error messaging for cops add/update when no keys found in the password store

### Added
- Stored keys integrity check

## 2.26.0 [2018/09/01]
### Added
- Stored keys integrity check

## 2.24.4 [2018/08/30]
### Changed
- Moved modules directory so that MacOSx with case insensitive file systems can use cops without modification
- Changed OS check to include Mac

## 2.24.3 [2018/08/20]
### Fixed
- Ambiguous command on add,addkey. Now checks for explicit match first

## 2.24.2 [2018/08/20]
### Fixed
- Changed 'accepted command' message so it does not display if you have already provided the full command name

## 2.24.1 [2018/08/16]
### Changed
- Output message on INFO when using shortened commands
- TODO/Bug comment for global parameters

## 2.24.0 [2018/08/16]
### Added
- Cops can now accept shortened commands. eg, 'cops v' will invoke 'cops version'. Shortened commands must be unabiguous or a FATAL is thrown with information.
### Fixed
- Croak message with invalid parameter

## 2.22.5 [2018/05/09]
### Changed
- Method of calculating entropy. This is just cleaner.

## 2.22.4 [2018/05/01]
### Fixed
- Fixed bug with --xdo that was casuing rogue processes to hang around

## 2.22.3 [2018/04/19]
### Fixed
- Some parameter descriptions. Cosmetic only

## 2.22.2 [2018/04/19]
### Fixed
- Keychange command now fixed, and more informative than before

## 2.22.1 [2018/04/17]
### Fixed
- Optionalarg for search now in place ( allows prompted input )
- Search now case insensitive
- Progression now indicated by filenames rather than dots in search

## 2.22.0 [2018/04/16]
### Added
- Cops now alters its process to 'cops [redacted]'. This does not solve the problem of leaked information via process listing, but greatly negates the window that this information is available within.
- Added search function

### Flagged
- Major bug spotted in 'keychange' command. See comments for details. Added croak for now even though this is live 'stable' branch. *mustfix*

## 2.20.0 [2018/02/09]
### Merged 2.19.0 into stable branch

## 2.19.0 [2018/01/19]
### Added
- Dev release - Added --noprompts to facilitate inline usage of cops in configuration setups

## 2.18.6 [2018/01/19]
### Fixed
- Bug in help pages for commands with no paramaters

## 2.18.5 [2018/01/16]
### Fixed
- Bug in parameter value sanitisation

## 2.18.4 [2018/01/12]
### Fixed
- Minor bug in copsrc parser

## 2.18.3 [2018/01/12]
### Added
- --xdo-delay parameter for --xdo.

## 2.18.2 [2018/01/01]
### Fixed
- Erroneous character output on non-secret add/update

## 2.18.1 [2017/12/30]
### Fixed
- ID specification bug when hitting enter on an empty string with 'cops get'

## 2.18.0 [2017/12/17]
### Added
- All entries are now chomped on input ( password/notes/username )
- '--autochomp' parameter to remove last newlines from any output
- Functionality for --quiet to imply --noansi
- '--stagewhisper' parameter to allow ansi encoding when using '--quiet' (restores previous behaviour)

## 2.16.5 [2017/12/04]
### Fixed
- '--quiet' now silences valid endorsements
- Version notice now on 'info' so it is supressed with --quiet

## 2.16.4 [2017/12/02]
### Fixed
- '--stdin' not working (at all) on add/update

## 2.16.3 [2017/09/12]
### Fixed
- Generate not showing password when used with --crypt

## 2.16.2 [2017/09/06]
### Fixed
- Hashed output not being shown on update/add
- encrypt function not working (at all).

### Todo
- Improve QA process :(

## 2.16.1 [2017/09/06]
### Fixed
- Jumpmethod in copsrc not accepting hyphens or dots

## 2.16.0 [2017/09/06]
### Added
- --groupentries option for 'list'
- Can now specify per-entry jumpmethod in copsrc. Would be perhaps better to store in another field in the cops data structure, but this is simple to modify!

### Fixed
- Change parameters to be identified by name, not single letters. Major backend improvement for future growth

## 2.14.6 [2017/08/10]
### Fixed
- Entering a passphrase message should be on stderr

## 2.14.5 [2017/08/09]
### Fixed
- > password length not being checked when using -N
- Versioning in build scripts

## 2.14.4 [2017/08/07]
### Fixed
- Another endorsement bug

## 2.14.3 [2017/08/07]
### Fixed
- Various bugs with endorsement
- Missing dfv info on refresh

## 2.14.0 [2017/07/18]
### Added
- '--backwards' option to retrieve historical entries
- date now displayed when retrieving an entry

## 2.13.0 [2017/07/14]
### Fixed
- Fixed -X itterations. Was only running once disregarding parameter option
- Improved feedback for misstyped commands

### Added
- Started adding --backwards parameter

## 2.12.3 [2017/06/23]
### Added
- ubuntu/deb build scripts (no functionality change to cops)

### Fixed
- Improved ssh key conversion function

## 2.12.2 [2017/06/19]
### Fixed
- Fixed some uninitialised variables

## 2.12.1 [2017/06/19]
### Fixed
- Erroneous statement that passphrase was unlocked for endorsement on 'get'

## 2.12.0 [2017/06/19]
### Added
- --numbered parameter to specify characters from the password to be displayed

### Fixed
- cops help was failing following previous update
- cops update was failing when an entry existed ( and name was specified exactly ) but the subtype did not exist

## 2.10.1 [2017/06/14]
### Added
- --salt and --nosalt options to cops get ( were previously only on cops generate and hash )

### Fixed
- help pages on non-existent commands ( now borks correctly )

## 2.10.0 [2017/05/24]
### Added
- Ability to format your own autofill methods ( --jumpmethod )
- rng::bool for future use (transparent to end user)

### Fixed
- Autofill not working at all
- Some code styling issues
- Invalid PRINT statement before print.pm loaded
- Changed 'range' to 'limit' to clarify usage in rng::generate
- Arch limit determined now on first call of rng rather than on startup
- Improved 'which' lookups

## 2.8.7 [2017/05/10]
### Fixed
- Default RNG specification

## 2.8.6 [2017/05/09]
### Fixed
- Endorsement display type
- --listwidth is now an integer not a string
- more fixes for --show

## 2.8.5 [2017/05/03]
### Fixed
- Debug code left in --show :(

### Added
- Text showing endorsement occuring

## 2.8.4 [2017/05/03]
### Fixed
- Erroneous implied --show when using --xdo

## 2.8.3 [2017/05/02]
### Fixed
- Addition bug with hyphens

## 2.8.2 [2017/04/29]
### Fixed
- Command check now occurs in the right place, so invalid commands cannot receive a 'no paramaters' error

## 2.8.1 [2017/04/22]
### Fixed
- 'show' works now when -X or -b are supplied as arguments ( no longer mutually exculsive )

## 2.8.0 [2017/04/20]
### Fixed
- Endorsement no longer borks without reason (and garbage output) if an invalid private key passphrase is loaded

### Added
- More messaging when prompting for a passphrase (i.e. why!)
- Endorsement verification changed to 'notice' for better visibility
- Version check on startup

## 2.6.1 [2017/04/10]
### Fixed
- Endorsement no longer borks if there is a missing publickey

## 2.6.0 [2017/03/30]
### Added
- Hooks for nogui version of cops. Allows cops to supress all options relating to a x-windows environment
- Hooks for bulding different rpms on os version
- Buildrpm script

## 2.4.0 [2017/03/20]
### Added
- Simple hook for those who symlink directly to cops

## 2.2.1 [2017/03/13]
### Added
- Command typo helper

## 2.2.0 [2017/02/21]
### Added
- Endorsement of entries. See documentation for details
- Examples documentation
- Changelog

### Changed
- Entries now use a new file format that will not be able to be read by previous versions of cops. Backwards compatability is maintained in cops 2.2.0

### Fixed
- License wording ( s/Foobar/COPS/ )
- Noansi now forced when using 'cops encrypt'
- Some commands allowed parameters that were not supported, and vice-versa
- Many back-end tweaks and changes invisible to the end user

