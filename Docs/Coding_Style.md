# Functions

## Return codes
Functions return 1 on success and 0 on failure ( contrary to posix standards ) because I prefer to write

	do_something() and print "It worked";

Rather than

	do_something() or print "It worked";

## Arguments
Where more than one argument is passed to a function, a hash reference is most commonly used.
Exceptions are only made where data ordering and quantity are consistent ( eg, fifo::write )



# Structure

Modules in FUNC/ should not rely on any data in COPS::args for their information. All data MUST be passed to the functions therein to maintain abstraction. (There are a couple of very small exceptions to this which are documented in code)

COPS::args::params should never be modified outside of COPS::args. EVER!

hash references are used over hashes as a styling preference.

We don't use a die handler as, to be honest, we shouldn't ever die if everything is configured correctly. We may change this later but I don't like the idea of looping back into COPS::print::error :(

EVERYTHING, and I mean EVERYTHING that handles file i/o for cops storage, be it store lists, password files, whatever ... MUST pass through COPS::FUNC::file. This allows us to abstract out file retrieval/write methods. For example, a rest api rather than a local file system, in the future.
Local binary execs, eg interacting with openssl are fine.

Cops rule #73: Sanitize everything. Then sanitize it again.

# imports

We use 'require' and import' rather than 'use' for all third party libraries that are not inherrent to the basic functionality of cops. This allows users to maintain basic operation even if we add new features that rely on an external library.
For reference, 'use' is invoked at compile time. 'require/import' is at execution so we can soft fail.









