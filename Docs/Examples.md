# Cops usage examples

Short (-S) and long (--show) parameters are supported and interchangable. Use 'cops params' for a full list
Typing 'cops help \<command>' will give you a list of parameters supported by that command (and their defaults if applicable)


## Retrieving entries
### Retrieve a password entry from a specified datastore
`cops get <search term or id> --store /path/to/store`

### Retrieve a password entry and autotype it into another screen
`cops get <search term or id> --xdo`

### Retrieve a username entry and add it to your clipboard
`cops get <search term or id> -t username -bb`

### Get the 7th and 13th characters of a password (useful for those annoying banking sites)
`cops get <search term of id> -N 7,13`


## Adding/updating entries
### Create a strong wordlist password entry for all users in the default storage
`cops create <name> -length 6 --wordlist-size 3`

### Create a password entry just for you and show it on the command line 
`cops create <name> --onlyme --show`

### Create a password entry just for someone else, and return a sha512 hash of the entry
`cops create <name> --onlyme --whoiam <other.person> --crypt sha512`

### Create a password entry where you manually input the password
`cops create <name> --nogen`

### Create a username entry
`cops create <name> -t username`

### Create an default password entry for a specific group of users (see Docs/Using_Groups.md)
`cops create --group <group name> <name>`

### Update an entry, and send it to the autotype function twice ( tab seperated ). Useful for updating a password on a form/login
`cops update <name or id> -X2`

### Update an entry to add/update data in the type 'notes'
`cops update -t notes <name or id>` 


## Generating passwords 
### Generate a 16 character random alphanumeric password
`cops generate -l 16 -ra`

### Generate a 6 element wordbank password using the level 3 wordlist, and print out strength details
`cops generate -l 6 -z3 --check`



## Encrypting and decrypting
### Encrypt some data on stdin
`cops encrypt`

### Encrypt some data on stdin using someone elses public key
`cops encrypt -p <path to key>`

### Encrypt the phrase 'this is a test' and endorse it with a signature
`cops encrypt 'this is a test' -E `

### Decrypt a cops datafile
`cops decrypt <filename>`

### Decrypt an entry on stdin using a different private key and specifying a public key to verify with
`cops decrypt -P <path to private key> --verifywith <path to public key>`





