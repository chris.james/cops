# Getting started with cops

## First run

Firstly, to check that cops will be able to run, type ./environment_check. You can ignoring any warnings assuming you do not need the additional functionality provided by that module. Anything marked 'fail' must be remedied. 
If you use the rpm version of cops, all dependencies should already be resolved.

Now, let's try a simple password generation.

`cops generate -l4 -w`

You can ignore any messages around creating directories. That's just cops being nice for you!

So, this command generates a password around the 'sweet spot' for usability versus security. The -w flag indicates that we're using the weaker /dev/urandom. We're just checking things *work*  
Now, try again without the -w, and with a much longer password

`cops generate -l64 -ra`

This generates a random character password 64 characters in length. If the generation times out then you have an issue with entropy generation on your system. This is common on virtual machines.

Now let's set up the database. If you are part of a multi-user system, speak to your administrator about where your password database is located and how it is retrieved before proceding!

`cops list`

You should get a message saying no entries found, unless of course you have set up cops before.

To add an entry,

`cops create test1`

You will see a message similar to the below

`FATAL: The function 'create' requires at least one public key to be available ( and none were found !). Either add keys to /home/chris/.cops_passwords//.cops_keys or use --publickey=<path>`

Cops needs a ssh public key to continue. Unfortunately, the default rsa public key stored in your home directory isn't in the right format. 

You should already have a ssh key set up, and the private key of that should already be sacrosanct to you ( hence the principle of cops, your ssh private key should be encrypted with the ONE passphrase you know off by heart ).

```
  [chris@localhost cops2]$ ./cops addkey ~/.ssh/id_rsa.pub
  Added key for 'chris' to store '/home/chris/.cops_passwords/'
```

*IF* this fails, an alternative method for adding your public key is detailed in ./cops help addkey. This states:

```
  If your installed version of ssh-keygen cannot convert a public ssh key to the required
  format, you can generate it manually if you are the owner of your private key by using openssl directly.

  eg

  openssl rsa -in ~/.ssh/id_rsa -pubout -out `whoami`.pem

  Copy this to the .cops.keys directory of the password store.
```

Once your key is added, we're ready to go!

```
  [chris@localhost cops2]$ ./cops -s ~/.copstest create test1
  Encrypting data for chris
  Password length was 74 characters ( to verify creation )
```

Well, that worked, but what was the password ?

There are flags you can pass on the 'create' command that will show you the password, or copy it to your clipboard or even autotype it into another window. However, we'll retrieve it the old fashioned way.

`cops get test`

Cops uses a 'best match' search method. You don't need to type the whole name.
You will now be prompted for your passphrase. This is *required* to unlock your private key .... the other hald of your asyemtric ssh keypair.

```
  [chris@localhost cops2]$ ./cops get test1
  Retrieving entry 'test1'
  Enter private key passphrase:
  Chaperones-Reich-Regressions-Punchy-Compositor-Tonality-Housebuyers-Stokes
```

You can see that the default settings generate an eight word password using the 'medium' strength wordlist.

Let's add another and create a random character password 

```
  [chris@localhost cops2]$ ./cops create test2 -ra -l16 -S
  Encrypting data for chris
  7PHZej2nnCrFNXSd
```

This time we've used -r to enforce random character generation. -a specifies alphanumerics only and -S states to show the password after generation.
All parameters in cops have either a short or long name. In this case

`cops create test2 --randomchar --alphanumeric --length 16 --show`

Would have achieved the same result.

Let's check that this password is retrievable.

```
  cops get test
  +--------------------------------------------------------------------------------------------------+
  |   ID | Entry                                           |    Password |    Username |       Notes |
  +--------------------------------------------------------------------------------------------------+
  |    1 | test1                                           |  2017/01/30 |             |             |
  |    2 | test2                                           |  2017/01/30 |             |             |
  +--------------------------------------------------------------------------------------------------+
  Multiple matches found. Please elaborate by entering an ID:
```

Here, the 'best match' concept is having a spot of bother. There are multiple entries that match your criteria.
You can either use the full name 'test2', or you could now type the id number of the entry. In this case it is 2.
You can also use the id number on the cli to be unambiguous.

`cops get 2`

*IMPORTANT* 
Id numbers WILL change as you add new entries. Never rely on a previous id number to return the same information a week later!

*IF* you wish to ensure that you only retrieve the entry expected you should use the full password name PLUS the flag --exactmatch/-e.












