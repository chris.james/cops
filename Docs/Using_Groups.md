# Using groups

As of version 2.43.2 onwards, there is now the option to specify and use groups.
This allows the same password store to be used for mulitple users whilst segregating access when required.

For example, if the following keys are stored in your password store under ./.cops_keys 

- Alice.pem
- Bob.pem
- Chris.pem
- Dave.pem
- Emily.pem
- Fred.pem
- George.pem
- Hannah.pem

We can then specify a group called 'admins'
This is done by creating a plain text file in the same directory called 'admins.group'
That file would have the following content

```
# List of superusers
Alice
Chris
Hannah
```

Note: Lines starting with a '#' are ignored


From this point on, cops still functions as expected. If you do 'cops add windows_localadmin_2019' then all users will have entries added to the system.
However, if you were to run the same command as 'cops add windows_localadmin_2019 --group=admins' then only Alice, Chris and Hannah would have their entries added.

--group ONLY functions when adding, updating or 'refreshing' an entry.

As all users have access to these group files when stored in git, and to prevent a user from adding themselves to a group, checksums are calculated and stored locally. You are warned if a checksum alters.