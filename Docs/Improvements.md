# Improvements to be analysed, and potentially implemented

  1. Option of an http storage system for remote access. Requires the implementation of a REST api server
  2. History: Default to the last chosen answer on <enter> when multiple search results returned
  
These improvements should also be tracked in git
