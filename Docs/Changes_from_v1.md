Changes from version 1 of cops to version 2.0 (some breaking changes)


Code changes
------------
Much more robust code. Removed reliance on args->param where possible from modules. Everything is now much more extensible.

Lots more error detection and sanity checking built in.


Functional changes
------------------
--be-useless argument retired. It was fairly useless! Use -ra to generate a password if password restrictions require a number

Clipboard buffer is restored on exit when using --buffer

Removed the ability to create multipart entries. They complicated things massively for very little gain. Suggestion is to AES256 encrypt anything too big for cops and store that key in cops.

--simplify option removed. Instead, pass an argument to --wordlist-size of 1,2 or 3 ( default 2 ). 3 uses the very large wordlist, 1 uses a very small wordlist.

Removed brute force grade from info. ./cops generate now provides average cracking time for a given password.

Cops now writes the encrypted entries before passing to stdout/buffer/xdo. This prevents committing a password and then having a bug/control-c exit cops before saving to the password store

--pwstore is now --store/-s 
--publickey shortcut ( -Y ) is now -p

Added ability to endorse and verify entries

command name changes
  refreshme is now keychange
  regenerate is now refresh ( to avoid confusion with 'generate' )


Cosmetic changes
----------------
Changed print colours slightly.
You can now create your own colour scheme in ~/.copsrc.
Much better information sent to the user on errors

