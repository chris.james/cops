# About the original author

## Whoami ?
I'm not a dev in the traditional sense. My background is as a linux sysadmin. But because of that, I am passionate about security. 
I also have nearly twenty years experience hacking around in perl, so it's my go-to language.

As someone without a academic dev background, I apologise in advance if any part of my coding style either upsets or confuses you. I feel I write more legible perl than a lot of what is out there, but to be honest ... that's not difficult :)

## Open source
Cops is truly open source. Fork it. Do as you will. Rewrite it in C# if that's your thing.
However, I'll only accept merge requests to the 'official' cops repo in perl ( or possibly additional modules in python or ruby ).
If I have to take time to learn a new language to accept your merge requests, it ain't gonna happen! :)

## Peer review
Cops has been peer reviewed at various stages by devs and mathematicians who are far cleverer than I.
The principles that cops works on should be sound. The implementation should be sound.

## Bugs
If you find a bug, please report it (preferably with a -D flag to get debug output).
If you find a security issue, however small, REPORT IT!


