# Password Quality Checks

## Summary

I've been through several iterations of this concept since the inception of cops.
However, every single version has made 'huge' assumptions about various aspects of password strength and storage.

Multiple cloud GPU rigs are now so insanely fast that the concept of measuring password strength by 'time to crack' is long gone. The only **safe** measurement of strength is in energy cost, but even then we have to make a huge amount of assumptions around storage mechanisms (e.g. bcrypt vs sha512) for offline attacks.

Even the zxcvbn library (which was written by people far cleverer than I) does not do the topic justice, and it is over a decade old now.

'Online' attacks are largely a non-starter. A simple (random) three or four word password will easily be strong enough to defeat any such attack, particularly when paired with rate-limiting and/or MFA

The issue of course is in a malicious actor getting hold of the hashed secrets, and *you* cannot dictate the security measures a company has taken with your passwords.

However, the advice I can provide is fairly straightforward.

If you are at ALL worried about your password being cracked via an offline attack.

Use at **LEAST** 80 bits of entropy for your secret

**With 128 bits of entropy, your secret is safe from any currently publicly known brute-force cracking method .... now, and for the near future.** - This is regardless of whether or not it is stored in bcrypt, or SHA3, or whether it is salted ... as long as the algorithm is not **broken** (e.g. md5)

You can check how much entropy is in a cops generated secret by passing the --entropy flag to generate/create/update

You can also pass --hibp to check the HaveIBennPwned api for passwords that are known in the wild
