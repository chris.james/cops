## Customising cops colour schemes

You can alter the default colour schemes within cops by adding values to your ~/.copsrc config file

Format is 

`colour-<level>=<colour>`

Where level is one of
-  debug
-  info
-  std
-  notice
-  warn
-  fatal
-  error
  
And colour is any lowercase ansi colour identifier.

-  black
-  blue
-  red
-  magenta
-  green
-  cyan
-  yellow
-  white
  
You can set a background colour using the on_ keyword, eg blue on_white
  
You can also use
-  bold
-  underline
-  reverse
-  blink
  
For example

`colour-debug=bold blue on_white`

Sets all debug information to be in a bright blue, on a white background. Not that this is a good idea! :)

Defaults are set as

  debug   => 'magenta',
  info    => 'green',
  std     => 'white',
  notice  => 'cyan',
  warn    => 'bold yellow',
  fatal   => 'red',
  error   => 'bold red',

In addition, highlighted text is set to the same as 'error'
You cannot overide the 'underline' or 'bold' settings on certain indicators ( ie. as displayed in cops help ) but the colours will be retained
This does mean that if you set 'info' or 'std' to a bold colour (or underlined) some formatting will be ambiguous.

