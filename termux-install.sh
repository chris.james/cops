#!/bin/sh
pkg install perl git clang util-linux openssh openssl-tool make termux-api -y
cd ~/
git clone https://gitlab.com/chris.james/cops.git

cpan --defaultdeps -I App::cpanminus
cpanm Term::ReadKey Digest::MD4 Digest::SHA Digest::MD5 HTTP::Tiny Crypt::CBC Crypt::Rjindael Crypth::SmbHash Moo List::AllUtils --no-wget

touch ~/cops/.nogui 

echo "termux" >> ~/.copsrc
echo "weaker" >> ~/.copsrc # /dev/random entropy pool seems to be rubbish on tested android devices
echo "PATH=$PATH:~/cops" >> ~/.profile
echo "Please note, for clipboard functionality you will also need to install the Termux API app from the playstore"

